package es.vivelibre.models.JsonConverters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.vivelibre.models.Meter.BloodPreassureMonitor;
import es.vivelibre.models.Meter.HealthMonitor;
import es.vivelibre.models.Meter.Meter;
import es.vivelibre.models.Meter.PulseOximeter;
import es.vivelibre.models.Meter.Scale;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.DateFunctions;
import es.vivelibre.models.Utils.Messages;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class JsonMeterConverter {
    public static ArrayList<Entity> ConverToDTOList(JSONArray result)
            throws Exception {
        ArrayList<Entity> meterList = new ArrayList<Entity>();
        for (int i = 0; i < result.length(); i++) {
            meterList.add(Conver((JSONObject) result.get(i)));
        }
        return meterList;
    }

    public static Meter Conver(JSONObject result) throws Exception {

        String type = result.getString("Type");
        if (type == null)
            throw new Exception(
                    Messages.warning_CannotConvertJsonToDTOWithWrongMeterType);

        Meter meter;

        if (type.equals("BloodPreassureMonitor")) {
            meter = new BloodPreassureMonitor();
        } else if (type.equals("PulseOximeter")) {
            meter = new PulseOximeter();
        } else if (type.equals("Scale")) {
            meter = new Scale();
        } else if (type.equals("HealthMonitor")) {
            meter = new HealthMonitor();
        } else
            throw new Exception(
                    Messages.warning_CannotConvertJsonToDTOWithWrongMeterType);

        Gson gson = new GsonBuilder().setDateFormat(DateFunctions.DATE_FORMAT)
                .create();
        return gson.fromJson(result.toString(), meter.getClass());
    }
}
