package es.vivelibre.models.JsonConverters;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

import es.vivelibre.models.Alert.MeterAlert;
import es.vivelibre.models.Meter.Meter;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class JsonMeterValueCompleteConverter {

    public static ArrayList<Entity> ConverToList(JSONArray result)
            throws Exception {
        ArrayList<Entity> meterValuesList = new ArrayList<Entity>();
        for (int i = 0; i < result.length(); i++) {
            meterValuesList.add(Conver((JSONObject) result.get(i)));
        }
        return meterValuesList;
    }

    public static MeterValueComplete Conver(JSONObject result)
            throws Exception {

        JSONObject meter = new JSONObject(result.getString("meter"));
        JSONObject meterValue = new JSONObject(result.getString("meterValue"));
        JSONObject alert = null;
        Meter meterAux = null;

        String type = meter.getString("Type");
        if(meter.getString("Type") != "null"){
             meterAux = JsonMeterConverter.Conver(meter);
        }


        MeterValue meterValueAux;
        meterValueAux = JsonMeterValueConverter
                .Conver(meterValue);

        if (!meterValueAux.getAlert_id().equals(new UUID(0L, 0L))) {
            alert = new JSONObject(result.getString("alert"));
        }

        MeterAlert meterAlertAux = null;
        //comentado por no funcionar
        /*if (alert != null )
            meterAlertAux = (MeterAlert) JsonAlertConverter
                    .Conver(alert);
        */
        return new MeterValueComplete(meterAux, meterValueAux, meterAlertAux);
    }
}

