package es.vivelibre.models.JsonConverters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.models.Meter.WeightValue;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.DateFunctions;
import es.vivelibre.models.Utils.Messages;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class JsonMeterValueConverter {

    public static ArrayList<Entity> ConverToDTOList(JSONArray result)
            throws Exception {
        ArrayList<Entity> meterValuesList = new ArrayList<Entity>();
        for (int i = 0; i < result.length(); i++) {
            meterValuesList.add(Conver((JSONObject) result.get(i)));
        }
        return meterValuesList;
    }

    public static MeterValue Conver(JSONObject result) throws Exception {

        String type = result.getString("Type");
        if (type == null)
            throw new Exception(
                    Messages.warning_CannotConvertJsonToDTOWithWrongMeterValueType);

        MeterValue meterValue;

        if (type.equals("BloodPressureValue")) {
            meterValue = new BloodPressureValue();
        } else if (type.equals("HeartRateValue")) {
            meterValue = new HeartRateValue();
        } else if (type.equals("OxygenSaturationValue")) {
            meterValue = new OxygenSaturationValue();
        } else if (type.equals("WeightValue")) {
            meterValue = new WeightValue();
        } else
            throw new Exception(
                    Messages.warning_CannotConvertJsonToDTOWithWrongMeterValueType);

        Gson gson = new GsonBuilder().setDateFormat(DateFunctions.DATE_FORMAT)
                .create();
        return gson.fromJson(result.toString(), meterValue.getClass());
    }
}
