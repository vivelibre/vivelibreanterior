package es.vivelibre.models.JsonConverters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import es.vivelibre.models.Alert.Alert;
import es.vivelibre.models.Alert.BloodPressureAlert;
import es.vivelibre.models.Alert.HeartRateAlert;
import es.vivelibre.models.Alert.OxygenSaturationAlert;
import es.vivelibre.models.Alert.WeightAlert;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.DateFunctions;
import es.vivelibre.models.Utils.Messages;

import org.json.JSONArray;
import org.json.JSONObject;
/**
 * Created by mfernandez on 22/04/2016.
 */
public class JsonAlertConverter {
    public static ArrayList<Entity> ConverToDTOList(JSONArray result)
            throws Exception {
        ArrayList<Entity> alertList = new ArrayList<Entity>();
        for (int i = 0; i < result.length(); i++) {
            alertList.add(Conver((JSONObject) result.get(i)));
        }
        return alertList;
    }

    public static Alert Conver(JSONObject result) throws Exception {

        String type = result.getString("Type");
        if (type == null)
            throw new Exception(
                    Messages.warning_CannotConvertJsonToDTOWithWrongAlertType);

        Alert alert;

        if (type.equals("HeartRateAlert")) {
            alert = new HeartRateAlert();
        } else if (type.equals("BloodPressureAlert")) {
            alert = new BloodPressureAlert();
        } else if (type.equals("OxygenSaturationAlert")) {
            alert = new OxygenSaturationAlert();
        } else if (type.equals("WeightAlert")) {
            alert = new WeightAlert();
        } else
            throw new Exception(
                    Messages.warning_CannotConvertJsonToDTOWithWrongAlertType);

        Gson gson = new GsonBuilder().setDateFormat(DateFunctions.DATE_FORMAT)
                .create();
        return gson.fromJson(result.toString(), alert.getClass());
    }
}
