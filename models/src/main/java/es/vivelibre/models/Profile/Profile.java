package es.vivelibre.models.Profile;

import android.database.Cursor;

import java.util.UUID;

import es.vivelibre.models.SQLite.Entity;


/**
 * Created by mfernandez on 21/04/2016.
 */
public class Profile extends Entity{

    private String Name;
    private String First_name;
    private String Last_name;
    private String Sex;
    private String Id_Card;
    private String Country;
    private String Languaje;
    private String Username;
    private ImageProfile ImageProfile;


    public Profile(String name, String firstName, String lastName, String sex,
                   String idCard, String country, String languaje, String username) {
        super();
        Name = name;
        First_name = firstName;
        Last_name = lastName;
        Sex = sex;
        Id_Card = idCard;
        Country = country;
        Languaje = languaje;
        Username = username;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFirst_name() {
        return First_name;
    }

    public void setFirst_name(String first_name) {
        First_name = first_name;
    }

    public String getLast_name() {
        return Last_name;
    }

    public void setLast_name(String last_name) {
        Last_name = last_name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getId_Card() {
        return Id_Card;
    }

    public void setId_Card(String id_Card) {
        Id_Card = id_Card;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getLanguaje() {
        return Languaje;
    }

    public void setLanguaje(String languaje) {
        Languaje = languaje;
    }

    public ImageProfile getImageProfile() {
        return ImageProfile;
    }

    public void setImageProfile(ImageProfile imageProfile) {
        ImageProfile = imageProfile;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }



    public Profile() {
    }

    public Profile(UUID userId) {
        super.ChangeCurrentIdentity(userId);
    }


    public Profile(Cursor cursor) {
        super.ChangeCurrentIdentity(UUID.fromString(cursor.getString(0)));
        setName(cursor.getString(1));
        setFirst_name(cursor.getString(2));
        setLast_name(cursor.getString(3));
        setSex(cursor.getString(4));
        setId_Card(cursor.getString(5));
        setCountry(cursor.getString(6));
        setLanguaje(cursor.getString(7));
        setUsername(cursor.getString(8));
    }

    public static Profile generateProfileFromCursor(Cursor cursor) {
        if (cursor.moveToFirst()) {
            do {
                Profile profile = new Profile(cursor);
                if (profile != null)
                    return profile;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return null;
    }
}
