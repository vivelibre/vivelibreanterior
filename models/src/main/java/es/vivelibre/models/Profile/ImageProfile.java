package es.vivelibre.models.Profile;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.UUID;

import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 21/04/2016.
 */
public class ImageProfile extends Entity {
    private static final long serialVersionUUID = -9028816541544277458L;
    public String Name;
    public String Extension;
    public String Data;

    public ImageProfile(UUID id) {
        super.ChangeCurrentIdentity(id);
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getExtension() {
        return Extension;
    }

    public void setExtension(String extension) {
        Extension = extension;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public ImageProfile(){

    }
    public ImageProfile(String name, String extension, String data) {
        setName(name);
        setExtension(extension);
        setData(data);
    }

    public ImageProfile(Cursor cursor) {
        super.ChangeCurrentIdentity(UUID.fromString(cursor.getString(0)));
        setName(cursor.getString(1));
        setExtension(cursor.getString(2));
        setData(cursor.getString(3));
    }

    public static ImageProfile generateImageProfileFromCursor(Cursor cursor) {
        if (cursor.moveToFirst()) {
            do {
                ImageProfile imageProfile = new ImageProfile(cursor);
                if (imageProfile != null)
                    return imageProfile;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return null;
    }
}
