package es.vivelibre.models.SQLite;

import android.content.ContentValues;

import es.vivelibre.models.Profile.Profile;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class SQLiteProfileUtils {
    private final static String PROFILE_SQLITE_TABLE = "Profile";
    private final static String PROFILE_SQLITE_TABLE_ID = "Id";
    private final static String PROFILE_SQLITE_TABLE_NAME = "Name";
    private final static String PROFILE_SQLITE_TABLE_FIRST_NAME = "First_name";
    private final static String PROFILE_SQLITE_TABLE_LAST_NAME = "Last_name";
    private final static String PROFILE_SQLITE_TABLE_SEX = "Sex";
    private final static String PROFILE_SQLITE_TABLE_CARD_ID = "Id_Card";
    private final static String PROFILE_SQLITE_TABLE_COUNTRY = "Country";
    private final static String PROFILE_SQLITE_TABLE_LANGUAJE = "Languaje";
    private final static String PROFILE_SQLITE_TABLE_USERNAME = "Username";

    public static String getProfileSqliteTable() {
        return PROFILE_SQLITE_TABLE;
    }

    public static String getProfileSqliteTableId() {
        return PROFILE_SQLITE_TABLE_ID;
    }

    public static String getProfileSqliteTableName() {
        return PROFILE_SQLITE_TABLE_NAME;
    }

    public static String getProfileSqliteTableFirstName() {return PROFILE_SQLITE_TABLE_FIRST_NAME;}

    public static String getProfileSqliteTableLastName() {
        return PROFILE_SQLITE_TABLE_LAST_NAME;
    }

    public static String getProfileSqliteTableSex() {
        return PROFILE_SQLITE_TABLE_SEX;
    }

    public static String getProfileSqliteTableCardId() {
        return PROFILE_SQLITE_TABLE_CARD_ID;
    }

    public static String getProfileSqliteTableCountry() {
        return PROFILE_SQLITE_TABLE_COUNTRY;
    }

    public static String getProfileSqliteTableLanguage() {
        return PROFILE_SQLITE_TABLE_LANGUAJE;
    }

    public static String getProfileSqliteTableUsername() {
        return PROFILE_SQLITE_TABLE_USERNAME;
    }

    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + getProfileSqliteTable() + " ("
                + getProfileSqliteTableId() + " VARCHAR(36) PRIMARY KEY,"
                + getProfileSqliteTableName() + " VARCHAR(50),"
                + getProfileSqliteTableFirstName() + " VARCHAR(50),"
                + getProfileSqliteTableLastName() + "  VARCHAR(250),"
                + getProfileSqliteTableSex() + " VARCHAR(25),"
                + getProfileSqliteTableCardId() + " VARCHAR(10),"
                + getProfileSqliteTableCountry() + " VARCHAR(50),"
                + getProfileSqliteTableLanguage() + " VARCHAR(50),"
                + getProfileSqliteTableUsername() + " VARCHAR(50))";

    }

    public static ContentValues generateInsertProfileValues(Profile profile) {

        ContentValues values = new ContentValues();
        values.put(getProfileSqliteTableId(), profile.getId().toString());
        values.put(getProfileSqliteTableName(), profile.getName());
        values.put(getProfileSqliteTableFirstName(), profile.getFirst_name());
        values.put(getProfileSqliteTableLastName(), profile.getLast_name());
        values.put(getProfileSqliteTableSex(), profile.getSex());
        values.put(getProfileSqliteTableCardId(), profile.getId_Card());
        values.put(getProfileSqliteTableCountry(), profile.getCountry());
        values.put(getProfileSqliteTableLanguage(), profile.getLanguaje());
        values.put(getProfileSqliteTableUsername(), profile.getUsername());

        return values;
    }

    public static ContentValues generateUpdateProfileValues(Profile profile) {
        ContentValues values = generateInsertProfileValues(profile);
        values.remove(SQLiteProfileUtils.getProfileSqliteTableId());

        return values;
    }

    public static String getUpdateTableQuery() {
        return getProfileSqliteTableId() + "=?";
    }

    public static String[] getUpdateArgs(Profile profile) {
        return new String[] { profile.getId().toString() };
    }

    public static String getRemoveTableQuery() {
        return getProfileSqliteTableId() + "=?";
    }

    public static String[] getRemoveArgs(Profile profile) {
        return new String[] { profile.getId().toString() };
    }

    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + getProfileSqliteTable();
    }
}

