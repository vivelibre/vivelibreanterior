package es.vivelibre.models.SQLite;

import java.util.UUID;

/**
 * Created by mfernandez on 22/04/2016.
 */
public abstract class Entity {
    private UUID Id;

    public boolean IsTransient() {
        return (Id == null || (Id.toString().length()) == 0);
    }

    public void GenerateNewIdentity() {
        if (IsTransient())
            this.Id = UUID.randomUUID();
    }

    public void ChangeCurrentIdentity(UUID identity) {
        if ((identity.toString()).length() != 0)
            this.Id = identity;
    }

    public boolean Equals(Object obj) {
        if (obj == null || !(obj instanceof Entity))
            return false;

        if (this == obj)
            return true;

        Entity item = (Entity) obj;

        if (item.IsTransient() || this.IsTransient())
            return false;
        else
            return (item.Id).compareTo(this.Id) == 0;
    }

    public UUID getId() {
        return Id;
    }

    public void setId(UUID id) {
        this.Id = id;
    }

}
