package es.vivelibre.models.SQLite;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class SQLiteMeterUtils {
    private final static String METER_SQLITE_TABLE = "Meter";

    private final static String METER_SQLITE_TABLE_ID = "Id";
    private final static String METER_SQLITE_TABLE_BRAND = "Brand";
    private final static String METER_SQLITE_TABLE_MODEL = "Model";
    private final static String METER_SQLITE_TABLE_SERIAL_NUMBER = "SerialNumber";
    private final static String METER_SQLITE_TABLE_TYPE = "Type";

    public static String getMeterSqliteTable() {
        return METER_SQLITE_TABLE;
    }

    public static String getMeterSqliteTableId() {
        return METER_SQLITE_TABLE_ID;
    }

    public static String getMeterSqliteTableBrand() {
        return METER_SQLITE_TABLE_BRAND;
    }

    public static String getMeterSqliteTableModel() {
        return METER_SQLITE_TABLE_MODEL;
    }

    public static String getMeterSqliteTableSerialNumber() {
        return METER_SQLITE_TABLE_SERIAL_NUMBER;
    }

    public static String getMeterSqliteTableType() {
        return METER_SQLITE_TABLE_TYPE;
    }

    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + getMeterSqliteTable() + " ("
                + getMeterSqliteTableId() + " VARCHAR(36) PRIMARY KEY,"
                + getMeterSqliteTableBrand() + " VARCHAR(50),"
                + getMeterSqliteTableModel() + " VARCHAR(50),"
                + getMeterSqliteTableSerialNumber() + " VARCHAR(50),"
                + getMeterSqliteTableType() + " INTEGER)";
    }

    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + getMeterSqliteTable();
    }

}
