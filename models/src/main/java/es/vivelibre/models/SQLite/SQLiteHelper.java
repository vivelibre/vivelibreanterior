package es.vivelibre.models.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class SQLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "technology.vivelibre.health.client.db";
    private static final int DATABASE_VERSION = 1;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLiteProfileUtils.getCreateTableQuery());
        db.execSQL(SQLiteImageProfileUtils.getCreateTableQuery());
        db.execSQL(SQLiteMeterUtils.getCreateTableQuery());
        db.execSQL(SQLiteAlertUltils.getCreateTableQuery());
        db.execSQL(SQLiteMeterValueUtils.getCreateTableQuery());
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        manageUpgrade(db, oldVersion);
    }

    private void manageUpgrade(SQLiteDatabase db, int oldVersion) {

    }
    public static String getHelperSqliteTable(){
        return DATABASE_NAME;
    }
    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + getHelperSqliteTable();
    }
    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + getHelperSqliteTable();
    }

}

