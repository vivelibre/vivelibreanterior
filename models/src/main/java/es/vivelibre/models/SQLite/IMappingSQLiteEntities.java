package es.vivelibre.models.SQLite;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IMappingSQLiteEntities {
    ContentValues GenerateInsertValues(Entity item);

    String GenerateRemove(Entity item);

    String[] GenerateRemoveArgs(Entity item);

    String GenerateUpdate(Entity item);

    String[] GenerateUpdateArgs(Entity item);

    String GenerateGet(Entity item);

    String[] GenerateGetArgs(Entity item);

    String GenerateGetOrderBy(Entity item);

    String GenerateGetAll(Entity item);

    String GetDataBaseTableName(Entity item);

    Entity GetEntityFromCursor(Entity item, Cursor cursor);

    ArrayList<Entity> GetListEntitiesFromCursor(Entity item, Cursor cursor);

    ContentValues GenerateUpdateValues(Entity item);
}
