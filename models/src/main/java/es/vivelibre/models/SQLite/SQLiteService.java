package es.vivelibre.models.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import static android.content.ContentValues.TAG;

/**
 * Created by maca- on 16/05/2016.
 */
public class SQLiteService {

    public static void deleteMeterSQLite(Context context) {
        SQLiteHelper chdbh = new SQLiteHelper(context);
        SQLiteDatabase db = chdbh.getWritableDatabase();

        try {
            db.execSQL(SQLiteMeterUtils.getDeleteTableQuery());
            db.execSQL(SQLiteMeterUtils.getCreateTableQuery());
            db.close();
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "SQLiteService deleteMeterSQLite");
            FirebaseCrash.report(e);
            db.close();
        }
    }

    public static void deleteAlertSQLite(Context context) {
        SQLiteHelper chdbh = new SQLiteHelper(context);
        SQLiteDatabase db = chdbh.getWritableDatabase();

        try {
            db.execSQL(SQLiteAlertUltils.getDeleteTableQuery());
            db.execSQL(SQLiteAlertUltils.getCreateTableQuery());
            db.close();
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "SQLiteService deleteAlertSQLite");
            FirebaseCrash.report(e);
            db.close();
        }
    }

    public static void deleteImageProfileSQLite(Context context) {
        SQLiteHelper chdbh = new SQLiteHelper(context);
        SQLiteDatabase db = chdbh.getWritableDatabase();

        try {
            db.execSQL(SQLiteImageProfileUtils.getDeleteTableQuery());
            db.execSQL(SQLiteImageProfileUtils.getCreateTableQuery());
            db.close();
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "SQLiteService deleteImageProfileSQLite");
            FirebaseCrash.report(e);
            db.close();
        }
    }

    public static void deleteMeterValueSQLite(Context context) {
        SQLiteHelper chdbh = new SQLiteHelper(context);
        SQLiteDatabase db = chdbh.getWritableDatabase();

        try {
            db.execSQL(SQLiteMeterValueUtils.getDeleteTableQuery());
            db.execSQL(SQLiteMeterValueUtils.getCreateTableQuery());
            db.close();
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "SQLiteService deleteMeterValueSQLite");
            FirebaseCrash.report(e);
            db.close();
        }
    }


    public static void deleteProfileSQLite(Context context) {
        SQLiteHelper chdbh = new SQLiteHelper(context);
        SQLiteDatabase db = chdbh.getWritableDatabase();

        try {
            db.execSQL(SQLiteProfileUtils.getDeleteTableQuery());
            db.execSQL(SQLiteProfileUtils.getCreateTableQuery());
            db.close();
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "SQLiteService deleteProfileSQLite");
            FirebaseCrash.report(e);
            db.close();
        }
    }
    public static void deleteHelperSQLite(Context context) {
        SQLiteHelper chdbh = new SQLiteHelper(context);
        SQLiteDatabase db = chdbh.getWritableDatabase();

        try {
            db.execSQL(SQLiteHelper.getDeleteTableQuery());
            db.execSQL(SQLiteHelper.getCreateTableQuery());
            db.close();
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "SQLiteService deleteHelperSQLite");
            FirebaseCrash.report(e);
            db.close();
        }
    }






}
