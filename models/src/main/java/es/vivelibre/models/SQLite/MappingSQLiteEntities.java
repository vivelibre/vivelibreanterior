package es.vivelibre.models.SQLite;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;

import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class MappingSQLiteEntities implements IMappingSQLiteEntities {

    private static MappingSQLiteEntities INSTANCE = new MappingSQLiteEntities();

    public static MappingSQLiteEntities getInstance() {
        return INSTANCE;
    }

    public ContentValues GenerateInsertValues(Entity item) {
        if (item instanceof Profile) {
            return SQLiteProfileUtils
                    .generateInsertProfileValues((Profile) item);
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils
                    .generateInsertImageProfileValues((ImageProfile) item);
        } else if (item instanceof MeterValue) {
            return SQLiteMeterValueUtils
                    .generateInsertMeterValueValues((MeterValue) item);
        }
        return null;
    }

    public String GenerateUpdate(Entity item) {
        if (item instanceof Profile) {
            return SQLiteProfileUtils.getUpdateTableQuery();
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils.getUpdateTableQuery();
        } else if (item instanceof MeterValue) {
            return SQLiteMeterValueUtils.getUpdateTableQuery();
        }
        return null;
    }

    public String[] GenerateUpdateArgs(Entity item) {
        if (item instanceof Profile) {
            return SQLiteProfileUtils.getUpdateArgs((Profile) item);
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils.getUpdateArgs((ImageProfile) item);
        } else if (item instanceof MeterValue) {
            return SQLiteMeterValueUtils.getUpdateArgs((MeterValue) item);
        }
        return null;
    }

    public String GenerateRemove(Entity item) {
        if (item instanceof Profile) {
            return SQLiteProfileUtils.getRemoveTableQuery();
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils.getRemoveTableQuery();
        }
        return null;
    }

    public String[] GenerateRemoveArgs(Entity item) {
        if (item instanceof Profile) {
            return SQLiteProfileUtils.getRemoveArgs((Profile) item);
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils.getRemoveArgs((ImageProfile) item);
        }
        return null;
    }

    public String GenerateGet(Entity item) {
        if (item instanceof Profile) {
            return null;
        } else if (item instanceof ImageProfile) {
            return null;
        }
        return null;
    }

    public String[] GenerateGetArgs(Entity item) {
        if (item instanceof Profile) {
            return null;
        } else if (item instanceof ImageProfile) {
            return null;
        }
        return null;
    }

    public String[] GenerateGetAllArgs(Entity item) {
        if (item instanceof MeterValue) {
            return ((MeterValue)item).GetSpecificationArgs();
        }
        return null;
    }

    public String GenerateGetOrderBy(Entity item) {
        if (item instanceof Profile) {
            return null;
        } else if (item instanceof ImageProfile) {
            return null;
        }
        return null;
    }

    public String GenerateGetAll(Entity item) {
        if (item instanceof Profile) {
            return null;
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils.getGetTableQuery();
        } else if (item instanceof MeterValue) {
            return ((MeterValue)item).GetSpecification();
        }
        return null;
    }

    public String GetDataBaseTableName(Entity item) {
        if (item instanceof Profile) {
            return SQLiteProfileUtils.getProfileSqliteTable();
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils.getImageSqliteTable();
        } else if (item instanceof MeterValue) {
            return SQLiteMeterValueUtils.getMeterValueSqliteTable();
        }
        return "";
    }

    public ContentValues GenerateUpdateValues(Entity item) {
        if (item instanceof Profile) {
            return SQLiteProfileUtils
                    .generateUpdateProfileValues((Profile) item);
        } else if (item instanceof ImageProfile) {
            return SQLiteImageProfileUtils
                    .generateUpdateImageProfileValues((ImageProfile) item);
        } else if (item instanceof MeterValue) {
            return SQLiteMeterValueUtils
                    .generateUpdateMeterValues((MeterValue) item);
        }
        return null;
    }

    public Entity GetEntityFromCursor(Entity item, Cursor cursor) {
        if (item instanceof Profile) {
            return Profile.generateProfileFromCursor(cursor);
        } else if (item instanceof ImageProfile) {
            return ImageProfile.generateImageProfileFromCursor(cursor);
        }
        return null;
    }

    public ArrayList<Entity> GetListEntitiesFromCursor(Entity item,
                                                       Cursor cursor) {
        if (item instanceof MeterValue) {
            return MeterValue.GenerateListMeterValuesFromCursor(cursor, item);
        }
        return null;
    }

}
