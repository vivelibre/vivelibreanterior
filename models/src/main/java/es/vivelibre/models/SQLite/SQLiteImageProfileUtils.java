package es.vivelibre.models.SQLite;

import android.content.ContentValues;

import es.vivelibre.models.Profile.ImageProfile;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class SQLiteImageProfileUtils {
    private final static String IMAGE_SQLITE_TABLE = "ImageProfile";

    private final static String IMAGE_SQLITE_TABLE_ID = "Id";
    private final static String IMAGE_SQLITE_TABLE_NAME = "Name";
    private final static String IMAGE_SQLITE_TABLE_EXTENSION = "Extension";
    private final static String IMAGE_SQLITE_TABLE_DATA = "Data";

    public static String getImageSqliteTable() {
        return IMAGE_SQLITE_TABLE;
    }

    public static String getImageSqliteTableId() {
        return IMAGE_SQLITE_TABLE_ID;
    }

    public static String getImageSqliteTableName() {
        return IMAGE_SQLITE_TABLE_NAME;
    }

    public static String getImageSqliteTableExtension() {
        return IMAGE_SQLITE_TABLE_EXTENSION;
    }

    public static String getImageSqliteTableData() {
        return IMAGE_SQLITE_TABLE_DATA;
    }

    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + getImageSqliteTable() + " ("
                + getImageSqliteTableId() + " VARCHAR(36) PRIMARY KEY,"
                + getImageSqliteTableName() + " VARCHAR(50),"
                + getImageSqliteTableExtension() + " VARCHAR(10),"
                + getImageSqliteTableData() + "  TEXT," + "FOREIGN KEY("
                + getImageSqliteTableId() + ") REFERENCES "
                + SQLiteProfileUtils.getProfileSqliteTable() + "("
                + SQLiteProfileUtils.getProfileSqliteTableId() + ") ON UPDATE CASCADE)";

    }

    public static ContentValues generateInsertImageProfileValues(
            ImageProfile imageProfile) {

        ContentValues values = new ContentValues();
        values.put(getImageSqliteTableId(), imageProfile.getId().toString());
        values.put(getImageSqliteTableName(), imageProfile.getName());
        values.put(getImageSqliteTableExtension(), imageProfile.getExtension());
        values.put(getImageSqliteTableData(), imageProfile.getData());

        return values;
    }

    public static String getUpdateTableQuery() {
        return getImageSqliteTableId() + "=?";
    }

    public static String[] getUpdateArgs(ImageProfile imageProfile) {
        return new String[] { imageProfile.getId().toString() };
    }

    public static String getRemoveTableQuery() {
        return getImageSqliteTableId() + "=?";
    }

    public static String[] getRemoveArgs(ImageProfile imageProfile) {
        return new String[] { imageProfile.getId().toString() };
    }

    public static String getGetTableQuery() {
        return getImageSqliteTableId() + "=?";
    }

    public static String[] getGetArgs(ImageProfile imageProfile) {
        return new String[] { imageProfile.getId().toString() };
    }

    public static ContentValues generateUpdateImageProfileValues(
            ImageProfile imageProfile) {
        ContentValues values = generateInsertImageProfileValues(imageProfile);
        values.remove(SQLiteProfileUtils.getProfileSqliteTableId());

        return values;
    }
    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + getImageSqliteTable();
    }

}

