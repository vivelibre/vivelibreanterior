package es.vivelibre.models.SQLite;

import android.content.ContentValues;

import es.vivelibre.models.Meter.MeterValue;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class SQLiteMeterValueUtils {
    private final static String METER_VALUE_SQLITE_TABLE = "MeterValue";

    private final static String METER_VALUE_SQLITE_TABLE_ID = "Id";
    private final static String METER_VALUE_SQLITE_TABLE_METER_ID = "Meter_id";
    private final static String METER_VALUE_SQLITE_TABLE_ALERT_ID = "Alert_id";
    private final static String METER_VALUE_SQLITE_TABLE_VALUE = "Value";
    private final static String METER_VALUE_SQLITE_TABLE_DATE_READ = "Date_read";
    private final static String METER_VALUE_SQLITE_TABLE_DATE_SEND = "Date_send";
    private final static String METER_VALUE_SQLITE_TABLE_TYPE = "Type";
    private final static String METER_VALUE_SQLITE_TABLE_ERROR = "Error";
    private final static String METER_VALUE_SQLITE_TABLE_CLIENT_ID = "Client_id";

    public static String getMeterValueSqliteTable() {
        return METER_VALUE_SQLITE_TABLE;
    }

    public static String getMeterValueSqliteTableId() {
        return METER_VALUE_SQLITE_TABLE_ID;
    }

    public static String getMeterValueSqliteTableMeterId() {
        return METER_VALUE_SQLITE_TABLE_METER_ID;
    }

    public static String getMeterValueSqliteTableAlertId() {
        return METER_VALUE_SQLITE_TABLE_ALERT_ID;
    }

    public static String getMeterValueSqliteTableValue() {
        return METER_VALUE_SQLITE_TABLE_VALUE;
    }

    public static String getMeterValueSqliteTableDateRead() {
        return METER_VALUE_SQLITE_TABLE_DATE_READ;
    }

    public static String getMeterValueSqliteTableDateSend() {
        return METER_VALUE_SQLITE_TABLE_DATE_SEND;
    }

    public static String getMeterValueSqliteTableType() {
        return METER_VALUE_SQLITE_TABLE_TYPE;
    }

    public static String getMeterValueSqliteTableError() {
        return METER_VALUE_SQLITE_TABLE_ERROR;
    }

    public static String getMeterValueSqliteTableClientId() {
        return METER_VALUE_SQLITE_TABLE_CLIENT_ID;
    }

    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + getMeterValueSqliteTable()
                + " (" + getMeterValueSqliteTableId()
                + " VARCHAR(36) PRIMARY KEY,"
                + getMeterValueSqliteTableMeterId() + " VARCHAR(36),"
                + getMeterValueSqliteTableAlertId() + " VARCHAR(36),"
                + getMeterValueSqliteTableValue() + "  VARCHAR(250),"
                + getMeterValueSqliteTableDateRead() + " INTEGER,"
                + getMeterValueSqliteTableDateSend() + " INTEGER,"
                + getMeterValueSqliteTableType() + " INTEGER,"
                + getMeterValueSqliteTableError() + " INTEGER,"
                + getMeterValueSqliteTableClientId() + " VARCHAR(36),"

                + "FOREIGN KEY(" + getMeterValueSqliteTableClientId()
                + ") REFERENCES " + SQLiteProfileUtils.getProfileSqliteTable()
                + "(" + SQLiteProfileUtils.getProfileSqliteTableId()
                + ") ON UPDATE CASCADE," +

                "FOREIGN KEY(" + getMeterValueSqliteTableMeterId()
                + ") REFERENCES " + SQLiteMeterUtils.getMeterSqliteTable()
                + "(" + SQLiteMeterUtils.getMeterSqliteTableId()
                + ") ON UPDATE CASCADE" + ")";
    }

    public static ContentValues generateInsertMeterValueValues(
            MeterValue meterValue) {

        ContentValues values = new ContentValues();
        values.put(getMeterValueSqliteTableId(), meterValue.getId().toString());
        values.put(getMeterValueSqliteTableMeterId(), meterValue.getMeter_id());
        values.put(getMeterValueSqliteTableAlertId(),
                (meterValue.getAlert_id() != null) ? meterValue.getAlert_id()
                        .toString() : "");
        values.put(getMeterValueSqliteTableValue(), meterValue.getValue());
        values.put(getMeterValueSqliteTableDateRead(), meterValue.getDateRead()
                .getTime());
        values.put(getMeterValueSqliteTableDateSend(), meterValue.getDateSend()
                .getTime());
        values.put(getMeterValueSqliteTableType(), meterValue.getType());
        values.put(getMeterValueSqliteTableError(), meterValue.getError());
        values.put(getMeterValueSqliteTableClientId(), meterValue
                .getClient_id().toString());

        return values;
    }

    public static String getGetTableQuery() {
        return getMeterValueSqliteTable() + "=?";
    }

    public static String[] getGetArgs(MeterValue meterValue) {
        return new String[] { meterValue.getId().toString() };
    }

    public static String getUpdateTableQuery() {
        return getMeterValueSqliteTableId() + "=?";
    }

    public static String[] getUpdateArgs(MeterValue meterValue) {
        return new String[] { meterValue.getId().toString() };
    }

    public static ContentValues generateUpdateMeterValues(MeterValue meterValue) {
        ContentValues values = new ContentValues();
        values.put(getMeterValueSqliteTableDateSend(), meterValue.getDateSend()
                .getTime());
        values.put(getMeterValueSqliteTableError(), meterValue.getError());

        return values;
    }

    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + getMeterValueSqliteTable();
    }

}

