package es.vivelibre.models.SQLite;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class SQLiteAlertUltils {
    private final static String ALERT_SQLITE_TABLE = "Alert";

    private final static String ALERT_SQLITE_TABLE_ID = "Id";
    private final static String ALERT_SQLITE_TABLE_DATE_CREATE = "Date_Create";
    private final static String ALERT_SQLITE_TABLE_STATE = "State";
    private final static String ALERT_SQLITE_TABLE_CLIENT_ID = "ClientId";
    private final static String ALERT_SQLITE_TABLE_TYPE = "Type";
    private final static String ALERT_SQLITE_TABLE_SUBTYPE = "Subtype";
    private final static String ALERT_SQLITE_TABLE_DATE_CLOSE = "Date_Close";
    private final static String ALERT_SQLITE_TABLE_DESCRIPTION_CLIENT = "DescriptionClient";
    private final static String ALERT_SQLITE_TABLE_LEVEL = "Level";
    private final static String ALERT_SQLITE_TABLE_METER_ID = "MeterId";

    public static String getAlertSqliteTable() {
        return ALERT_SQLITE_TABLE;
    }

    public static String getAlertSqliteTableId() {
        return ALERT_SQLITE_TABLE_ID;
    }

    public static String getAlertSqliteTableDateCreate() {
        return ALERT_SQLITE_TABLE_DATE_CREATE;
    }

    public static String getAlertSqliteTableState() {
        return ALERT_SQLITE_TABLE_STATE;
    }

    public static String getAlertSqliteTableClientId() {
        return ALERT_SQLITE_TABLE_CLIENT_ID;
    }

    public static String getAlertSqliteTableType() {
        return ALERT_SQLITE_TABLE_TYPE;
    }

    public static String getAlertSqliteTableSubtype() {
        return ALERT_SQLITE_TABLE_SUBTYPE;
    }

    public static String getAlertSqliteTableDateClose() {
        return ALERT_SQLITE_TABLE_DATE_CLOSE;
    }

    public static String getAlertSqliteTableDescriptionClient() {
        return ALERT_SQLITE_TABLE_DESCRIPTION_CLIENT;
    }

    public static String getAlertSqliteTableLevel() {
        return ALERT_SQLITE_TABLE_LEVEL;
    }

    public static String getAlertSqliteTableMeterId() {
        return ALERT_SQLITE_TABLE_METER_ID;
    }

    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + getAlertSqliteTable() + " ("
                + getAlertSqliteTableId() + " VARCHAR(36) PRIMARY KEY,"
                + getAlertSqliteTableState() + " VARCHAR(25),"
                + getAlertSqliteTableClientId() + " VARCHAR(36),"
                + getAlertSqliteTableMeterId() + " VARCHAR(36),"
                + getAlertSqliteTableType() + "  INTEGER,"
                + getAlertSqliteTableSubtype() + "  INTEGER,"
                + getAlertSqliteTableLevel() + "  INTEGER,"
                + getAlertSqliteTableDateCreate() + " VARCHAR(25),"
                + getAlertSqliteTableDateClose() + " VARCHAR(10),"
                + getAlertSqliteTableDescriptionClient() + " VARCHAR(1000),"

                + "FOREIGN KEY(" + getAlertSqliteTableClientId()
                + ") REFERENCES " + SQLiteProfileUtils.getProfileSqliteTable()
                + "(" + SQLiteProfileUtils.getProfileSqliteTableId()
                + ") ON UPDATE CASCADE," +

                "FOREIGN KEY(" + getAlertSqliteTableMeterId() + ") REFERENCES "
                + SQLiteMeterUtils.getMeterSqliteTable() + "("
                + SQLiteMeterUtils.getMeterSqliteTableId()
                + ") ON UPDATE CASCADE" + ")";
    }
    public static String getDeleteTableQuery() {
        return "DROP TABLE IF EXISTS " + getAlertSqliteTable();
    }

}

