package es.vivelibre.models.Repository;

import android.content.Context;

import java.util.ArrayList;

import es.vivelibre.models.Profile.Profile;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class ProfileRepository extends Repository<Profile> implements
        IProfileRepository {

    private static ProfileRepository INSTANCE = new ProfileRepository();

    public static ProfileRepository getInstance() {
        return INSTANCE;
    }

    public ProfileRepository() {
        super();
    }

    @Override
    public ArrayList<Profile> GetAll(Context context, Profile item) {
        return null;
    }
}
