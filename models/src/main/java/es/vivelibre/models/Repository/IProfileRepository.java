package es.vivelibre.models.Repository;

import es.vivelibre.models.Profile.Profile;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IProfileRepository extends IRepository<Profile> {

}
