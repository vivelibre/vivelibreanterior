package es.vivelibre.models.Repository;

import es.vivelibre.models.Meter.MeterValue;

/**
 * Created by mfernandez on 10/05/2016.
 */
public interface IMeterValueRepository extends IRepository<MeterValue> {

}
