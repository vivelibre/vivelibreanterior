package es.vivelibre.models.Repository;

import es.vivelibre.models.Profile.ImageProfile;

/**
 * Created by mfernandez on 25/04/2016.
 */
public interface IImageProfileRepository extends IRepository<ImageProfile> {

}
