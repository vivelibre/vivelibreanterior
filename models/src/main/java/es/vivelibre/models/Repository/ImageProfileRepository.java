package es.vivelibre.models.Repository;

import es.vivelibre.models.Profile.ImageProfile;

/**
 * Created by mfernandez on 25/04/2016.
 */
public class ImageProfileRepository extends Repository<ImageProfile> implements IImageProfileRepository {

    private static ImageProfileRepository INSTANCE = new ImageProfileRepository();

    public static ImageProfileRepository getInstance() {
        return INSTANCE;
    }

    public ImageProfileRepository() {
        super();
    }
}
