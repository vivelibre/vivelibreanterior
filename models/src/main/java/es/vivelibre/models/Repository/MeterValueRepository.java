package es.vivelibre.models.Repository;

import es.vivelibre.models.Meter.MeterValue;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class MeterValueRepository extends Repository<MeterValue> implements
        IMeterValueRepository {

    private static MeterValueRepository INSTANCE = new MeterValueRepository();

    public static MeterValueRepository getInstance() {
        return INSTANCE;
    }

    public MeterValueRepository() {
        super();
    }

}