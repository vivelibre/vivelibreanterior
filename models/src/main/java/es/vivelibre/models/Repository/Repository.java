package es.vivelibre.models.Repository;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.SQLite.MappingSQLiteEntities;
import es.vivelibre.models.SQLite.SQLiteHelper;

/**
 * Created by mfernandez on 22/04/2016.
 */
public abstract class Repository<T extends Entity> implements IRepository<T> {

    protected MappingSQLiteEntities mapping = MappingSQLiteEntities
            .getInstance();

    public Repository() {
        super();
    }

    public boolean Add(Context context, T item) {
        SQLiteDatabase db = getSQLiteDatabase(context, item);
        try {
            long returnCode = db.insertWithOnConflict(
                    mapping.GetDataBaseTableName(item), null,
                    mapping.GenerateInsertValues(item),
                    SQLiteDatabase.CONFLICT_IGNORE);
            db.close();
            return (returnCode != -1);
        } catch (Exception e) {
            db.close();
            return false;
        }
    }

    public void Remove(Context context, T item) {
        SQLiteDatabase db = getSQLiteDatabase(context, item);
        db.delete(mapping.GetDataBaseTableName(item),
                mapping.GenerateRemove(item), mapping.GenerateRemoveArgs(item));
        db.close();
    }

    public void Update(Context context, T item) {
        SQLiteDatabase db = getSQLiteDatabase(context, item);
        db.update(mapping.GetDataBaseTableName(item),
                mapping.GenerateUpdateValues(item),
                mapping.GenerateUpdate(item), mapping.GenerateUpdateArgs(item));
        db.close();
    }

    @SuppressWarnings("unchecked")
    public T Get(Context context, T item) {
        SQLiteDatabase db = getSQLiteDatabase(context, item);

        Cursor c = db.query(mapping.GetDataBaseTableName(item), null,
                mapping.GenerateGet(item), mapping.GenerateGetArgs(item), null,
                null, mapping.GenerateGetOrderBy(item));
        Entity entity = mapping.GetEntityFromCursor(item, c);
        c.close();
        return (T) entity;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<T> GetAll(Context context, T item) {
        SQLiteDatabase db = getSQLiteDatabase(context, item);

        Cursor c = db.query(mapping.GetDataBaseTableName(item), null,
                mapping.GenerateGetAll(item),
                mapping.GenerateGetAllArgs(item), null, null,
                null);
        ArrayList<Entity> listEntities = mapping.GetListEntitiesFromCursor(
                item, c);
        c.close();
        return (ArrayList<T>) listEntities;
    }

	/*
	 * PRIVATE FUNCTIONS
	 */

    private SQLiteDatabase getSQLiteDatabase(Context context, Entity item) {
        SQLiteHelper chdbh = new SQLiteHelper(context);
        return chdbh.getWritableDatabase();
    }

}
