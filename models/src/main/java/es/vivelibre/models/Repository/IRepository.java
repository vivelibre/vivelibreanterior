package es.vivelibre.models.Repository;

import android.content.Context;

import java.util.ArrayList;

import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IRepository<T extends Entity> {

    boolean Add(Context context, T item);

    void Remove(Context context, T item);

    void Update(Context context, T item);

    T Get(Context context, T item);

    ArrayList<T> GetAll(Context context, T item);
}
