package es.vivelibre.models.Utils;

import java.sql.Time;
import java.util.Date;

/**
 * Created by mfernandez on 27/04/2016.
 */
public class CardViewModel{
    private String tipo;
    private String medida;
    private int imagen;
    private String fecha_medida;
    private String valueType;

    public CardViewModel(String tipo, String medida, int imagen, String fecha_medida, String value_type)
    {
        this.tipo = tipo;
        this.medida= medida;
        this.imagen = imagen;
        this.fecha_medida= fecha_medida;
        this.valueType = value_type;
    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getFecha_medida() {
        return fecha_medida;
    }

    public void setFecha_medida(String fecha_medida) {
        this.fecha_medida = fecha_medida;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }
}
