package es.vivelibre.models.Utils;

import android.content.Context;

import es.vivelibre.models.SQLite.SQLiteService;

/**
 * Created by maca- on 16/05/2016.
 */
public class SQLiteModel {
    private static SQLiteModel INSTANCE = new SQLiteModel();

    public static SQLiteModel getInstance() {
        return INSTANCE;
    }

    public void borrarTablaMeterSQLite(Context context){
        SQLiteService.deleteMeterSQLite(context);
    }
    public void borrarTablaAlertSQLite(Context context) {
        SQLiteService.deleteAlertSQLite(context);
    }
    public void borrarTablaImagePrfileSQLite(Context context) {
        SQLiteService.deleteImageProfileSQLite(context);
    }
    public void borrarTablaMeterValueSQLite(Context context) {
        SQLiteService.deleteMeterValueSQLite(context);
    }
    public void borrarTablaProfileSQLite(Context context) {
        SQLiteService.deleteProfileSQLite(context);
    }
    public void borrarTablaHelperSQLite(Context context) {
        SQLiteService.deleteHelperSQLite(context);
    }

}
