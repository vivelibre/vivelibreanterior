package es.vivelibre.models.Utils;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.models.Meter.WeightValue;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class OAuthApi {

    public static final String API_PROFILE = "api/profile";
    public static final String API_METER_RESULT_CLIENTS = "api/meterresultclients";
    public static final String API_CLIENT_MONITOR_PARAMETERS = "api/clientmonitorparameters";
    public static final String API_METER_RESULT_CLIENTS_GET_ALL = "api/meterresultclients";
    public static final String API_MANUAL_METER_RESULT_CLIENTS = "api/manualmeterresultclients";
    public static final String API_IMAGE_PROFILE = "api/imageprofiles/";



    public static String GetAllMeterValuesByDatesResultClientApi(
            MeterValue meterValueType,
            OAuthRequestFilters oAuthRequestFilters) {
        String type;

        if (meterValueType instanceof HeartRateValue)
            type = "HeartRateValue";
        else if (meterValueType instanceof BloodPressureValue)
            type = "BloodPressureValue";
        else if (meterValueType instanceof OxygenSaturationValue)
            type = "OxygenSaturationValue";
        else if (meterValueType instanceof WeightValue)
            type = "WeightValue";
        else
            return null;
        return API_METER_RESULT_CLIENTS
                + "/"
                + oAuthRequestFilters.getResourceId()
                + "/"
                + DateFunctions.DateToTicks(oAuthRequestFilters
                .getDateIni()) +
                "/" + type + "/" +
                DateFunctions.DateToTicks(oAuthRequestFilters
                        .getDateFin());
    }

    public static String GetAllMeterResultClientApi(
            MeterValue meterValueType,
            OAuthRequestFilters oAuthRequestFilters) {
        String type;

        if (meterValueType instanceof HeartRateValue)
            type = "HeartRateValue";
        else if (meterValueType instanceof BloodPressureValue)
            type = "BloodPressureValue";
        else if (meterValueType instanceof OxygenSaturationValue)
            type = "OxygenSaturationValue";
        else if (meterValueType instanceof WeightValue)
            type = "WeightValue";
        else
            return null;
        return API_METER_RESULT_CLIENTS_GET_ALL
                + "/"
                + oAuthRequestFilters.getResourceId()
                + "/"
                + DateFunctions.DateToTicks(oAuthRequestFilters
                .getDateFin()) +
                "/" + type ;
    }

    public static String GetAllMeterValuesResultClientApi(
            MeterValue meterValueType,
            OAuthRequestFilters oAuthRequestFilters) {
        String type;

        if (meterValueType instanceof HeartRateValue)
            type = "HeartRateValue";
        else if (meterValueType instanceof BloodPressureValue)
            type = "BloodPressureValue";
        else if (meterValueType instanceof OxygenSaturationValue)
            type = "OxygenSaturationValue";
        else if (meterValueType instanceof WeightValue)
            type = "WeightValue";
        else
            return null;
        return API_METER_RESULT_CLIENTS_GET_ALL
                + "/"
                + oAuthRequestFilters.getResourceId()
                + "/"
                + DateFunctions.DateToTicks(oAuthRequestFilters
                .getDateIni()) + "/" +
                DateFunctions.DateToTicks(oAuthRequestFilters
                        .getDateFin());
    }

}
