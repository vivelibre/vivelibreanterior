package es.vivelibre.models.Utils;

import java.util.Date;
import java.util.UUID;

import es.vivelibre.models.Meter.MeterValueType;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class OAuthRequestFilters {

    public OAuthRequestFilters(UUID resourceId, Date dateIni) {
        setResourceId(resourceId);
        setDateIni(dateIni);
    }

    private UUID ResourceId;
    private Date DateIni;
    private Date DateFin;
    private String type;


    public UUID getResourceId() {
        return ResourceId;
    }

    public void setResourceId(UUID resourceId) {
        ResourceId = resourceId;
    }

    public Date getDateIni() {
        return DateIni;
    }

    public void setDateIni(Date dateIni) {
        DateIni = dateIni;
    }

    public OAuthRequestFilters(UUID resourceId, Date dateToFilterIni , Date dateToFilterFin) {
        setResourceId(resourceId);
        setDateIni(dateToFilterIni);
        setDateFin(dateToFilterFin);
    }
    public OAuthRequestFilters(UUID resourceId,String type, Date dateToFilterIni , Date dateToFilterFin) {
        setResourceId(resourceId);
        setDateIni(dateToFilterIni);
        setDateFin(dateToFilterFin);
        setType(type);
    }


    public Date getDateFin() {
        return DateFin;
    }

    public void setDateFin(Date dateFin) {
        DateFin = dateFin;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
