package es.vivelibre.models.Utils;

import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.CertificateException;

import static android.content.ContentValues.TAG;


/**
 * Created by mfernandez on 22/04/2016.
 */
public abstract class InstallTrustManager {
    public static void installHostTrustVerifier() {
        try {
            HttpsURLConnection
                    .setDefaultHostnameVerifier(new HostnameVerifier() {
                        public boolean verify(String hostname,
                                              SSLSession session) {
                            return true;
                        }
                    });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[] { new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            } }, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context
                    .getSocketFactory());
        } catch (Exception e) { // should never happen
            FirebaseCrash.logcat(Log.ERROR, TAG, "InstallTrustManager installHostTrustVerifier");
            FirebaseCrash.report(e);

            e.printStackTrace();
        }
    }
}
