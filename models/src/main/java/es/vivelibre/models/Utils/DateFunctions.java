package es.vivelibre.models.Utils;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.ContentValues.TAG;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class DateFunctions {

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final DateFormat DateTimeFormat = new SimpleDateFormat(
            DATE_FORMAT, new Locale("US"));

    @SuppressLint("SimpleDateFormat")
    public static String DateToString(Date date) {
        Format formatter = new SimpleDateFormat(DATE_FORMAT);
        return formatter.format(date);
    }

    public static Date StringToDate(String dateString) {
        try {
            return DateTimeFormat.parse(dateString);
        } catch (ParseException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "DateFunctions StringToDate");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    public static Date ParamsToDate(int year, int month, int day, int hour,
                                    int minute, int seconds) {
        String dateString = year + "-" + month + "-" + day + "T" + hour + ":"
                + minute + ":" + seconds;
        return StringToDate(dateString);

    }

    public static String GetFormatDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String day = (calendar.get(Calendar.DAY_OF_MONTH) < 10) ? ("0" + calendar
                .get(Calendar.DAY_OF_MONTH)) : ("" + calendar
                .get(Calendar.DAY_OF_MONTH));
        String month = ((calendar.get(Calendar.MONTH) + 1) < 10) ? ("0" + (calendar
                .get(Calendar.MONTH) + 1)) : ("" + (calendar
                .get(Calendar.MONTH) + 1));
        int year = calendar.get(Calendar.YEAR);
        return day + "-" + month + "-" + year;
    }

    public static String GetFormathour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String hour = (calendar.get(Calendar.HOUR_OF_DAY) < 10) ? ("0" + calendar
                .get(Calendar.HOUR_OF_DAY)) : ("" + calendar
                .get(Calendar.HOUR_OF_DAY));
        String minute = (calendar.get(Calendar.MINUTE) < 10) ? ("0" + calendar
                .get(Calendar.MINUTE)) : ("" + calendar.get(Calendar.MINUTE));
        String second = (calendar.get(Calendar.SECOND) < 10) ? ("0" + calendar
                .get(Calendar.SECOND)) : ("" + calendar.get(Calendar.SECOND));
        return hour + ":" + minute + ":" + second;
    }

    public static String DateToTicks(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int YEAR = calendar.get(Calendar.YEAR);
        int MONTH = calendar.get(Calendar.MONTH);
        int DAY_OF_MONTH = calendar.get(Calendar.DAY_OF_MONTH);
        int HOUR_OF_DAY = calendar.get(Calendar.HOUR_OF_DAY);
        int MINUTE = calendar.get(Calendar.MINUTE);
        int SECOND = calendar.get(Calendar.SECOND);

        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        cal.set(YEAR, MONTH, DAY_OF_MONTH, HOUR_OF_DAY, MINUTE, SECOND);
        cal.getTime().getTime();
        long date_ms_utc = cal.getTime().getTime();

        long TICKS_AT_EPOCH = 621355968000000000L;
        long TICKS_PER_MILLISECOND = 10000;
        long ticks = (date_ms_utc) * TICKS_PER_MILLISECOND + TICKS_AT_EPOCH;

        return Long.toString(ticks);
    }

}
