package es.vivelibre.models.Utils;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class OAuthConfig {
	/*
	 * Interna Producción
	 */
    // private static String ADDRESS = "https://172.30.112.35:25698/";

	/*
	 * Externa Producción
	 */
     //private static String ADDRESS = "https://app.vivelibre.es:25698/";

    /*
     * Externa Integración
     */
     private static String ADDRESS = "https://app.vivelibre.es:26589/";

	/*
	 * Interna Integración
	 */
    //private static String ADDRESS = "https://172.30.112.41:26589/";

    /*
     * Externa Integración Salud
     */
     //private static String ADDRESS = "https://app.vivelibre.es:7381/";

	/*
	 * Interna Integración Salud
	 */
    //private static String ADDRESS = "https://172.30.112.41:7381/";

	/*
	 * Externa Desarollo
	 */
    //private static String ADDRESS = "https://app.vivelibre.es:25691/";

	/*
	 * Local Mario
	 */
    //private static final String ADDRESS = "https://192.169.170.107:44305/";
    /*
	 * Local Roberto
	 */
    //private static final String ADDRESS = "https://192.169.170.103:44305/";

    /*
     * Local Jorge
     */
    //private static final String ADDRESS = "https://192.169.170.104:44305/";

    private static final String TOKEN_URL = ADDRESS + "OAuth20/Token";
    private static final String REDIRECT_URI = "http://localhost/demo";
    private static final String CONSUMER_KEY = "6999D6E5-C874-4526-8B95-E83C6A4B3C73";
    private static final String CONSUMER_SECRET = "YYEiyfehHDhsdf79dfhksd=sedruoe=HFH43434%%$KSHSghkHS&&Ehkdhfdf005434LJD==545=$jdlfjsdLJDlf";

    private static final int ACCESS_TOKEN_MAX_ATTEMPTS = 5;

    public static String getTOKEN_URL() {
        return TOKEN_URL;
    }

    public static String getREDIRECT_URI() {
        return REDIRECT_URI;
    }

    public static String getCONSUMER_KEY() {
        return CONSUMER_KEY;
    }

    public static String getCONSUMER_SECRET() {
        return CONSUMER_SECRET;
    }

    public static String getADDRESS() {
        return ADDRESS;
    }

    public static int getAccessTokenMaxAttempts() {
        return ACCESS_TOKEN_MAX_ATTEMPTS;
    }

}

