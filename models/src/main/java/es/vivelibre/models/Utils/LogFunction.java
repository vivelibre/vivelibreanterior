package es.vivelibre.models.Utils;

import android.util.Log;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class LogFunction {

    public static final boolean LOG_ACTIVE = true;
    public static final String DEBUG_LOG = "DEBUG_LOG";
    public static final String WARN_LOG = "WARN_LOG";
    public static final String ERROR_LOG = "ERROR_LOG";
    public static final String INFO_LOG = "INFO_LOG";
    public static final String VERBOSE_LOG = "VERBOSE_LOG";

    public static void writeLogCat(String TAG, String level, String msg) {
        if (LOG_ACTIVE) {
            switch (level) {
                case DEBUG_LOG:
                    Log.d(TAG, msg);
                    break;
                case WARN_LOG:
                    Log.w(TAG, msg);
                    break;
                case ERROR_LOG:
                    Log.e(TAG, msg);
                    break;
                case INFO_LOG:
                    Log.i(TAG, msg);
                    break;
                case VERBOSE_LOG:
                    Log.v(TAG, msg);
                    break;
                default:
                    break;
            }
        }
    }
}
