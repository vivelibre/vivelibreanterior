package es.vivelibre.models.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IOAuthHttpClient {

    HttpResponse HttpGetRequest(String api, String accessToken);

    HttpResponse HttpPutRequest(String api, String accessToken, StringEntity value);

    HttpResponse HttpPostRequest(String api, String accessToken, StringEntity value);

    HttpResponse HttpDeleteRequest(String api, String accessToken);

}
