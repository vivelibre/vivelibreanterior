package es.vivelibre.models.Utils;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class Messages {
    public static final String warning_CannotPerfomOAuthRequestWithEmptyRequest = "warning_CannotPerfomOAuthRequestWithEmptyRequest";
    public static final String warning_CannotPerfomOAuthRequestWithEmptyResponse = "warning_CannotPerfomOAuthRequestWithEmptyResponse";
    public static final String warning_CannotPerfomOAuthRequestWithWrongHttpStatusCode = "warning_CannotPerfomOAuthRequestWithWrongHttpStatusCode";
    public static final String warning_CannotPerfomOAuthRequestWithNotValidAccessToken = "warning_CannotPerfomOAuthRequestWithNotValidAccessToken";
    public static final String warning_CannotPerfomLoginOAuthRequestWithEmptyRequest = "warning_CannotPerfomLoginOAuthRequestWithEmptyRequest";
    public static final String warning_CannotPerfomLoginOAuthRequestWithNotValidAccessToken = "warning_CannotPerfomLoginOAuthRequestWithNotValidAccessToken";
    public static final String warning_CannotCreateProfileWithEmptyInformation = "warning_CannotCreateProfileWithEmptyInformation";
    public static final String warning_CannotCreateProfileIfAlreadyExist = "warning_CannotCreateProfileIfAlreadyExist";
    public static final String warning_CannotUpdateProfileWithEmptyInformation ="warning_CannotUpdateProfileWithEmptyInformation";
    public static final String warning_CannotLoginWithEmptyInformation ="warning_CannotLoginWithEmptyInformation";
    public static final String warning_CannotConvertJsonToDTOWithWrongAlertType ="warning_CannotConvertJsonToDTOWithWrongAlertType";
    public static final String warning_CannotConvertJsonToDTOWithWrongMeterType ="warning_CannotConvertJsonToDTOWithWrongMeterType";
    public static final String warning_CannotConvertJsonToDTOWithWrongMeterValueType ="warning_CannotConvertJsonToDTOWithWrongMeterValueType";
    public static final String warning_CannotGetProfileWithEmptyInformation ="warning_CannotGetProfileWithEmptyInformation";
    public static final String warning_CannotCreateImageProfileWithEmptyInformation = "warning_CannotCreateImageProfileWithEmptyInformation";
    public static final String warning_CannotCreateImageProfileIfAlreadyExist = "warning_CannotCreateImageProfileIfAlreadyExist";
    public static final String warning_CannotGetMeterValueWithEmptyClientProfileInformation ="warning_CannotGetMeterValueWithEmptyClientProfileInformation";
    public static final String warning_CannotGetClientMeterValuesWithEmptyInformation ="warning_CannotGetClientMeterValuesWithEmptyInformation";
    public static final String warning_CannotGetClientMonitorParametersWithEmptyInformation = "warning_CannotGetClientMonitorParametersWithEmptyInformation";
    public static final String warning_CannotAddNewMeterValueWithEmptyInformation = "warning_CannotAddNewMeterValueWithEmptyInformation";
    public static final String warning_CannotAddNewMeterValueWithEmptyClientProfileInformation = "warning_CannotAddNewMeterValueWithEmptyClientProfileInformation";
    public static final String warning_CannotUpdateProfileImageWithEmptyInformation = "warning_CannotCreateProfileImageWithEmptyInformation";
}
