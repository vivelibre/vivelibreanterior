package es.vivelibre.models.Utils;

/**
 * Created by mfernandez on 22/04/2016.
 */
public final class OAuthScopes {
    public static final String READ_PROFILE = "Read.Profile";
    public static final String READ_METER_RESULT_CLIENT = "Read.MeterResultClient";
    public static final String CREATE_METER_RESULT_CLIENT = "Create.MeterResultClient";
    public static final String READ_CLIENT_MONITOR_PARAMETERS = "Read.ClientMonitorParameters";
    public final static String CREATE_IMAGE_PROFILE = "Create.ImageProfile";

    public static final String ALL_APP_SCOPES = READ_PROFILE + " "
            + READ_METER_RESULT_CLIENT + " " + CREATE_METER_RESULT_CLIENT + " "
            + READ_CLIENT_MONITOR_PARAMETERS + " " + CREATE_IMAGE_PROFILE;

}
