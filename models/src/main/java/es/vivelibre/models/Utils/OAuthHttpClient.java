package es.vivelibre.models.Utils;

import android.util.Log;
import android.util.Pair;

import com.google.firebase.crash.FirebaseCrash;

import org.apache.amber.oauth2.client.OAuthClient;
import org.apache.amber.oauth2.client.URLConnectionClient;
import org.apache.amber.oauth2.client.request.OAuthClientRequest;
import org.apache.amber.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.amber.oauth2.common.exception.OAuthProblemException;
import org.apache.amber.oauth2.common.exception.OAuthSystemException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import static android.content.ContentValues.TAG;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class OAuthHttpClient implements IOAuthHttpClient {

    public Pair<String, String> GetAccessToken(OAuthClientRequest request) {
        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
        OAuthAccessTokenResponse response;
        try {
            response = oAuthClient.accessToken(request, "POST");

            return new Pair<String, String>(response.getRefreshToken(),
                    response.getAccessToken());
        } catch (OAuthSystemException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetAccessToken");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } catch (OAuthProblemException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetAccessToken");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse HttpGetRequest(String api, String accessToken) {
        try {
            DefaultHttpClient httpClient = GetDefaultHttpClient();
            HttpGet get = new HttpGet(OAuthConfig.getADDRESS() + api);
            get.setHeader("Authorization", "Bearer " + accessToken.toString());
            get.addHeader("Authorization", "Bearer " + accessToken.toString());

            return httpClient.execute(get);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient HttpGetRequest");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse HttpPutRequest(String api, String accessToken,
                                       StringEntity value) {
        try {
            DefaultHttpClient httpClient = GetDefaultHttpClient();

            HttpPut put = new HttpPut(api);
            put.addHeader("Authorization", "Bearer " + accessToken.toString());
            put.addHeader("Content-type", "application/json");

            put.setEntity(value);
            return httpClient.execute(put);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient HttpPutRequest");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse HttpPostRequest(String api, String accessToken,
                                        StringEntity value) {
        try {
            DefaultHttpClient httpClient = GetDefaultHttpClient();

            HttpPost post = new HttpPost(OAuthConfig.getADDRESS() + api);
            post.addHeader("Authorization", "Bearer " + accessToken.toString());
            post.addHeader("Content-type", "application/json");

            post.setEntity(value);
            return httpClient.execute(post);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient HttpPostRequest");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    public HttpResponse HttpDeleteRequest(String api, String accessToken) {
        try {
            DefaultHttpClient httpClient = GetDefaultHttpClient();

            HttpDelete delete = new HttpDelete(OAuthConfig.getADDRESS() + api);
            delete.setHeader("Authorization",
                    "Bearer " + accessToken.toString());
            delete.addHeader("Authorization",
                    "Bearer " + accessToken.toString());

            return httpClient.execute(delete);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient HttpDeleteRequest");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    private DefaultHttpClient GetDefaultHttpClient() {
        KeyStore trustStore;
        try {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams paramss = new BasicHttpParams();
            HttpProtocolParams.setVersion(paramss, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(paramss, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 25693));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    paramss, registry);

            return new DefaultHttpClient(ccm, paramss);
        } catch (KeyStoreException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetDefaultHttpClient");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetDefaultHttpClient");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } catch (CertificateException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetDefaultHttpClient");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } catch (IOException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetDefaultHttpClient");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } catch (KeyManagementException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetDefaultHttpClient");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "OAuthHttpClient GetDefaultHttpClient");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }
}

