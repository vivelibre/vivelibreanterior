package es.vivelibre.models.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mfernandez on 22/04/2016.
 */
public final class SharedCustomPreferencesUtils {

    private final static String PREFS_CLIENT = "technology.vivelibre.health.client.preferences.PREFS_CLIENT";

    private final static String SHARED_CUSTOM_PREF_PUSH_ID = "pref_pushId";
    private final static String SHARED_CUSTOM_PREF_SECURITY_LOCK = "pref_securityLock";
    private final static String SHARED_CUSTOM_PREF_SECURITY_LOCK_UNCONFIRMED = "pref_securityLockUnconfirmed";
    private final static String SHARED_CUSTOM_PREF_REGISTRATION_ID = "pref_registrationId";
    private final static String SHARED_CUSTOM_PREF_APP_VERSION = "pref_appVersion";
    private final static String SHARED_CUSTOM_PREF_SERVER_EXPIRATION_TIME = "pref_onServerExpirationTimeMs";
    private final static String SHARED_CUSTOM_PREF_REFRESH_TOKEN = "pref_refreshToken";
    private final static String SHARED_CUSTOM_PREF_ACCESS_TOKEN_DATA = "pref_accessTokenData";
    private final static String SHARED_CUSTOM_PREF_ACCESS_TOKEN_DATE = "pref_accessTokenDate";

    /*
     * PUSH ID
     */
    public static String getPushId(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getString(SHARED_CUSTOM_PREF_PUSH_ID, "");
    }

    public static void setPushId(Context context, String newPushId) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor.putString(SHARED_CUSTOM_PREF_PUSH_ID, newPushId);
        stateEditor.commit();
    }

    /*
     * REFRESH TOKEN
     */
    public static String getRefreshToken(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getString(SHARED_CUSTOM_PREF_REFRESH_TOKEN, "");
    }

    public static void setRefreshToken(Context context, String newRefreshToken) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor
                .putString(SHARED_CUSTOM_PREF_REFRESH_TOKEN, newRefreshToken);
        stateEditor.commit();
    }

    /*
     * ACCESS TOKEN DATA
     */
    public static String getAccessTokenData(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getString(SHARED_CUSTOM_PREF_ACCESS_TOKEN_DATA, "");
    }

    public static void setAccessTokenData(Context context,
                                          String newAccessTokenData) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor.putString(SHARED_CUSTOM_PREF_ACCESS_TOKEN_DATA,
                newAccessTokenData);
        stateEditor.commit();
    }

    /*
     * ACCESS TOKEN DATE
     */
    public static long getAccessTokenDate(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getLong(SHARED_CUSTOM_PREF_ACCESS_TOKEN_DATE, 0);
    }

    public static void setAccessTokenDate(Context context,
                                          long newAccessTokenDate) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor.putLong(SHARED_CUSTOM_PREF_ACCESS_TOKEN_DATE,
                newAccessTokenDate);
        stateEditor.commit();
    }

    /*
     * REGISTRATION ID
     */
    public static String getRegistrationId(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getString(SHARED_CUSTOM_PREF_REGISTRATION_ID, "");
    }

    public static void setRegistrationId(Context context,
                                         String newRegistrationId) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor.putString(SHARED_CUSTOM_PREF_REGISTRATION_ID,
                newRegistrationId);
        stateEditor.commit();
    }

    /*
     * APP VERSION
     */
    public static int getAppVersion(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getInt(SHARED_CUSTOM_PREF_APP_VERSION,
                Integer.MIN_VALUE);
    }

    public static void setAppVersion(Context context, int newAppVersion) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor.putInt(SHARED_CUSTOM_PREF_APP_VERSION, newAppVersion);
        stateEditor.commit();
    }

    /*
     * SERVER EXPIRATION TIME
     */
    public static long getServerExpirationTime(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getLong(SHARED_CUSTOM_PREF_SERVER_EXPIRATION_TIME,
                -1);
    }

    public static void setServerExpirationTime(Context context,
                                               long newServerExpirationTime) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor.putLong(SHARED_CUSTOM_PREF_SERVER_EXPIRATION_TIME,
                newServerExpirationTime);
        stateEditor.commit();
    }

    /*
     * SECURITY LOCK
     */
    public static String getSecurityLock(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getString(SHARED_CUSTOM_PREF_SECURITY_LOCK, "");
    }

    public static void setSecurityLock(Context context, String newSecurityLock) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor
                .putString(SHARED_CUSTOM_PREF_SECURITY_LOCK, newSecurityLock);
        stateEditor.commit();
    }

    /*
     * SECURITY LOCK UNCONFIRMED
     */
    public static String getSecurityLockUnconfirmed(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        return sharedPrefs.getString(
                SHARED_CUSTOM_PREF_SECURITY_LOCK_UNCONFIRMED, "");
    }

    public static void setSecurityLockUnconfirmed(Context context,
                                                  String newSecurityLockUnconfirmed) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();

        stateEditor.putString(SHARED_CUSTOM_PREF_SECURITY_LOCK_UNCONFIRMED,
                newSecurityLockUnconfirmed);
        stateEditor.commit();
    }

    /*
     * CLEAR PREFERENCES
     */
    public static void clearPreferences(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREFS_CLIENT, Context.MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = sharedPrefs.edit();
        stateEditor.clear();
        stateEditor.commit();
    }
}

