package es.vivelibre.models.Utils;

import android.content.Context;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class AccessToken {
    private static AccessToken INSTANCE = new AccessToken();
    private static final long EXPIRED_TIME = 100 * 1000;

    public static AccessToken getInstance() {
        return INSTANCE;
    }

    public String getData(Context context) {
        return SharedCustomPreferencesUtils.getAccessTokenData(context);
    }

    public void setData(Context context, String data) {
        SharedCustomPreferencesUtils.setAccessTokenData(context, data);
    }

    public long getDate(Context context) {
        return SharedCustomPreferencesUtils.getAccessTokenDate(context);
    }

    public void setDate(Context context, long date) {
        SharedCustomPreferencesUtils.setAccessTokenDate(context, date);
    }

    public static long getExpiredTime() {
        return EXPIRED_TIME;
    }

    public boolean isValid(Context context, long time) {
        if (getData(context) != null && !getData(context).equals("")) {
            if (time - getDate(context) < EXPIRED_TIME) {
                return true;
            }
        }
        return false;
    }
}

