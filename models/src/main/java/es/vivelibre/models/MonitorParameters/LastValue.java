package es.vivelibre.models.MonitorParameters;

import java.util.Date;
import java.util.UUID;

/**
 * Created by mfernandez on 17/05/2016.
 */
public class LastValue {
    private String value;
    private Date dateRead;
    private UUID idValue;

    public LastValue(){

    }

    public LastValue(String value , Date dateRead, UUID idValue){
        this.value = value;
        this.dateRead = dateRead;
        this.idValue = idValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getDateRead() {
        return dateRead;
    }

    public void setDateRead(Date dateRead) {
        this.dateRead = dateRead;
    }

    public UUID getIdValue() {
        return idValue;
    }

    public void setIdValue(UUID idValue) {
        this.idValue = idValue;
    }
}
