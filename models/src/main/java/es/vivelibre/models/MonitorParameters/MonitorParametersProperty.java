package es.vivelibre.models.MonitorParameters;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class MonitorParametersProperty {
    private boolean Active;
    private boolean Reminder;

    public MonitorParametersProperty(boolean active, boolean reminder) {
        this.Active = active;
        this.Reminder = reminder;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public boolean isReminder() {
        return Reminder;
    }

    public void setReminder(boolean reminder) {
        Reminder = reminder;
    }
}
