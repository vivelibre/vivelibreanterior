package es.vivelibre.models.MonitorParameters;

import java.util.UUID;

import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class MonitorParameters extends Entity {
    public MonitorParametersProperty weight;
    public MonitorParametersProperty bloodPressure;
    public MonitorParametersProperty heartRate;
    public MonitorParametersProperty bloodOxygenSaturation;

    private String weightType;
    private String bloodPressureType;
    private String heartRateType;
    private String bloodOxygenSaturationType;

    private LastValue weightLastValue;
    private LastValue bloodPressureLastValue;
    private LastValue heartRateLastValue;
    private LastValue bloodOxygenSaturationLastValue;


    public MonitorParametersProperty getWeight() {
        return weight;
    }

    public void setWeight(MonitorParametersProperty weight) {
        this.weight = weight;
    }

    public MonitorParametersProperty getBloodPreassure() {
        return bloodPressure;
    }

    public void setBloodPreassure(MonitorParametersProperty bloodPreassure) {
        this.bloodPressure = bloodPreassure;
    }

    public MonitorParametersProperty getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(MonitorParametersProperty heartRate) {
        this.heartRate = heartRate;
    }

    public MonitorParametersProperty getBloodOxygenSaturation() {
        return bloodOxygenSaturation;
    }

    public void setBloodOxygenSaturation(
            MonitorParametersProperty bloodOxygenSaturation) {
        this.bloodOxygenSaturation = bloodOxygenSaturation;
    }

    public String getWeightType() {
        return weightType;
    }

    public void setWeightType(String weightType) {
        this.weightType = weightType;
    }

    public String getBloodPressureType() {
        return bloodPressureType;
    }

    public void setBloodPressureType(String bloodPressureType) {
        this.bloodPressureType = bloodPressureType;
    }

    public String getHeartRateType() {
        return heartRateType;
    }

    public void setHeartRateType(String heartRateType) {
        this.heartRateType = heartRateType;
    }

    public String getBloodOxygenSaturationType() {
        return bloodOxygenSaturationType;
    }

    public void setBloodOxygenSaturationType(String bloodOxygenSaturationType) {
        this.bloodOxygenSaturationType = bloodOxygenSaturationType;
    }


    public LastValue getWeightLastValue() {
        return weightLastValue;
    }

    public void setWeightLastValue(LastValue weightLastValue) {
        this.weightLastValue = weightLastValue;
    }

    public LastValue getBloodPressureLastValue() {
        return bloodPressureLastValue;
    }

    public void setBloodPressureLastValue(LastValue bloodPressureLastValue) {
        this.bloodPressureLastValue = bloodPressureLastValue;
    }

    public LastValue getHeartRateLastValue() {
        return heartRateLastValue;
    }

    public void setHeartRateLastValue(LastValue heartRateLastValue) {
        this.heartRateLastValue = heartRateLastValue;
    }

    public LastValue getBloodOxygenSaturationLastValue() {
        return bloodOxygenSaturationLastValue;
    }

    public void setBloodOxygenSaturationLastValue(LastValue bloodOxygenSaturationLastValue) {
        this.bloodOxygenSaturationLastValue = bloodOxygenSaturationLastValue;
    }
}
