package es.vivelibre.models.Meter;

/**
 * Created by mfernandez on 22/04/2016.
 */
public enum MeterValueType {
    WEIGHT(1), BLOOD_PRESSURE(2), HEART_RATE(3), OXYGEN_SATURATION(4);

    private int value;

    private MeterValueType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}