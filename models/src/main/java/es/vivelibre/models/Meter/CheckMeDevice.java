package es.vivelibre.models.Meter;

/**
 * Created by mfernandez on 12/05/2016.
 */
public class CheckMeDevice {
    private String Region;
    private String Model;
    private String HardwareVer;
    private String SoftwareVer;
    private String LanguageVer;
    private String CurLanguage;
    private String FileVer;
    private String SPCPVer;
    private String Application;
    private String SN;

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getHardwareVer() {
        return HardwareVer;
    }

    public void setHardwareVer(String hardwareVer) {
        HardwareVer = hardwareVer;
    }

    public String getSoftwareVer() {
        return SoftwareVer;
    }

    public void setSoftwareVer(String softwareVer) {
        SoftwareVer = softwareVer;
    }

    public String getLanguageVer() {
        return LanguageVer;
    }

    public void setLanguageVer(String languageVer) {
        LanguageVer = languageVer;
    }

    public String getCurLanguage() {
        return CurLanguage;
    }

    public void setCurLanguage(String curLanguage) {
        CurLanguage = curLanguage;
    }

    public String getFileVer() {
        return FileVer;
    }

    public void setFileVer(String fileVer) {
        FileVer = fileVer;
    }

    public String getSPCPVer() {
        return SPCPVer;
    }

    public void setSPCPVer(String sPCPVer) {
        SPCPVer = sPCPVer;
    }

    public String getApplication() {
        return Application;
    }

    public void setApplication(String application) {
        Application = application;
    }

    public String getSerialNumber() {
        return SN;
    }

    public void setSerialNumber(String sN) {
        SN = sN;
    }

}
