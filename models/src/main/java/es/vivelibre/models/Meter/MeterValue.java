package es.vivelibre.models.Meter;

import android.database.Cursor;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;

import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.SQLite.SQLiteMeterValueUtils;
import es.vivelibre.models.Utils.DateFunctions;

import static android.content.ContentValues.TAG;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class MeterValue extends Entity {
    private String meter_id;
    private UUID client_id;
    private String value;
    private Date dateRead;
    private Date dateSend;
    private int type;
    private UUID alert_id;
    private int error;
    private boolean validate;

    public MeterValue() {

    }

    public MeterValue(Cursor cursor) {
        super.ChangeCurrentIdentity(UUID.fromString(cursor.getString(0)));
        setMeterId(cursor.getString(1));
        setAlert_id((!cursor.getString(2).equals("")) ? UUID.fromString(cursor
                .getString(2)) : null);
        setValue(cursor.getString(3));
        setDateRead(new Date(cursor.getLong(4)));
        setDateSend(new Date(cursor.getLong(5)));
        setType(cursor.getInt(6));
        setError(cursor.getInt(7));
        setClient_id(UUID.fromString(cursor.getString(8)));
    }

    public MeterValue(UUID id, Date dateSend, boolean error) {
        ChangeCurrentIdentity(id);
        setDateSend(dateSend);
        setError((error) ? 1 : 0);
    }

    public String getMeter_id() {
        return meter_id;
    }

    public void setMeterId(String meter_id) {
        this.meter_id = meter_id;
    }

    public UUID getClient_id() {
        return client_id;
    }

    public void setClient_id(UUID client_id) {
        this.client_id = client_id;
    }

    public String getValue() {
        return value;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getDateRead() {
        return dateRead;
    }

    public void setDateRead(Date dateRead) {
        this.dateRead = dateRead;
    }

    public Date getDateSend() {
        return dateSend;
    }

    public void setDateSend(Date dateSend) {
        this.dateSend = dateSend;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDateReadString() {
        return DateFunctions.DateToString(dateRead);
    }

    public String getDateSendString() {
        return DateFunctions.DateToString(dateSend);
    }

    public UUID getAlert_id() {
        return alert_id;
    }

    public void setAlert_id(UUID alert_id) {
        this.alert_id = alert_id;
    }

    public int getError() {
        return error;
    }

    public void setError(int err) {
        this.error = err;
    }

    public String GetSpecification() {
        if (getId() == null && meter_id == null && client_id == null
                && value == null && dateRead == null && dateSend == null
                && String.valueOf(type) == null && alert_id == null && String.valueOf(error) == null)
            return null;

        String id_spec = (getId() != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableId() + "=?"
                : "";
        String meter_id_spec = (meter_id != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableMeterId()
                + "=?" : "";
        String client_id_spec = (client_id != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableClientId()
                + "=?" : "";
        String value_spec = (value != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableValue() + "=?"
                : "";
        String dateRead_spec = (dateRead != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableDateRead()
                + "=?" : "";
        String dateSend_spec = (dateSend != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableDateSend()
                + "=?" : "";
        String type_spec = (String.valueOf(type) != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableType() + "=?"
                : "";
        String alert_id_spec = (alert_id != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableAlertId()
                + "=?" : "";
        String error_spec = (String.valueOf(error) != null) ? " AND "
                + SQLiteMeterValueUtils.getMeterValueSqliteTableError() + "=?"
                : "";

        String spec = id_spec.concat(meter_id_spec).concat(client_id_spec)
                .concat(value_spec).concat(dateRead_spec).concat(dateSend_spec)
                .concat(type_spec).concat(alert_id_spec).concat(error_spec);

        return spec.substring(5, spec.length());
    }

    public String[] GetSpecificationArgs() {
        if (getId() == null && meter_id == null && client_id == null
                && value == null && dateRead == null && dateSend == null
                && String.valueOf(type) == null && alert_id == null && String.valueOf(error) == null)
            return null;

        String id_arg = (getId() != null) ? getId() + "," : "";
        String meter_id_arg = (meter_id != null) ? meter_id + "," : "";
        String client_id_arg = (client_id != null) ? client_id + "," : "";
        String value_arg = (value != null) ? value + "," : "";
        String dateRead_arg = (dateRead != null) ? dateRead + "," : "";
        String dateSend_arg = (dateSend != null) ? dateSend + "," : "";
        String type_arg = (String.valueOf(type) != null) ? type + "," : "";
        String alert_id_arg = (alert_id != null) ? alert_id + "," : "";
        String error_arg = (String.valueOf(error) != null) ? error + "," : "";

        String args = id_arg.concat(meter_id_arg).concat(client_id_arg)
                .concat(value_arg).concat(dateRead_arg).concat(dateSend_arg)
                .concat(type_arg).concat(alert_id_arg).concat(error_arg);

        return new String[] { args.substring(0, args.length() - 1) };
    }


    private static MeterValue getMeterValueFromCursor(Cursor cursor) {
        MeterValue meterValue = null;
        int type = cursor.getInt(6);
        if (MeterValueType.BLOOD_PRESSURE.getValue() == type) {
            meterValue = new BloodPressureValue(cursor);
            return (meterValue.getType() == MeterValueType.BLOOD_PRESSURE
                    .getValue()) ? meterValue : null;
        } else if (MeterValueType.HEART_RATE.getValue() == type) {
            meterValue = new HeartRateValue(cursor);
            return (meterValue.getType() == MeterValueType.HEART_RATE
                    .getValue()) ? meterValue : null;
        } else if (MeterValueType.OXYGEN_SATURATION.getValue() == type) {
            meterValue = new OxygenSaturationValue(cursor);
            return (meterValue.getType() == MeterValueType.OXYGEN_SATURATION
                    .getValue()) ? meterValue : null;
        } else if (MeterValueType.WEIGHT.getValue() == type) {
            meterValue = new WeightValue(cursor);
            return (meterValue.getType() == MeterValueType.WEIGHT.getValue()) ? meterValue
                    : null;
        }
        return null;
    }

    public static StringEntity GetMeterValueBinding(
            MeterValue meterValue) {
        StringEntity value = null;
        try {
            String meterValueString = new Gson().toJson(meterValue);
            JSONObject meterValueJSON;

            meterValueJSON = new JSONObject(meterValueString);
            meterValueJSON.accumulate("MeterValueDTO", meterValue.getClass()
                    .getSimpleName());
            value = new StringEntity(meterValueJSON.toString(), HTTP.UTF_8);
            value.setContentType("application/json");

            return value;

        } catch (UnsupportedEncodingException e1) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValue GetMeterValueBinding");
            FirebaseCrash.report(e1);
            e1.printStackTrace();
        } catch (JSONException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValue GetMeterValueBinding");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    public static StringEntity GetMeterValueListBinding(
            ArrayList<MeterValueComplete> meterValuesComplete) {
        StringEntity value = null;
        try {
            JSONArray meterValuesJSON = new JSONArray();
            Iterator<MeterValueComplete> it = meterValuesComplete
                    .iterator();
            while (it.hasNext()) {
                MeterValue meterValue = ((MeterValueComplete) it
                        .next()).getMeterValue();
                String result_string = new Gson().toJson(meterValue);
                JSONObject meterValueJSON = new JSONObject(result_string);
                meterValueJSON.accumulate("MeterValueDTO", meterValue
                        .getClass().getSimpleName());
                meterValuesJSON.put(meterValueJSON);
            }
            value = new StringEntity(meterValuesJSON.toString(), HTTP.UTF_8);
            value.setContentType("application/json");

            return value;

        } catch (UnsupportedEncodingException e1) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValue GetMeterValueListBinding");
            FirebaseCrash.report(e1);
            e1.printStackTrace();
        } catch (JSONException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValue GetMeterValueListBinding");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        return null;
    }

    public static MeterValue generateMeterValueFromCursor(Cursor cursor) {
        MeterValue meterValue = null;
        if (cursor.moveToFirst()) {
            meterValue = getMeterValueFromCursor(cursor);
        }
        cursor.close();
        return meterValue;
    }

    public static ArrayList<Entity> GenerateListMeterValuesFromCursor(
            Cursor cursor, Entity item) {
        if (cursor.moveToFirst()) {
            ArrayList<Entity> list_meterValues = new ArrayList<Entity>();
            do {
                MeterValue meterValue = getMeterValueFromCursor(cursor);
                if (meterValue != null)
                    list_meterValues.add(meterValue);
            } while (cursor.moveToNext());
            cursor.close();
            return list_meterValues;
        }
        cursor.close();
        return null;
    }
    public static WeightValue CreateWeightValue(UUID clientId, String value,
                                                String serialNumber, Date dateRead, boolean error) {

        WeightValue weightValue = new WeightValue();
        weightValue.setMeterId(serialNumber);
        weightValue.setClient_id(clientId);
        weightValue.setValue(value);
        weightValue.setDateRead(dateRead);
        weightValue.setDateSend(new Date());
        weightValue.setType(MeterValueType.WEIGHT.getValue());
        weightValue.setError((error) ? 1 : 0);

        weightValue.GenerateNewIdentity();

        return weightValue;
    }

    public static MeterValue CreateHeartRateValue(UUID clientId, String value,
                                                  String serialNumber, Date dateRead, boolean error) {

        HeartRateValue heartRateValue = new HeartRateValue();
        heartRateValue.setMeterId(serialNumber);
        heartRateValue.setClient_id(clientId);
        heartRateValue.setValue(value);
        heartRateValue.setDateRead(dateRead);
        heartRateValue.setDateSend(new Date());
        heartRateValue.setType(MeterValueType.HEART_RATE.getValue());
        heartRateValue.setError(0);

        heartRateValue.GenerateNewIdentity();

        return heartRateValue;
    }

    public static MeterValue CreateOxygenSaturationValue(UUID clientId,
                                                         String value, String serialNumber, Date dateRead, boolean error) {

        OxygenSaturationValue oxygenSaturationValue = new OxygenSaturationValue();
        oxygenSaturationValue.setMeterId(serialNumber);
        oxygenSaturationValue.setClient_id(clientId);
        oxygenSaturationValue.setValue(value);
        oxygenSaturationValue.setDateRead(dateRead);
        oxygenSaturationValue.setDateSend(new Date());
        oxygenSaturationValue.setType(MeterValueType.OXYGEN_SATURATION
                .getValue());
        oxygenSaturationValue.setError((error) ? 1 : 0);

        oxygenSaturationValue.GenerateNewIdentity();

        return oxygenSaturationValue;
    }

    public static MeterValue CreateBloodPressureValue(UUID clientId,
                                                      String value, String serialNumber, Date dateRead, boolean error) {

        BloodPressureValue bloodPressureValue = new BloodPressureValue();
        bloodPressureValue.setMeterId(serialNumber);
        bloodPressureValue.setClient_id(clientId);
        bloodPressureValue.setValue(value);
        bloodPressureValue.setDateRead(dateRead);
        bloodPressureValue.setDateSend(new Date());
        bloodPressureValue.setType(MeterValueType.BLOOD_PRESSURE.getValue());
        bloodPressureValue.setError((error) ? 1 : 0);

        bloodPressureValue.GenerateNewIdentity();

        return bloodPressureValue;
    }

    public boolean isError() {
        if (error != 0)
            return true;
        else
            return false;
    }
}

