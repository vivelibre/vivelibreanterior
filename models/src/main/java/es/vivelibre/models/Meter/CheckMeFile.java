package es.vivelibre.models.Meter;

/**
 * Created by mfernandez on 12/05/2016.
 */
public class CheckMeFile {
    private String FileName;
    private byte FileType;
    private byte[] FileBuf;

    public CheckMeFile(String fileName, byte fileType, byte[] fileBuf) {
        setFileName(fileName);
        setFileType(fileType);
        setFileBuf(fileBuf);
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public byte getFileType() {
        return FileType;
    }

    public void setFileType(byte fileType) {
        FileType = fileType;
    }

    public byte[] getFileBuf() {
        return FileBuf;
    }

    public void setFileBuf(byte[] fileBuf) {
        FileBuf = fileBuf;
    }

}
