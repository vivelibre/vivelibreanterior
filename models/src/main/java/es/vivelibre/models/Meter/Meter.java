package es.vivelibre.models.Meter;

import java.util.UUID;

import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 22/04/2016.
 */
public abstract class Meter extends Entity {
    private String brand;
    private String model;
    private String serialNumber;
    private int type;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
