package es.vivelibre.models.Meter;

import java.util.EmptyStackException;

import es.vivelibre.models.Alert.MeterAlert;
import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class MeterValueComplete extends Entity{
    private Meter meter;
    private MeterAlert alert;
    private MeterValue meterValue;

    public MeterValueComplete(Meter meter, MeterValue meterValue,
                                 MeterAlert alert) {
        setMeter(meter);
        setMeterValue(meterValue);
        setAlert(alert);
    }

    public Meter getMeter() {
        return meter;
    }

    public void setMeter(Meter meter) {
        this.meter = meter;
    }

    public MeterAlert getAlert() {
        return alert;
    }

    public void setAlert(MeterAlert alert) {
        this.alert = alert;
    }

    public MeterValue getMeterValue() {
        return meterValue;
    }

    public void setMeterValue(MeterValue meterValue) {
        this.meterValue = meterValue;
    }


}
