package es.vivelibre.models.Alert;

import java.util.UUID;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class MeterAlert extends Alert {
    private UUID MeterId;

    public UUID getMeterId() {
        return MeterId;
    }

    public void setMeterId(UUID meterId) {
        MeterId = meterId;
    }


}