package es.vivelibre.models.Alert;

import java.util.Date;
import java.util.UUID;

import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 22/04/2016.
 */
public abstract class Alert extends Entity {
    private Date Date_Create;
    private String State;
    private UUID ClientId;
    private int Type;
    private int Subtype;
    private Date Date_Close;
    private String DescriptionClient;
    private int Level;

    public int getLevel() {
        return Level;
    }

    public void setLevel(int level) {
        Level = level;
    }

    public Date getDate_Create() {
        return Date_Create;
    }

    public void setDate_Create(Date date_Create) {
        Date_Create = date_Create;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public UUID getClientId() {
        return ClientId;
    }

    public void setClientId(UUID clientId) {
        ClientId = clientId;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public Date getDate_Close() {
        return Date_Close;
    }

    public void setDate_Close(Date date_Close) {
        Date_Close = date_Close;
    }

    public String getDescriptionClient() {
        return DescriptionClient;
    }

    public void setDescriptionClient(String descriptionClient) {
        DescriptionClient = descriptionClient;
    }

    public int getSubtype() {
        return Subtype;
    }

    public void setSubtype(int subtype) {
        Subtype = subtype;
    }


}
