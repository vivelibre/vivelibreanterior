package es.vivelibre.services.MonitorParameters;

import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.services.RestServiceOAuth;

/**
 * Created by mfernandez on 27/04/2016.
 */
public class MonitorParametersRestServiceOAuth extends
        RestServiceOAuth<MonitorParameters> implements
        IMonitorParametersRestServiceOAuth {

    private static MonitorParametersRestServiceOAuth INSTANCE = new MonitorParametersRestServiceOAuth();

    public static MonitorParametersRestServiceOAuth getInstance() {
        return INSTANCE;
    }

}
