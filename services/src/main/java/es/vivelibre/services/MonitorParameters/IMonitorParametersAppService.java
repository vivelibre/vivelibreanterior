package es.vivelibre.services.MonitorParameters;

import android.content.Context;

import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 27/04/2016.
 */
public interface IMonitorParametersAppService {
    /**
     * Get client monitor parameters
     *
     * @param context
     *            Application context
     *
     * @return Monitor parameters representation if added or null if not added.
     */
    Entity GetMonitorParameters(Context context) throws Exception;
}
