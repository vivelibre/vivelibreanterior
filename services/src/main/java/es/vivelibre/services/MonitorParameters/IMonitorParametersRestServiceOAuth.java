package es.vivelibre.services.MonitorParameters;

import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.services.IRestService;

/**
 * Created by mfernandez on 27/04/2016.
 */
public interface IMonitorParametersRestServiceOAuth extends
        IRestService<MonitorParameters> {

}
