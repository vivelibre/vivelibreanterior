package es.vivelibre.services.MonitorParameters;

import android.content.Context;

import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.Messages;

/**
 * Created by mfernandez on 27/04/2016.
 */
public class MonitorParametersAppService implements
        IMonitorParametersAppService {

    private static MonitorParametersAppService INSTANCE = new MonitorParametersAppService();

    public static MonitorParametersAppService getInstance() {
        return INSTANCE;
    }

    MonitorParametersRestServiceOAuth _monitorParametersRestServiceOAuth = MonitorParametersRestServiceOAuth
            .getInstance();

    public MonitorParameters GetMonitorParameters(Context context)
            throws Exception {
        Entity monitorParameters = _monitorParametersRestServiceOAuth.Get(
                context, new MonitorParameters());
        if (monitorParameters == null)
            throw new Exception(
                    Messages.warning_CannotGetClientMonitorParametersWithEmptyInformation);

        return (MonitorParameters) monitorParameters;
    }

}
