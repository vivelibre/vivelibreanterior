package es.vivelibre.services.Profile;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;

import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.Utils.OAuthHttpClient;
import es.vivelibre.services.RestServiceOAuth;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class ProfileRestServiceOAuth extends RestServiceOAuth<Profile>
        implements IProfileRestServiceOAuth {

    private static ProfileRestServiceOAuth INSTANCE = new ProfileRestServiceOAuth();

    public static ProfileRestServiceOAuth getInstance() {
        return INSTANCE;
    }



}