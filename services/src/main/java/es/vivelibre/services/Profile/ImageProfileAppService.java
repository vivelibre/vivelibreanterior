package es.vivelibre.services.Profile;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;

import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Repository.ImageProfileRepository;
import es.vivelibre.models.Utils.Messages;

import static android.content.ContentValues.TAG;

/**
 * Created by mfernandez on 25/04/2016.
 */
public class ImageProfileAppService implements IImageProfileAppService {
    private static final String SUCCESSFUL_EXECUTE_REQUEST = "SUCCESSFUL_EXECUTE_REQUEST";
    private static ImageProfileAppService INSTANCE = new ImageProfileAppService();
    private static ProfileAppService _profileAppService = ProfileAppService
            .getInstance();

    private static ImageAppRestServiceOauth _ImageAppRestServiceOauth = ImageAppRestServiceOauth.getInstance();

    public static ImageProfileAppService getInstance() {
        return INSTANCE;
    }

    private ImageProfileRepository _imageProfileRepository = ImageProfileRepository
            .getInstance();

    public ImageProfile CreateImageProfile(Context context,
                                           ImageProfile imageProfile) throws Exception {

        if (imageProfile == null)
            throw new Exception(
                    Messages.warning_CannotCreateImageProfileWithEmptyInformation);

        if (GetImageProfile(context) != null)
            throw new Exception(
                    Messages.warning_CannotCreateImageProfileIfAlreadyExist);

        ImageProfile imageProfileAux = new ImageProfile(imageProfile.getName(),
                imageProfile.getExtension(), imageProfile.getData());
        imageProfile.ChangeCurrentIdentity(imageProfile.getId());

        if (_imageProfileRepository.Add(context, imageProfile))
            return imageProfile;
        return null;
    }

    public ImageProfile GetImageProfile(Context context) {
        ImageProfile imageProfile = _imageProfileRepository.Get(context, new ImageProfile());
        if (imageProfile != null)
            return imageProfile;
        return null;
    }

    public StringEntity getImageProfileValue(Context context,
                                                     ImageProfile imageProfile) {
        StringEntity value = null;
        try {
            String image_request_string = new Gson().toJson(imageProfile);

            JSONObject image_request_json = new JSONObject(image_request_string);
            image_request_json.accumulate("ImageProfile",
                    imageProfile.getClass().getSimpleName());
            value = new StringEntity(image_request_json.toString(), HTTP.UTF_8);
            value.setContentType("application/json");

            return value;

        } catch (UnsupportedEncodingException e1) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "ImageProfileAppService getImageProfileValue");
            FirebaseCrash.report(e1);
            e1.printStackTrace();
            return null;
        } catch (JSONException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "ImageProfileAppService getImageProfileValue");
            FirebaseCrash.report(e);
            e.printStackTrace();
            return null;
        }
    }

    public void UpdateImageProfile(Context context,
                                   ImageProfile imageProfile) throws Exception {
       if (imageProfile == null)
           throw new Exception(Messages.warning_CannotUpdateProfileImageWithEmptyInformation);

        ImageProfile persisted = _imageProfileRepository.Get(context, new ImageProfile(imageProfile.getId()));

        if (persisted == null)
            throw new Exception(Messages.warning_CannotUpdateProfileImageWithEmptyInformation);

        ImageProfile current = imageProfile;

        _imageProfileRepository.Update(context,current);
    }

    public  boolean sendImageProfile(Context context,
                                           ImageProfile imageProfile) {
        try {

            Log.i("ImageProfileAppService", "sendImageProfile");
            ImageProfile imageProfileReturn =  _ImageAppRestServiceOauth.SendImageProfile(context,imageProfile);
            if (imageProfileReturn != null){
                return true;
            }
            else return false;

        }catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "ImageProfileAppService sendImageProfile");
            FirebaseCrash.report(e);
            Log.e("ImageProfileAppService", "sendImageProfile");
            e.printStackTrace();
            return false;
        }
    }



}
