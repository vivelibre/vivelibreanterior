package es.vivelibre.services.Profile;

import android.content.Context;

import org.apache.http.entity.StringEntity;

import es.vivelibre.models.Profile.ImageProfile;

/**
 * Created by mfernandez on 18/07/2016.
 */
public interface IImageAppRestServiceOauth {
    /**
     * Add new meter value
     *
     * @param context
     *            Application context
     * @param imageProfile
     *            The imageProfile value representation to add
     *
     * @return imageProfile value results complete with additional information
     *         representation if added or null if not added.
     */
    ImageProfile SendImageProfile(Context context,
                                  ImageProfile imageProfile) throws Exception;


    /**
     * Add new meter value
     *
     * @param context
     *            Application context
     * @param imageProfile
     *            The imageProfile value representation to add
     *
     * @return StringEntity value results complete with additional information
     *         representation if added or null if not added.
     */
    StringEntity getImageProfileValue(Context context,
                                  ImageProfile imageProfile) throws Exception;

}
