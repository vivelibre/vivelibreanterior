package es.vivelibre.services.Profile;

import es.vivelibre.models.Profile.Profile;
import es.vivelibre.services.IRestService;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IProfileRestServiceOAuth extends IRestService<Profile> {

}
