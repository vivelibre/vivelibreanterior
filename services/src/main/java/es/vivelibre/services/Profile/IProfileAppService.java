package es.vivelibre.services.Profile;

import android.content.Context;

import es.vivelibre.models.Profile.Profile;


/**
 * Created by mfernandez on 21/04/2016.
 */
public interface IProfileAppService {
    /**
     * Create profile
     *
     * @param context
     *            Application context
     * @param profile
     *            The profile representation to add
     *
     * @return Created profile representation if added or null if not added.
     */
    Profile CreateProfile(Context context, Profile profile)
            throws Exception;

    /**
     * Get user profile
     *
     * @param context
     *            Application context
     * @return Profile representation if exist or null if not exist.
     */
    Profile GetProfile(Context context);

    /**
     * Update existing profile
     *
     * @param context
     *            Application context
     * @param profileDTO
     *            The profile with changes
     *
     */
    void UpdateProfile(Context context, Profile profileDTO) throws Exception;
}
