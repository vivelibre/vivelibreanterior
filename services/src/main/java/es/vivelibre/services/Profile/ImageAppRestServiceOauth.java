package es.vivelibre.services.Profile;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;

import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.services.RestServiceOAuth;

import static android.content.ContentValues.TAG;

/**
 * Created by mfernandez on 18/07/2016.
 */
public class ImageAppRestServiceOauth extends RestServiceOAuth<ImageProfile> implements IImageAppRestServiceOauth {

    private static ImageAppRestServiceOauth INSTANCE = new ImageAppRestServiceOauth();

    public static ImageAppRestServiceOauth getInstance() {
        return INSTANCE;
    }

    public ImageProfile SendImageProfile (Context context , ImageProfile imageProfile) throws Exception {

        ImageProfile imageProfileAux = imageProfile;

        StringEntity value = getImageProfileValue(context,imageProfileAux);

        ImageProfile entity = this.AddImage(context, imageProfile,value);

        return entity;

    }
    public StringEntity getImageProfileValue(Context context,
                                             ImageProfile imageProfile) {
        StringEntity value = null;
        try {
            String image_request_string = new Gson().toJson(imageProfile);

            JSONObject image_request_json = new JSONObject(image_request_string);
            image_request_json.accumulate("ImageProfileDTO",
                    "ImageProfileClientDTO");
            value = new StringEntity(image_request_json.toString(), HTTP.UTF_8);
            value.setContentType("application/json");

            return value;

        } catch (UnsupportedEncodingException e1) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "ImageAppRestServiceOauth getImageProfileValue");
            FirebaseCrash.report(e1);
            e1.printStackTrace();
            return null;
        } catch (JSONException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "ImageAppRestServiceOauth getImageProfileValue");
            FirebaseCrash.report(e);
            e.printStackTrace();
            return null;
        }
    }
}
