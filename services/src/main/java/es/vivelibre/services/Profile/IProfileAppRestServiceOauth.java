package es.vivelibre.services.Profile;

import android.content.Context;

import es.vivelibre.models.Profile.Profile;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IProfileAppRestServiceOauth {

    /**
     * Get profile
     *
     * @param context
     *            Application context
     *
     * @return Profile representation if added or null if not added.
     */
    Profile GetProfile(Context context) throws Exception;
}