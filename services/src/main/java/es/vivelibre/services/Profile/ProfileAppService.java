package es.vivelibre.services.Profile;

import android.content.Context;

import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.Repository.ProfileRepository;
import es.vivelibre.models.Utils.Messages;

/**
 * Created by mfernandez on 21/04/2016.
 */
public class ProfileAppService implements IProfileAppService {

    private static ProfileAppService INSTANCE = new ProfileAppService();

    public static ProfileAppService getInstance() {
        return INSTANCE;
    }

    private ProfileRepository _profileRepository = ProfileRepository
            .getInstance();

    public Profile CreateProfile(Context context, Profile profile)
            throws Exception {
        if (profile == null)
            throw new Exception(
                    Messages.warning_CannotCreateProfileWithEmptyInformation);

        if (GetProfile(context) != null)
            throw new Exception(
                    Messages.warning_CannotCreateProfileIfAlreadyExist);

        Profile profileAux = new Profile(profile.getName(),
                profile.getFirst_name(), profile.getLast_name(),
                profile.getSex(), profile.getId_Card(),
                profile.getCountry(), profile.getLanguaje(),
                profile.getUsername());
        profile.ChangeCurrentIdentity(profile.getId());

        if (_profileRepository.Add(context, profile))
            return profile;
        return null;
    }

    public Profile GetProfile(Context context) {
        Profile profile = _profileRepository.Get(context, new Profile());
        if (profile != null)
            return profile;
        return null;
    }

    public void UpdateProfile(Context context, Profile profile)
            throws Exception {
        if (profile == null)
            throw new Exception(
                    Messages.warning_CannotUpdateProfileWithEmptyInformation);

        Profile persisted = _profileRepository.Get(context, new Profile(
                profile.getId()));
        if (persisted == null)
            throw new Exception(
                    Messages.warning_CannotUpdateProfileWithEmptyInformation);

        Profile current = profile;

        _profileRepository.Update(context, current);
    }
}