package es.vivelibre.services.Profile;

import android.content.Context;

import org.apache.http.entity.StringEntity;

import java.util.UUID;

import es.vivelibre.models.Profile.ImageProfile;

/**
 * Created by mfernandez on 25/04/2016.
 */
public interface IImageProfileAppService {
    /**
     * Create image profile
     *
     * @param context
     *            Application context
     * @param imageProfile
     *            The image profile representation to add
     *
     * @return Created image profile representation if added or null if not
     *         added.
     */
    ImageProfile CreateImageProfile(Context context,
                                    ImageProfile imageProfile) throws Exception;

    /**
     * Get user image profile
     *
     * @param context
     *            Application context
     * @return Image profile representation if exist or null if not exist.
     */
    ImageProfile GetImageProfile(Context context);

    /**
     * Update existing image profile
     *
     * @param context
     *            Application context
     * @param imageProfile
     *            The image profile with changes
     *
     */
    void UpdateImageProfile(Context context, ImageProfile imageProfile)
            throws Exception;

    /**
     * Send existing image profile
     *
     * @param context
     *            Application context
     * @param imageProfile
     *            The image profile with changes
     *
     */
     boolean sendImageProfile(Context context,
                     ImageProfile imageProfile);

    /**
     * getImageProfileValue Entity existing image profile
     *
     * @param context
     *            Application context
     * @param imageProfile
     *            The image profile with changes
     *
     */
    StringEntity getImageProfileValue(Context context,
                                      ImageProfile imageProfile);

}
