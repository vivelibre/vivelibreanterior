package es.vivelibre.services.Profile;

import android.content.Context;

import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.Messages;
import es.vivelibre.services.RestServiceOAuth;

/**
 * Created by mfernandez on 22/04/2016.
*/
public class ProfileAppRestServiceOauth extends RestServiceOAuth<Profile>
        implements IProfileAppRestServiceOauth {

    private static ProfileAppRestServiceOauth INSTANCE = new ProfileAppRestServiceOauth();

    public static ProfileAppRestServiceOauth getInstance() {
        return INSTANCE;
    }

    ProfileRestServiceOAuth _profileRestServiceOAuth = ProfileRestServiceOAuth
            .getInstance();

    public Profile GetProfile(Context context) throws Exception {
        Entity profile = _profileRestServiceOAuth
                .Get(context, new Profile());
        if (profile == null)
            throw new Exception(
                    Messages.warning_CannotGetProfileWithEmptyInformation);

        return (Profile) profile;
    }



}