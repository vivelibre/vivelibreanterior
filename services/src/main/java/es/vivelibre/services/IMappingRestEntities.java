package es.vivelibre.services;

import android.content.Context;

import es.vivelibre.models.SQLite.Entity;

/**
 * Created by mfernandez on 25/04/2016.
 */
public interface IMappingRestEntities {

    String GetReadScope(Entity item);

    String GetCreateScope(Entity item);

    String GetUpdateScope(Entity item);

    String GetDeleteScope(Entity item);

    String GetApi(Entity item);

    String GetRefreshToken(Context context);

    void UpdateRefreshToken(Context context, String token);

}