package es.vivelibre.services;

import es.vivelibre.models.JsonConverters.JsonMeterValueCompleteConverter;
import es.vivelibre.models.Meter.Meter;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.AccessToken;
import es.vivelibre.models.Utils.InstallTrustManager;
import es.vivelibre.models.Utils.Messages;
import es.vivelibre.models.Utils.OAuthApi;
import es.vivelibre.models.Utils.OAuthConfig;
import es.vivelibre.models.Utils.OAuthHttpClient;
import es.vivelibre.models.Utils.OAuthRequestFilters;

import org.apache.amber.oauth2.client.request.OAuthClientRequest;
import org.apache.amber.oauth2.common.message.types.GrantType;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.google.firebase.crash.FirebaseCrash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class RestServiceOAuth<T extends Entity> implements IRestService<T> {

    protected MappingRestEntities mapping = MappingRestEntities.getInstance();
    protected static AccessToken accessToken = AccessToken.getInstance();

    public RestServiceOAuth() {
        super();
    }

    public T Add(Context context, T item, StringEntity value) throws Exception {
        return HttpRequest(context, item, new HttpPost(), value);
    }

    public T AddImage(Context context, T item, StringEntity value) throws Exception {
        return HttpRequest(context, item, new HttpPost(), value);
    }


    @SuppressWarnings("unchecked")
    public ArrayList<T> AddList(Context context, ArrayList<T> items,
                                StringEntity value) throws Exception {

        T item = items.get(0);
        OAuthHttpClient oAuthHttpClient = ConfigureHttpRequest(context, item,
                new HttpPost());
        HttpResponse httpResponse = oAuthHttpClient.HttpPostRequest(
                GetApi(item).concat("list"), accessToken.getData(context),
                value);

        Pair<Integer, String> response = GetHttpResponse(httpResponse);
        Log.i("AddList", response.toString());

        return (ArrayList<T>) GetResultListObject(item, new JSONArray(
                response.second));
    }

    public ArrayList<Entity> GetResultListObject(Entity item, JSONArray result)
            throws Exception {
        if (item instanceof MeterValueComplete) {
            return JsonMeterValueCompleteConverter.ConverToList(result);
        }
        return null;
    }

    public String GetApi(Entity item) {
        if (item instanceof Profile) {
            return OAuthApi.API_PROFILE;
        } else if (item instanceof MeterValueComplete) {
            if (((MeterValueComplete) item).getMeterValue().getMeter_id().equals("00000000-0000-0000-0000-000000000000")) {
                return OAuthApi.API_MANUAL_METER_RESULT_CLIENTS;
            }else {
                return OAuthApi.API_METER_RESULT_CLIENTS;
            }
        } else if (item instanceof MonitorParameters) {
            return OAuthApi.API_CLIENT_MONITOR_PARAMETERS;
        }else if (item instanceof ImageProfile){
            return OAuthApi.API_IMAGE_PROFILE;
        }
        return null;
    }

    public void Remove(Context context, T item) throws Exception {
        // TODO Auto-generated method stub

    }

    public void Modify(Context context, T item, StringEntity value)
            throws Exception {
        // TODO Auto-generated method stub

    }

    public T Get(Context context, T item) throws Exception {
        return HttpRequest(context, item, new HttpGet(), null);
    }

    public ArrayList<T> GetAll(Context context, T item,
                               OAuthRequestFilters oAuthRequestFilters) throws Exception {
        return HttpRequestAll(context, item, new HttpGet(), oAuthRequestFilters);
    }

	/*
	 * PRIVATE METHODS
	 */

    @SuppressWarnings("unchecked")
    private T HttpRequest(Context context, T item,
                          HttpRequestBase httpRequestBase, StringEntity value)
            throws Exception {
        synchronized (context) {

            OAuthHttpClient oAuthHttpClient = ConfigureHttpRequest(context,
                    item, httpRequestBase);
            HttpResponse httpResponse = ExecuteRequest(item,
                    accessToken.getData(context), httpRequestBase,
                    oAuthHttpClient, value);

            Pair<Integer, String> response = GetHttpResponse(httpResponse);
            Log.i("HttpRequest", response.toString());
            if (httpRequestBase instanceof HttpGet
                    || httpRequestBase instanceof HttpPost) {
                return (T) mapping.GetResultObject(item, new JSONObject(
                        response.second));
            }
            return item;
        }
    }

    @SuppressWarnings("unchecked")
    private ArrayList<T> HttpRequestAll(Context context, T item,
                                        HttpRequestBase httpRequestBase,
                                        OAuthRequestFilters oAuthRequestFilters) throws Exception {
        synchronized (context) {

            OAuthHttpClient oAuthHttpClient = ConfigureHttpRequest(context,
                    item, httpRequestBase);
            HttpResponse httpResponse = ExecuteGetAllRequest(item,
                    accessToken.getData(context), httpRequestBase,
                    oAuthHttpClient, oAuthRequestFilters);

            Pair<Integer, String> response = GetHttpResponse(httpResponse);
            Log.i("HttpRequestAll",response.toString());
            // ArrayList<T> a = (ArrayList<T>) gson.fromJson(response.second,
            // new TypeToken<ArrayList<T>>() {
            // }.getType());

            return (ArrayList<T>) mapping.GetResultListObject(item,
                    new JSONArray(response.second));
        }
    }

    private String GetScope(HttpRequestBase httpRequestBase, T item) {
        if (httpRequestBase instanceof HttpGet)
            return mapping.GetReadScope(item);
        else if (httpRequestBase instanceof HttpPost)
            return mapping.GetCreateScope(item);
        else if (httpRequestBase instanceof HttpPut)
            return mapping.GetUpdateScope(item);
        else if (httpRequestBase instanceof HttpDelete)
            return mapping.GetDeleteScope(item);
        return null;
    }

    private HttpResponse ExecuteRequest(T item, String accessToken,
                                        HttpRequestBase httpRequestBase, OAuthHttpClient httpOAuthClient,
                                        StringEntity value) {

        if (httpRequestBase instanceof HttpGet)
            return httpOAuthClient.HttpGetRequest(mapping.GetApi(item),
                    accessToken);
        else if (httpRequestBase instanceof HttpPost)
            return httpOAuthClient.HttpPostRequest(mapping.GetApi(item),
                    accessToken, value);
        else if (httpRequestBase instanceof HttpPut)
            return httpOAuthClient.HttpPutRequest(mapping.GetApi(item),
                    accessToken, value);
        else if (httpRequestBase instanceof HttpDelete)
            return httpOAuthClient.HttpDeleteRequest(mapping.GetApi(item),
                    accessToken);
        return null;
    }

    private HttpResponse ExecuteGetAllRequest(T item, String accessToken,
                                              HttpRequestBase httpRequestBase, OAuthHttpClient httpOAuthClient,
                                              OAuthRequestFilters oAuthRequestFilters) {

        if (httpRequestBase instanceof HttpGet)
            return httpOAuthClient.HttpGetRequest(
                    mapping.GetAllApi(item, oAuthRequestFilters), accessToken);
        return null;
    }

    private OAuthHttpClient ConfigureHttpRequest(Context context, T item,
                                                 HttpRequestBase httpRequestBase) throws Exception {
        InstallTrustManager.installHostTrustVerifier();

        OAuthClientRequest request = null;

        String scope = GetScope(httpRequestBase, item);

        request = OAuthClientRequest.tokenLocation(OAuthConfig.getTOKEN_URL())
                .setClientId(OAuthConfig.getCONSUMER_KEY())
                .setClientSecret(OAuthConfig.getCONSUMER_SECRET())
                .setGrantType(GrantType.REFRESH_TOKEN)
                .setRefreshToken(mapping.GetRefreshToken((context)))
                .setScope(scope).buildBodyMessage();

        if (request == null)
            throw new Exception(
                    Messages.warning_CannotPerfomOAuthRequestWithEmptyRequest);

        OAuthHttpClient oAuthHttpClient = new OAuthHttpClient();

        if (!accessToken.isValid(context, System.currentTimeMillis())) {
            Pair<String, String> tokens = oAuthHttpClient
                    .GetAccessToken(request);
            if (tokens != null) {
                String newRefreshToken = tokens.first;
                String newAccessToken = tokens.second;

                accessToken.setData(context, newAccessToken);
                accessToken.setDate(context, System.currentTimeMillis());

                if (!mapping.GetRefreshToken(context).equals(newRefreshToken))
                    mapping.UpdateRefreshToken(context, newRefreshToken);
            } else
                throw new Exception(
                        Messages.warning_CannotPerfomOAuthRequestWithNotValidAccessToken);
        }
        return oAuthHttpClient;
    }

    private Pair<Integer, String> GetHttpResponse(HttpResponse httpResponse)
            throws Exception {
        Pair<Integer, String> response = GetResponse(httpResponse);
        if (response == null)
            throw new Exception(
                    Messages.warning_CannotPerfomOAuthRequestWithEmptyResponse);

        if (!String.valueOf(response.first).startsWith("2"))
            throw new Exception(
                    Messages.warning_CannotPerfomOAuthRequestWithWrongHttpStatusCode);
        Log.i("GetHttpResponse: ",response.toString());
        return response;
    }

    private Pair<Integer, String> GetResponse(HttpResponse httpResponse) {
        if (httpResponse == null)
            return null;

        String response = null;
        HttpEntity entity = httpResponse.getEntity();
        if (entity != null) {
            InputStream instream;
            try {
                instream = entity.getContent();
                response = convertStreamToString(instream);
                instream.close();
                int httpStatusCode = httpResponse.getStatusLine()
                        .getStatusCode();
                Log.i("httpStatusCode: ", String.valueOf(httpStatusCode));
                return new Pair<Integer, String>(httpStatusCode, response);
            } catch (IllegalStateException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "RestServiceOauth GetResponse");
                FirebaseCrash.report(e);
                e.printStackTrace();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "RestServiceOauth GetResponse");
                FirebaseCrash.report(e);
                e.printStackTrace();
            } catch (JSONException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "RestServiceOauth GetResponse");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
        }
        return null;
    }

    private String convertStreamToString(InputStream is) throws JSONException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "RestServiceOauth convertStreamToString");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "RestServiceOauth convertStreamToString");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
