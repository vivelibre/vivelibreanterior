package es.vivelibre.services;

import android.content.Context;

import org.apache.http.entity.StringEntity;

import java.util.ArrayList;

import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.OAuthRequestFilters;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IRestService<T extends Entity> {

    T Add(Context context, T item, StringEntity value) throws Exception;

    T AddImage(Context context, T item, StringEntity value) throws Exception;


    ArrayList<T> AddList(Context context, ArrayList<T> items, StringEntity value)
            throws Exception;

    void Remove(Context context, T item) throws Exception;

    void Modify(Context context, T item, StringEntity value) throws Exception;

    T Get(Context context, T item) throws Exception;

    ArrayList<T> GetAll(Context context, T item,
                        OAuthRequestFilters oAuthRequestFilters) throws Exception;
}