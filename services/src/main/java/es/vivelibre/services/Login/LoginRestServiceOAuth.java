package es.vivelibre.services.Login;

import android.content.Context;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class LoginRestServiceOAuth extends RestServiceOAuthLogin implements
        IRestServiceLogin {

    private static LoginRestServiceOAuth INSTANCE = new LoginRestServiceOAuth();

    public static LoginRestServiceOAuth getInstance() {
        return INSTANCE;
    }

    public LoginRestServiceOAuth() {
        super();
    }

    public boolean Login(Context context, String username, String password)
            throws Exception {
        return super.Login(context, username, password);
    }
}

