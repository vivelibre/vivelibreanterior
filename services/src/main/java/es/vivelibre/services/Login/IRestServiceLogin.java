package es.vivelibre.services.Login;

import android.content.Context;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface IRestServiceLogin {
    boolean Login(Context context, String username, String password) throws Exception;
}
