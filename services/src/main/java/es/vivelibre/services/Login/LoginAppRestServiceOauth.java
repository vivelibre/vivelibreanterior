package es.vivelibre.services.Login;

import android.content.Context;

import es.vivelibre.models.Utils.Messages;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class LoginAppRestServiceOauth extends RestServiceOAuthLogin implements
        ILoginAppRestServiceOauth {

    private static LoginAppRestServiceOauth INSTANCE = new LoginAppRestServiceOauth();

    public static LoginAppRestServiceOauth getInstance() {
        return INSTANCE;
    }

    private LoginRestServiceOAuth _loginRestServiceOAuth = LoginRestServiceOAuth
            .getInstance();

    public boolean LoginUser(Context context, String username, String password)
            throws Exception {
        if (username == null || username.isEmpty() || password == null
                || password.isEmpty())
            throw new Exception(
                    Messages.warning_CannotLoginWithEmptyInformation);

        return _loginRestServiceOAuth.Login(context, username, password);
    }

}
