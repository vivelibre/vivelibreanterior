package es.vivelibre.services.Login;

import android.content.Context;
import android.util.Pair;

import org.apache.amber.oauth2.client.OAuthClient;
import org.apache.amber.oauth2.client.request.OAuthClientRequest;
import org.apache.amber.oauth2.common.exception.OAuthProblemException;
import org.apache.amber.oauth2.common.exception.OAuthSystemException;
import org.apache.amber.oauth2.common.message.types.GrantType;
import org.apache.http.entity.StringEntity;

import java.util.Locale;

import es.vivelibre.models.Utils.InstallTrustManager;
import es.vivelibre.models.Utils.Messages;
import es.vivelibre.models.Utils.OAuthConfig;
import es.vivelibre.models.Utils.OAuthHttpClient;
import es.vivelibre.models.Utils.OAuthScopes;
import es.vivelibre.models.Utils.SharedCustomPreferencesUtils;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class RestServiceOAuthLogin implements IRestServiceLogin {
    private static final int ACCESS_TOKEN_MAX_ATTEMPTS = 5;
    private static RestServiceOAuthLogin INSTANCE = new RestServiceOAuthLogin();

    public static RestServiceOAuthLogin getInstance() {
        return INSTANCE;
    }

    //protected MappingRestEntities mapping = MappingRestEntities.getInstance();
    protected static AccessToken accessToken = AccessToken.getInstance();

    public boolean Login(Context context, String username, String password)
            throws Exception {
        synchronized (context) {

            InstallTrustManager.installHostTrustVerifier();

            OAuthClientRequest request = null;

            request = OAuthClientRequest
                    .tokenLocation(OAuthConfig.getTOKEN_URL())
                    .setClientId(OAuthConfig.getCONSUMER_KEY())
                    .setClientSecret(OAuthConfig.getCONSUMER_SECRET())
                    .setGrantType(GrantType.PASSWORD)
                    .setScope(OAuthScopes.ALL_APP_SCOPES)
                    .setUsername(username)
                    .setPassword(password).buildBodyMessage();

            if (request == null)
                throw new Exception(
                        Messages.warning_CannotPerfomLoginOAuthRequestWithEmptyRequest);

            OAuthHttpClient oAuthHttpClient = new OAuthHttpClient();

            Pair<String, String> tokens = oAuthHttpClient
                    .GetAccessToken(request);
            if (tokens != null) {
                String newRefreshToken = tokens.first;
                String newAccessToken = tokens.second;

                accessToken.setData(context, newAccessToken);
                accessToken.setDate(context, System.currentTimeMillis());

                if (!GetRefreshToken(context).equals(newRefreshToken))
                    UpdateRefreshToken(context, newRefreshToken);

                return true;

            } else{
                throw new Exception(
                        Messages.warning_CannotPerfomLoginOAuthRequestWithNotValidAccessToken);

            }
        }
    }

    public String GetRefreshToken(Context context) {
        return SharedCustomPreferencesUtils.getRefreshToken(context);
    }
    public void UpdateRefreshToken(Context context, String token) {
        SharedCustomPreferencesUtils.setRefreshToken(context, token);
    }
}


