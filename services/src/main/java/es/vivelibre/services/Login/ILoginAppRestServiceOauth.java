package es.vivelibre.services.Login;

import android.content.Context;

/**
 * Created by mfernandez on 22/04/2016.
 */
public interface ILoginAppRestServiceOauth {

    /**
     * Login user into the application
     *
     * @param context
     *            Application context
     * @param username
     *            The user name
     * @param password
     *            The user password
     *
     * @return Return true if login correct.
     */
    boolean LoginUser(Context context, String username, String password)
            throws Exception;
}
