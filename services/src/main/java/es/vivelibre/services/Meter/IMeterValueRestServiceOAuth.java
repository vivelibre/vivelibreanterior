package es.vivelibre.services.Meter;

import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.services.IRestService;

/**
 * Created by mfernandez on 27/04/2016.
 */
public interface IMeterValueRestServiceOAuth extends IRestService<MeterValueComplete> {
}
