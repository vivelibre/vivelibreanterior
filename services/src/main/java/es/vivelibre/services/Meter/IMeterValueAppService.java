package es.vivelibre.services.Meter;

import android.content.Context;

import java.util.ArrayList;

import es.vivelibre.models.Meter.MeterValue;

/**
 * Created by mfernandez on 10/05/2016.
 */
public interface IMeterValueAppService {
    /**
     * Add new meter value
     *
     * @param context
     *            Application context
     * @param meterValue
     *            The meter value representation to add
     *
     * @return Added meter value representation
     */
    MeterValue AddNewMeterValue(Context context, MeterValue meterValue)
            throws Exception;

    /**
     * Get user meter values
     *
     * @param context
     *            Application context
     * @param meterValue
     *            Type of meter value results to find
     *            Specification to filter
     * @return A collection of meter values representation for a client
     */
    ArrayList<MeterValue> GetMeterValues(Context context,
                                         MeterValue meterValue)
            throws Exception;

    /**
     * Update meter values
     *
     * @param context
     *            Application context
     * @param meterValue
     *            Type of meter value results to find
     */
    void UpdateMeterValue(Context context, MeterValue meterValue)
            throws Exception;

}
