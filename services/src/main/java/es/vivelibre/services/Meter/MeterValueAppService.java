package es.vivelibre.services.Meter;

import android.content.Context;

import java.util.ArrayList;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.models.Meter.WeightValue;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.Repository.MeterValueRepository;
import es.vivelibre.models.Utils.Messages;
import es.vivelibre.services.Profile.ProfileAppService;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class MeterValueAppService implements IMeterValueAppService {

    private static MeterValueAppService INSTANCE = new MeterValueAppService();

    public static MeterValueAppService getInstance() {
        return INSTANCE;
    }

    private MeterValueRepository _meterValueRepository = MeterValueRepository
            .getInstance();
    private ProfileAppService _profileAppService = ProfileAppService
            .getInstance();

    public MeterValue AddNewMeterValue(Context context,
                                          MeterValue meterValue) throws Exception {

        if (meterValue == null)
            throw new Exception(
                    Messages.warning_CannotAddNewMeterValueWithEmptyInformation);

        Profile profile = _profileAppService.GetProfile(context);
        if (profile == null)
            throw new Exception(
                    Messages.warning_CannotAddNewMeterValueWithEmptyClientProfileInformation);

        MeterValue meterValueAux = null;
        if (meterValue instanceof HeartRateValue) {
            meterValueAux = meterValueAux.CreateHeartRateValue(
                    profile.getId(), meterValue.getValue(),
                    meterValue.getMeter_id(), meterValue.getDateRead(),
                    meterValue.isError());
        } else if (meterValue instanceof OxygenSaturationValue) {
            meterValueAux = meterValueAux.CreateOxygenSaturationValue(
                    profile.getId(), meterValue.getValue(),
                    meterValue.getMeter_id(), meterValue.getDateRead(),
                    meterValue.isError());
        } else if (meterValue instanceof BloodPressureValue) {
            meterValueAux = meterValueAux.CreateBloodPressureValue(
                    profile.getId(), meterValue.getValue(),
                    meterValue.getMeter_id(), meterValue.getDateRead(),
                    meterValue.isError());
        } else if (meterValue instanceof WeightValue) {
            meterValueAux = meterValueAux.CreateWeightValue(
                    profile.getId(), meterValue.getValue(),
                    meterValue.getMeter_id(), meterValue.getDateRead(),
                    meterValue.isError());
        } else
            throw new Exception();

        if (_meterValueRepository.Add(context, meterValueAux))
            return meterValueAux;
        return null;
    }

    public ArrayList<MeterValue> GetMeterValues(Context context,
                                                   MeterValue meterValue)
            throws Exception {
        ArrayList<MeterValue> meterValues = _meterValueRepository.GetAll(
                context, meterValue);
        if (meterValues != null && meterValues.size() > 0)
            return meterValues;
        return new ArrayList<MeterValue>();
    }

    public void UpdateMeterValue(Context context, MeterValue meterValue)
            throws Exception {
        _meterValueRepository.Update(context, meterValue);
    }

}
