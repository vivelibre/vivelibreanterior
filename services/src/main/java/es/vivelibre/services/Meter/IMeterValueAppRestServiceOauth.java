package es.vivelibre.services.Meter;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;

import es.vivelibre.models.Meter.MeterValueComplete;

/**
 * Created by mfernandez on 27/04/2016.
 */
public interface IMeterValueAppRestServiceOauth {
    /**
     * Add new meter value
     *
     * @param context
     *            Application context
     * @param meterValue
     *            The meter value representation to add
     *
     * @return Meter value results complete with additional information
     *         representation if added or null if not added.
     */
    MeterValueComplete AddNewMeterValue(Context context,
                                        MeterValueComplete meterValue) throws Exception;

    /**
     * Get client meter values
     *
     * @param context
     *            Application context
     * @param meterValueType
     *            Type of meter values to find
     * @param dateToFind
     *            Date filter to find only values of selected day
     *
     * @return A collection of meter value results complete with additional
     *         information representation
     */
    ArrayList<MeterValueComplete> GetMeterValues(Context context,
                                                 MeterValueComplete meterValueComplete, Date dateToFind)
            throws Exception;
}

