package es.vivelibre.services.Meter;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;

import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.services.RestServiceOAuth;

/**
 * Created by mfernandez on 27/04/2016.
 */
public class MeterValueRestServiceOAuth extends
        RestServiceOAuth<MeterValueComplete> implements
        IMeterValueRestServiceOAuth {

    private static MeterValueRestServiceOAuth INSTANCE = new MeterValueRestServiceOAuth();

    public static MeterValueRestServiceOAuth getInstance() {
        return INSTANCE;
    }
}