package es.vivelibre.services.Meter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.models.Meter.MeterValueType;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.Messages;
import es.vivelibre.models.Utils.OAuthRequestFilters;
import es.vivelibre.services.Profile.ProfileAppService;

/**
 * Created by mfernandez on 27/04/2016.
 */
public class MeterValueAppRestServiceOauth implements IMeterValueAppRestServiceOauth {

    private ProfileAppService _profileAppService = ProfileAppService
            .getInstance();
    private static MeterValueAppRestServiceOauth INSTANCE = new MeterValueAppRestServiceOauth();

    public static MeterValueAppRestServiceOauth getInstance() {
        return INSTANCE;
    }

    MeterValueRestServiceOAuth _meterValueRestServiceOAuth = MeterValueRestServiceOAuth
            .getInstance();

    public MeterValueComplete AddNewMeterValue(Context context, MeterValueComplete meterValue) throws Exception {
        MeterValue meterValueAux = meterValue.getMeterValue();

        Entity meterValueAdded = _meterValueRestServiceOAuth.Add(context,
                meterValue,
                MeterValue.GetMeterValueBinding(meterValueAux));

        return (MeterValueComplete) meterValueAdded;
    }

    public ArrayList<MeterValueComplete> AddNewListMeterValues(
            Context context,
            ArrayList<MeterValueComplete> meterValuesComplete)
            throws Exception {

        ArrayList<MeterValueComplete> meterValuesAdded = _meterValueRestServiceOAuth
                .AddList(context,meterValuesComplete,MeterValue.GetMeterValueListBinding(meterValuesComplete));
        // Aqui lo que vamos hacer es comprobar las medidas que hemos enviado o no y vamos hacer un setSharedPreferences de las medidas
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        for (int i = 0; i < meterValuesAdded.size(); i++) {
            Gson gson = new Gson();
            //Le cambio la fecha al SharedPreferences con la del movil
            Date actual  = Calendar.getInstance().getTime();
            meterValuesAdded.get(i).getMeterValue().setDateRead(actual);
            String json = gson.toJson(meterValuesAdded.get(i).getMeterValue());
            String clase = meterValuesAdded.get(i).getMeterValue().getClass().getSimpleName();
            prefsEditor.putString(meterValuesAdded.get(i).getMeterValue().getClass().getSimpleName(), json);

        }
        prefsEditor.commit();
        return meterValuesAdded;
    }
    public ArrayList<MeterValueComplete> GetMeterValues(Context context, MeterValueComplete meterValueComplete, Date dateToFind) throws Exception {
        Profile profile = _profileAppService.GetProfile(context);
        if (profile == null)
            throw new Exception(
                    Messages.warning_CannotGetMeterValueWithEmptyClientProfileInformation);

        OAuthRequestFilters oAuthRequestFilters = new OAuthRequestFilters(
                profile.getId(), dateToFind);
        ArrayList<MeterValueComplete> meterValuesComplete = _meterValueRestServiceOAuth
                .GetAll(context, meterValueComplete, oAuthRequestFilters);
        if (meterValuesComplete == null)
            throw new Exception(
                    Messages.warning_CannotGetClientMeterValuesWithEmptyInformation);

        return meterValuesComplete;
    }



    public ArrayList<MeterValueComplete> GetMeterValues(Context context, MeterValueComplete meterValueComplete, Date dateIni , Date dateFin) throws Exception {
        Profile profile = _profileAppService.GetProfile(context);
        if (profile == null)
            throw new Exception(
                    Messages.warning_CannotGetMeterValueWithEmptyClientProfileInformation);

        OAuthRequestFilters oAuthRequestFilters = new OAuthRequestFilters(
                profile.getId(), dateIni, dateFin);
        ArrayList<MeterValueComplete> meterValuesComplete = _meterValueRestServiceOAuth
                .GetAll(context, meterValueComplete, oAuthRequestFilters);
        if (meterValuesComplete == null)
            throw new Exception(
                    Messages.warning_CannotGetClientMeterValuesWithEmptyInformation);

        return meterValuesComplete;
    }

    public ArrayList<MeterValueComplete> GetMeterValues(Context context, String type,  MeterValueComplete meterValueComplete, Date dateIni , Date dateFin) throws Exception {
        Profile profile = _profileAppService.GetProfile(context);

        if (profile == null)
            throw new Exception(
                    Messages.warning_CannotGetMeterValueWithEmptyClientProfileInformation);

        OAuthRequestFilters oAuthRequestFilters = new OAuthRequestFilters(
                profile.getId(),type, dateIni, dateFin);

        ArrayList<MeterValueComplete> meterValuesComplete = _meterValueRestServiceOAuth
                .GetAll(context, meterValueComplete, oAuthRequestFilters);


        return meterValuesComplete;
    }

}
