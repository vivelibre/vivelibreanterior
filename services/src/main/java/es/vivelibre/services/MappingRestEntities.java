package es.vivelibre.services;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.vivelibre.models.JsonConverters.JsonMeterValueCompleteConverter;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.SQLite.Entity;
import es.vivelibre.models.Utils.DateFunctions;
import es.vivelibre.models.Utils.OAuthApi;
import es.vivelibre.models.Utils.OAuthRequestFilters;
import es.vivelibre.models.Utils.OAuthScopes;
import es.vivelibre.models.Utils.SharedCustomPreferencesUtils;

/**
 * Created by mfernandez on 25/04/2016.
 */
public class MappingRestEntities implements IMappingRestEntities {

    private static MappingRestEntities INSTANCE = new MappingRestEntities();

    public static MappingRestEntities getInstance() {
        return INSTANCE;
    }

    public String GetReadScope(Entity item) {
        if (item instanceof Profile) {
            return OAuthScopes.READ_PROFILE;
        } else if (item instanceof MonitorParameters) {
            return OAuthScopes.READ_CLIENT_MONITOR_PARAMETERS;
        } else if (item instanceof MeterValueComplete) {
            return OAuthScopes.READ_METER_RESULT_CLIENT;
        }
        return null;
    }

    public String GetCreateScope(Entity item) {
        if (item instanceof MeterValueComplete) {
            return OAuthScopes.CREATE_METER_RESULT_CLIENT;
        }else if(item instanceof ImageProfile)
            return OAuthScopes.CREATE_IMAGE_PROFILE;
        return null;
    }

    public String GetUpdateScope(Entity item) {
        return null;
    }

    public String GetDeleteScope(Entity item) {
        return null;
    }

    public String GetApi(Entity item) {
        if (item instanceof Profile) {
            return OAuthApi.API_PROFILE;
        } else if (item instanceof MeterValueComplete) {
            return OAuthApi.API_METER_RESULT_CLIENTS;
        } else if (item instanceof MonitorParameters) {
            return OAuthApi.API_CLIENT_MONITOR_PARAMETERS;
        }else if( item instanceof ImageProfile){
            return OAuthApi.API_IMAGE_PROFILE;
        }
        return null;
    }

    public String GetAllApi(Entity item, OAuthRequestFilters oAuthRequestFilters) {
        if (item instanceof MeterValueComplete) {
            if (oAuthRequestFilters.getType() != null){
                // Api GetMeterValues 30 dias con el tipo
                if (oAuthRequestFilters.getDateIni() == null){
                    // Api GetMeterValues para un dia concreto
                    return OAuthApi.GetAllMeterResultClientApi(
                            ((MeterValueComplete) item).getMeterValue(),
                            oAuthRequestFilters);
                }
                else {
                    return OAuthApi.GetAllMeterValuesByDatesResultClientApi(
                            ((MeterValueComplete) item).getMeterValue(),
                            oAuthRequestFilters);
                }
            }
            else if (oAuthRequestFilters.getDateFin() == null){
                // Api GetMeterValues para un dia concreto
                return OAuthApi.GetAllMeterResultClientApi(
                        ((MeterValueComplete) item).getMeterValue(),
                        oAuthRequestFilters);
            }else {
                // Api GetMeterValues con Fechas Inicial y Final
                return OAuthApi.GetAllMeterValuesResultClientApi(
                        ((MeterValueComplete) item).getMeterValue(),
                        oAuthRequestFilters);
            }
        }
        return null;
    }

    public Entity GetResultObject(Entity item, JSONObject result) throws Exception {
        Gson gson = new GsonBuilder().setDateFormat(DateFunctions.DATE_FORMAT)
                .create();
        if (item instanceof MeterValueComplete) {
            return JsonMeterValueCompleteConverter.Conver(result);
        } else if (item instanceof Profile) {
            return gson
                    .fromJson(result.toString(), new Profile().getClass());
        } else if (item instanceof MonitorParameters) {
            return gson.fromJson(result.toString(),
                    new MonitorParameters().getClass());
        }else if(item instanceof ImageProfile){
            return gson.fromJson(result.toString(), new ImageProfile().getClass());
        }
        else return null;
    }

    public ArrayList<Entity> GetResultListObject(Entity item, JSONArray result)
            throws Exception {
        if (item instanceof MeterValueComplete) {
            return JsonMeterValueCompleteConverter.ConverToList(result);
        }
        return null;
    }

    public String GetRefreshToken(Context context) {
        return SharedCustomPreferencesUtils.getRefreshToken(context);
    }

    public void UpdateRefreshToken(Context context, String token) {
        SharedCustomPreferencesUtils.setRefreshToken(context, token);
    }


}
