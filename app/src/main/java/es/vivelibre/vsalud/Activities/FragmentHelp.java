package es.vivelibre.vsalud.Activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;


import com.astuetz.PagerSlidingTabStrip;

import es.vivelibre.vsalud.Adapters.HelpViewPagerAdapter;
import es.vivelibre.vsalud.R;

public class FragmentHelp extends Fragment{

    private TabHost mTabHost;
    private ViewPager mViewPager;
    private HelpViewPagerAdapter mTabsAdapter;


    View v;

    public FragmentHelp() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    //Toast.makeText(getActivity(), "Falta implementación", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new FragmentHome();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_home, fragment)
                            .commit();

                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_help, container, false);
        mViewPager = (ViewPager)rootView.findViewById(R.id.pager);
        mTabsAdapter= new HelpViewPagerAdapter(getFragmentManager());
        mViewPager.setAdapter(mTabsAdapter);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip)rootView.findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);
        tabs.setShouldExpand(true);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
