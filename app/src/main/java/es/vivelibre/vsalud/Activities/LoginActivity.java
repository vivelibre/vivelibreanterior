package es.vivelibre.vsalud.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import es.vivelibre.models.Profile.Profile;
import es.vivelibre.vsalud.Controllers.LoginController;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.R;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements OnClickListener {

    private static LoginController loginController = LoginController
            .getInstance();

    private Button buttonSubmit;
    private EditText editUsername;
    private EditText editPassword;
    //private LinearLayout layoutOptions;
    private Context context;
    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Login");
        setContentView(R.layout.activity_login);

        initializeViews();
        checkSessionState();
    }

    /**
     * Initializes references to the views.
     */
    private void initializeViews() {
        context = getApplicationContext();
        activity = this;

        editUsername = (EditText) findViewById(R.id.loginUser);
        editPassword = (EditText) findViewById(R.id.loginPassword);

        buttonSubmit = (Button) findViewById(R.id.loginButton);
        buttonSubmit.setOnClickListener(this);
    }

    private boolean isFirstTime()
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return ranBefore;

    }

    /**
     * Check if the user is logged. If is logged and the application is not
     * locked by security code, go to main activity. Otherwise, go to unlock
     * screen activity.
     */
    private void checkSessionState() {
        if (isLogged()) {
            if (isLocked()) {
                // go_to_lock_screen();
            } else {
                launchMainMenuActivity();
            }
        }
    }

    /**
     * Launch {@code HomeActivity}
     */
    private void launchMainMenuActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startActivity(homeIntent);
        finish();
    }
    /**
     * Check if the user is logged.
     *
     * @return Return true if the user is logged.
     */
    private boolean isLogged() {
        return loginController.isLogged(context);
    }

    /**
     * Check if the application is locked by security code.
     *
     * @return Return true if the application is locked.
     */
    private boolean isLocked() {
        return loginController.isLocked(context);
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            startAsyncTaskLogin();
        }else{
            startActivity(new Intent(LoginActivity.this, TermsActivity.class));
        }
    }

    public void loginProcessResponse(boolean isLoginSuccessful) {
        DisplayFunctions.CancelDialog();
        if (isLoginSuccessful) {
            launchMainMenuActivity();
        } else {
            //layoutOptions.setVisibility(View.VISIBLE);
            errorLogin();
        }
    }
    /**
     * Starts login operation in the background.
     */
    private void startAsyncTaskLogin() {
        Log.i("LOGIN ACTIVITY", "AsyncTask Login Proccess");
        AsyncTask<String, Float, Boolean> AsyncTaskLogin = new AsyncTask<String, Float, Boolean>() {

            String username= "";
            String password= "";

            protected void onPreExecute() {
                username =  editUsername.getText().toString();
                password = editPassword.getText().toString();

                password = getMD5(password);

                DisplayFunctions.showProgressDialog(activity,
                        getString(R.string.login_progress_dialog_title),
                        getString(R.string.login_progress_dialog_login), true);
            }

            protected Boolean doInBackground(String... params) {
                try {
                    if (loginController.isLoginProcessSuccessful(context,
                            username, password)) {
                        Profile profile = loginController
                                .getProfile(context);
                        if (profile != null) {
                            loginController.storeProfile(context, profile);
                            return true;
                        }
                    }
                } catch (Exception e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "LoginActivity AsyncTask Login Proccess");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
                return false;
            }

            protected void onPostExecute(Boolean result) {
                loginProcessResponse(result);
            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskLogin
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskLogin.execute("");
    }

    public void errorLogin() {
        new AlertDialog.Builder(this)
                .setTitle("Error de Acceso")
                .setMessage("Usuario o Contraseña Incorrectos")
                .setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                       dialog.cancel();
                    }
                })
                .show();
    }

    public static String getMD5(String input) {
        byte[] valuebytes = input.getBytes();
        try {

            String result = Base64.encodeToString(valuebytes, Base64.DEFAULT);

            return result;

        } catch (Exception e){
            FirebaseCrash.logcat(Log.ERROR, TAG, "LoginActivity getMD5");
            FirebaseCrash.report(e);
            return null;
        }
    }

}

