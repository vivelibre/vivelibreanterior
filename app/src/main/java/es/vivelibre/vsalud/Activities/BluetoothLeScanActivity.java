package es.vivelibre.vsalud.Activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.uxcam.UXCam;

import es.vivelibre.vsalud.Bluetooth.le.BluetoothLeUtils;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.Bluetooth.nonin.N3230Utils;
import es.vivelibre.vsalud.R;

/**
 * Created by mfernandez on 11/05/2016.
 */
public class BluetoothLeScanActivity extends Activity {
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mBluetoothDevice = null;
    private Intent intentHome;
    private DisplayFunctions displayFunctions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("BLE");
        //getActionBar().setTitle(R.string.title_devices);
        intentHome = new Intent();

        displayFunctions = new DisplayFunctions();

        //displayFunctions.CancelDialog();
        displayFunctions.showProgressDialog(this,
                getString(R.string.meter_type_nonin_3230),
                getString(R.string.send_meter_values_searching_device), true);

        if (!getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            displayFunctions.showToast(this,
                    getString(R.string.ble_not_supported));
            finish();
        }

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mBluetoothAdapter = bluetoothManager.getAdapter();
        }

        if (mBluetoothAdapter == null) {
            displayFunctions.showToast(this,
                    getString(R.string.error_bluetooth_not_supported));
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scanLeDevice(false);
        if (mBluetoothDevice == null){
            displayFunctions
                    .showToast(
                            this,
                            getString(R.string.send_meter_values_searching_device_error));
        }
        finish();
    }

    protected void onResume() {
        super.onResume();

        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent,
                        BluetoothLeUtils.REQUEST_ENABLE_BT);
            }
        }
        scanLeDevice(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothLeUtils.REQUEST_ENABLE_BT
                && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
    }

    private void goToControlActivity(BluetoothDevice device) {
        final Intent intent = new Intent(this, NONIN3230ControlActivity.class);
        intent.putExtra(N3230Utils.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(N3230Utils.EXTRAS_DEVICE_ADDRESS, device.getAddress());
        intent.putExtra("imagen",intentHome.getIntExtra("imagen",0));
        startActivity(intent);
        finish();
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    }
                    invalidateOptionsMenu();
                    finish();
                }
            }, BluetoothLeUtils.SCAN_PERIOD);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                if(mBluetoothAdapter == null)
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }
        invalidateOptionsMenu();
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi,
                             byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                        String deviceName = device.getName();
                        // Aqui parece que estaba el error del pete en los NONIN
                        if( deviceName != null ){
                            if (device.getName().contains("Nonin3230")){
                                mBluetoothDevice = device;
                                goToControlActivity(mBluetoothDevice);
                            }
                        }

                }
            });
        }
    };



}
