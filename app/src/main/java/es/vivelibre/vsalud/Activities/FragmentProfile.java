package es.vivelibre.vsalud.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.vsalud.Controllers.ProfileController;
import es.vivelibre.vsalud.Functions.UtilityImg;
import es.vivelibre.vsalud.R;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 07/07/2016.
 */
public class FragmentProfile extends Fragment {

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private ImageView photoUserProfile;
    private Bitmap myBitmap;
    private View v;
    private String userName;
    private static ProfileController profileController = ProfileController.getInstance();
    private CollapsingToolbarLayout toolbar;
    private TextView txtNombre;
    private Context context;
    private Button updateImg;
    private String userChoosenTask;
    Profile profile;
    private ImageView photoUserNav;
    private ProgressBar progressbar;
    private NavigationView navigationView;
    public FragmentProfile(){

    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    //Toast.makeText(getActivity(), "Falta implementación", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new FragmentHome();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_home, fragment)
                            .commit();



                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Profile ViveLibre");

        v = inflater.inflate(R.layout.fragment_profile, container, false);

        context = getContext();

        progressbar = (ProgressBar) v.findViewById(R.id.progressBar2);
        progressbar.setVisibility(View.GONE);

        navigationView = ((HomeActivity) getActivity()).navigationView;

        View nav_header = navigationView.getHeaderView(0);

        photoUserNav = (ImageView)nav_header.findViewById(R.id.photoUser);

        photoUserProfile = (ImageView)v.findViewById(R.id.imageProfile);

        toolbar = (CollapsingToolbarLayout) v.findViewById(R.id.toolbar_layout);
        txtNombre = (TextView) v.findViewById(R.id.txtNombre);

        updateImg = (Button)v.findViewById(R.id.updateImg);
        updateImg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        final FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.floatingButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startAsyncTaskTakeProfile();
            }
        });


        startAsyncTaskTakeProfile();

        return v;
    }

    private void selectImage() {
        final CharSequence[] items = { "Camara", "Galeria de Fotos",
                "Cancelar" };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Añadir Foto!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= UtilityImg.checkPermission(context);

                if (items[item].equals("Camara")) {
                    userChoosenTask ="Camara";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Galeria de Fotos")) {
                    userChoosenTask ="Galeria de Fotos";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "Fragment Profile onCaptureImageResult");
            FirebaseCrash.report(e);
            e.printStackTrace();
        } catch (IOException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "Fragment Profile onCaptureImageResult");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }

        if (profileController.GetImageProfile(context) == null){
            profile.setImageProfile(new ImageProfile("fotoperfil","jpeg",PhotoBase64FromBitmap(thumbnail)));
            profile.getImageProfile().setId(profile.getId());
        }else
            profile.getImageProfile().setData(PhotoBase64FromBitmap(thumbnail));

        photoUserProfile.setImageBitmap(thumbnail);
        photoUserNav.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData());
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "Fragment Profile SelectFromGalleryResult");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
        }

        if (profileController.GetImageProfile(context) == null){
            profile.setImageProfile(new ImageProfile("fotoperfil","jpeg",PhotoBase64FromBitmap(bm) ));
            profile.getImageProfile().setId(profile.getId());
        }else
            profile.getImageProfile().setData(PhotoBase64FromBitmap(bm));

        photoUserProfile.setImageBitmap(bm);
        photoUserNav.setImageBitmap(bm);
    }


    private String PhotoBase64FromBitmap(Bitmap thumbnail) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            startAsyncTaskSendImage(requestCode,data);
        }
    }

    private void startAsyncTaskSendImage(final int requestCode, final Intent data) {
        Log.i("Fragment Profile", "AsyncTask Send Image Profile");
        AsyncTask<String, Float, Boolean> AsyncTaskSendImage = new AsyncTask<String, Float, Boolean>() {

            @Override
            protected void onPreExecute() {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }

            protected Boolean doInBackground(String... params) {
                try {
                   return profileController.sendImageProfile(context,profile.getImageProfile());

                } catch (Exception e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "Fragment Profile startAsyncTaskSendImage");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                    return false;
                }

            }
            protected void onPostExecute(Boolean result) {
                if (result) {
                    Toast.makeText(context, R.string.foto_actualizada,
                            Toast.LENGTH_SHORT).show();
                    startAsyncTaskRefreshImage();

                }else {
                    Toast.makeText(context, R.string.foto_no_actualizada,
                            Toast.LENGTH_SHORT).show();
                    startAsyncTaskTakeProfile();
                }
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskSendImage
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskSendImage.execute("");

    }


    private void startAsyncTaskTakeProfile() {
        Log.i("Fragment Profile", "AsyncTask Get Profile Del Servicio Movil");
        AsyncTask<String, Float, Boolean> AsyncTaskLogin = new AsyncTask<String, Float, Boolean>() {

            protected void onPreExecute() {
                progressbar.setVisibility(View.VISIBLE);
            }

            protected Boolean doInBackground(String... params) {
                try {

                    // Cojemos la ultima imagen del servicio Movil
                    profile = profileController.GetProfileFromService(context);

                    if(!(profileController.GetImageProfile(context) == null)) {
                        // Cambiamos la informacion de la imagen en nuestro SQLite
                        profileController.UpdateImageProfile(context, profile.getImageProfile());

                        // Colocamos la Imagen en un Bitmap
                        myBitmap = profileController.getContactPhotoFromBase64(profile.getImageProfile().getData());
                    }
                    // Colocamos el nombre y los apellidos en su correspondiente sitio de la interfaz
                    userName = profile.getName()+ " " + profile.getFirst_name() + " " + profile.getLast_name();

                } catch (Exception e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "Fragment Profile StartAsyncTaskTakeProfile");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
                return false;
            }

            protected void onPostExecute(Boolean result) {
                progressbar.setVisibility(View.GONE);
                txtNombre.setText(userName);
                if (myBitmap != null) {
                    photoUserProfile.setImageBitmap(myBitmap);
                    photoUserNav.setImageBitmap(myBitmap);
                    photoUserProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    //photoUser.setAdjustViewBounds(true);
                }
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskLogin
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskLogin.execute("");
    }

    private void startAsyncTaskRefreshImage() {
        Log.i("Fragment Profile", "AsyncTask Update Profile from SQLite");
        AsyncTask<String, Float, Boolean> AsyncTaskLogin = new AsyncTask<String, Float, Boolean>() {

            protected void onPreExecute() {
                progressbar.setVisibility(View.VISIBLE);
            }

            protected Boolean doInBackground(String... params) {
                try {
                    Profile profile = profileController
                            .getProfile(context);

                    if (profile != null) {

                         profileController.UpdateImageProfile(context, profile.getImageProfile());

                        return true;
                    }

                } catch (Exception e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "Fragment Profile AsyncTask Update Profile from SQLite");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
                return false;
            }

            protected void onPostExecute(Boolean result) {
                progressbar.setVisibility(View.GONE);

            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskLogin
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskLogin.execute("");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
