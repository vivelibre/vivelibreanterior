package es.vivelibre.vsalud.Activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.WeightValue;
import es.vivelibre.vsalud.Bluetooth.and.BluetoothAndService;
import es.vivelibre.vsalud.Controllers.MeterValueController;
import es.vivelibre.vsalud.Bluetooth.and.ANDDevicesUtils;
import es.vivelibre.vsalud.Functions.CommunicationPacket;
import es.vivelibre.vsalud.Functions.ConnectivityFunctions;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.Functions.Sections.DataBloodPressureSection;
import es.vivelibre.vsalud.Functions.Sections.DataScaleSection;
import es.vivelibre.vsalud.Functions.Sections.DataSection;
import es.vivelibre.vsalud.R;

import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class ANDControlActivity extends AppCompatActivity implements View.OnClickListener {

    private MeterValueController _meterValueController = MeterValueController
            .getInstance();

    private String mConnectedDeviceName = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothAndService mBluetoothService = null;
    private ArrayList<MeterValue> valuesToSend = new ArrayList<MeterValue>();
    private Context context;
    private ImageView imagen;
    private FloatingActionButton botonIrHome;
    private  Intent intent;
    private ArrayList<MeterValue> meterValuesToResult;
    private Button botonReconnect;
    private String value;
    ArrayList<Pair<Integer, byte[]>> dataRead = new ArrayList<Pair<Integer, byte[]>>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("AND Bluetooth");

        intent = getIntent();
        setContentView(R.layout.and_control_activity);

        imagen = (ImageView)findViewById(R.id.listenerImage);
        imagen.setImageResource(intent.getIntExtra("imagen",0));

        botonIrHome = (FloatingActionButton)findViewById(R.id.listenerClose);

        //Cambiar nombre en vista
        botonReconnect = (Button)findViewById(R.id.listenerManual);
        botonReconnect.setOnClickListener(this);
        botonIrHome.setOnClickListener(this);

        context = getApplicationContext();


        //setActionBar();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,
                    ANDDevicesUtils.REQUEST_ENABLE_BT);
        } else {
            setupTrace();
        }

        //Quitado el dialogo de peticion de bluetooth visible durante 300 segundos
        //ensureDiscoverable();
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if (mBluetoothService != null) {
            if (mBluetoothService.getState() == BluetoothAndService.STATE_NONE) {
                mBluetoothService.start();
            }
        } else
            setupTrace();
    }

    private void setupTrace() {
        DisplayFunctions.showProgressDialog(this,
                getString(R.string.meter_type_default_and_device),
                getString(R.string.send_meter_values_searching_device), true);
        mBluetoothService = new BluetoothAndService(this, mHandler);
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBluetoothService != null)
            mBluetoothService.stop();
    }

    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(
                    BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,
                    ANDDevicesUtils.DISCOVERABLE_DURATION);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     *
     * @param message
     *            A string of text to send.
     */
    private void sendMessage(String message) {
        if (mBluetoothService.getState() != BluetoothAndService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        if (message.length() > 0) {
            byte[] send = message.getBytes();

            mBluetoothService.write(send);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ANDDevicesUtils.REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    setupTrace();
                } else {
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    private void processInput(int len, byte[] rxpacket) {
        ByteArrayOutputStream mPacket = new ByteArrayOutputStream(128);
        mPacket.write(rxpacket, 0, len);
        ByteArrayInputStream pdu = new ByteArrayInputStream(
                mPacket.toByteArray());
        Pair<Boolean, Boolean> checkPacketCorrect = CommunicationPacket
                .checkPacket(pdu, mPacket);

        if (checkPacketCorrect.first && checkPacketCorrect.second) {
            CommunicationPacket communicationPacket = new CommunicationPacket(
                    pdu);
            if (communicationPacket.isCorrectInput()) {
                DataSection dataSection = communicationPacket.getDataSection();

                if (dataSection instanceof DataBloodPressureSection) {
                    BloodPressureValue bloodPressureValue = ANDDevicesUtils
                            .getBloodPressureResult(communicationPacket);
                    if (bloodPressureValue != null)
                        valuesToSend.add(bloodPressureValue);
                    HeartRateValue heartRateValue = ANDDevicesUtils
                            .getHeartRateResult(communicationPacket);
                    if (heartRateValue != null)
                        valuesToSend.add(heartRateValue);
                } else if (dataSection instanceof DataScaleSection) {
                    WeightValue weightValue = ANDDevicesUtils
                            .getWeightResult(communicationPacket);
                    if (weightValue != null)
                        valuesToSend.add(weightValue);
                }
                try {
                    pdu.close();
                } catch (IOException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "ANDControlActivity process inputs");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendResponseToDevice(ByteArrayOutputStream mPacket,
                                      String response) {
        sendMessage(response);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

	/*
	 * PROCESS INPUTS
	 */

    private void startAsyncTaskProcessInputs() {
        Log.i("AND Control Activity", "AsyncTask Process Inputs");
        AsyncTask<String, Float, Boolean> AsyncTaskProcessInputs = new AsyncTask<String, Float, Boolean>() {

            protected void onPreExecute() {
                mHandler.obtainMessage(ANDDevicesUtils.MESSAGE_SENDING_VALUES,
                        1, 0).sendToTarget();
            }

            protected Boolean doInBackground(String... params) {
                Iterator<Pair<Integer, byte[]>> it = dataRead.iterator();
                while (it.hasNext()) {
                    Pair<Integer, byte[]> data = it.next();
                    processInput(data.first, data.second);
                }
                return valuesToSend.size() > 0;
            }

            protected void onPostExecute(Boolean result) {
                if (result) {
                    mHandler.obtainMessage(
                            ANDDevicesUtils.MESSAGE_PROCESS_INPUTS, 0, 0)
                            .sendToTarget();
                } else {
                    mHandler.obtainMessage(
                            ANDDevicesUtils.MESSAGE_PROCESS_INPUTS, 1, 0)
                            .sendToTarget();
                }
            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskProcessInputs.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskProcessInputs.execute("");
    }

	/*
	 * SEND RESULT
	 */

    private void startAsyncTaskSendResults() {
        Log.i("AND Control Activity", "AsyncTask Send Result");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {

            protected void onPreExecute() {
                Date horaActual = Calendar.getInstance().getTime();
                //Calculamos si hay diferencia de la ultima medida con la hora actual del telefono para saber si hay que sumarle
                int diferencia = horaActual.getHours() - valuesToSend.get(valuesToSend.size()-1).getDateRead().getHours();
                    for (MeterValue values: valuesToSend) {
                        Calendar cal = Calendar.getInstance(); // creates calendar
                        cal.setTime(values.getDateRead()); // sets calendar time/date
                        cal.add(Calendar.HOUR_OF_DAY, diferencia); // adds one hour
                        values.setDateRead(cal.getTime());
                    }

                if (intent.getIntExtra("imagen",0) == R.drawable.tensiometro) {
                    String[] separated = valuesToSend.get(valuesToSend.size()-2).getValue().split("@");
                    if (valuesToSend.get(valuesToSend.size()-1).getValue()== null){
                        // Si el valor del pulso al medirlo con el tensiometro es nulo (Medida no correcta)
                        valuesToSend.get(valuesToSend.size()-1).setValue("E1");
                        valuesToSend.get(valuesToSend.size()-2).setValidate(true);
                        valuesToSend.get(valuesToSend.size()-1).setValidate(false);
                        value = separated[0]+ ":" + separated[1] + ":" + "Pulso irregular";
                    }
                    else{
                        valuesToSend.get(valuesToSend.size()-2).setValidate(true);
                        valuesToSend.get(valuesToSend.size()-1).setValidate(true);
                        value = separated[0]+ ":" + separated[1] + ":" + valuesToSend.get(valuesToSend.size()-1).getValue();
                    }
                }else{
                    value = valuesToSend.get(valuesToSend.size()-1).getValue() + " kg";
                }
            }

            protected ArrayList<MeterValue> doInBackground(String... params) {
                return _meterValueController.AddNewListMeterValues(context,
                        valuesToSend);
            }

            protected void onPostExecute(ArrayList<MeterValue> meterValues) {
                meterValuesToResult = meterValues;
                DisplayFunctions.CancelDialog();
                if (meterValues == null
                        || (meterValues != null && meterValues.size() > 0)) {
                    mHandler.obtainMessage(
                            ANDDevicesUtils.MESSAGE_SEND_VALUES_COMPLETE, 1, 0)
                            .sendToTarget();
                } else {
                    mHandler.obtainMessage(
                            ANDDevicesUtils.MESSAGE_SEND_VALUES_COMPLETE, 0, 0)
                            .sendToTarget();
                }
            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }

    // protected void sendLowBatteryAlert(CommunicationPacket
    // communicationPacket,
    // Pair<MeterValueDTO, MeterValueDTO> result) {
    //
    // if (result != null && communicationPacket.isLowBattery()) {
    // MeterValueDTO meterValueDTO = (result.first != null) ? result.first
    // : result.second;
    //
    // LowBatteryAlertDTO lowBatteryAlertDTO = new LowBatteryAlertDTO();
    // lowBatteryAlertDTO.setMeterId(UUID.fromString(meterValueDTO
    // .getMeterId()));
    // }
    //
    // }

	/*
	 * HANDLER EVENTS
	 */

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ANDDevicesUtils.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothAndService.STATE_CONNECTING:
                            mBluetoothService.write(ANDDevicesUtils.GetDate());
                            break;
                        case BluetoothAndService.STATE_CONNECTED:
                            break;
                    }
                    break;
                case ANDDevicesUtils.MESSAGE_WRITE:
                    break;
                case ANDDevicesUtils.MESSAGE_READ:
                    dataRead.add(new Pair<Integer, byte[]>(msg.arg1,
                            (byte[]) msg.obj));
                    sendResponseToDevice(null, "PWA4");
                    break;
                case ANDDevicesUtils.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(
                            ANDDevicesUtils.DEVICE_NAME);
                    manageStateConnecting();
                    break;
                case ANDDevicesUtils.MESSAGE_CONNECTION_LOST:
                    manageConnectionLost();
                    break;
                case ANDDevicesUtils.MESSAGE_PROCESS_INPUTS:
                    manageProcessInputs(msg.arg1 == 0);
                    break;
                case ANDDevicesUtils.MESSAGE_SENDING_VALUES:
                    manageStateSendingValues();
                    break;
                case ANDDevicesUtils.MESSAGE_SEND_VALUES_COMPLETE:
                    manageStateSendValuesComplete(msg.arg1 == 0);
                    break;
                case ANDDevicesUtils.MESSAGE_NETWORK_CONNECTIVITY_ERROR:
                    manageStateNetworkConnectivityError();
                    break;
                case ANDDevicesUtils.MESSAGE_ERROR:
                    manageStateError();
                    break;
            }
        }
    };

    protected void manageConnectionLost() {
        DisplayFunctions.changeTextProgressDialog(this, mConnectedDeviceName,
                getString(R.string.send_meter_values_process_inputs), true);
        startAsyncTaskProcessInputs();
    }

    protected void manageStateConnecting() {
        DisplayFunctions.changeTextProgressDialog(this,
                getString(R.string.meter_type_default_and_device),
                getString(R.string.send_meter_values_connecting), true);
    }

    protected void manageProcessInputs(boolean success) {
        if (success) {
            if (ConnectivityFunctions.networkAvailable(context)) {
                startAsyncTaskSendResults();
            } else {
                mHandler.obtainMessage(
                        ANDDevicesUtils.MESSAGE_NETWORK_CONNECTIVITY_ERROR, 1,
                        0).sendToTarget();
            }
        } else {
            mHandler.obtainMessage(ANDDevicesUtils.MESSAGE_ERROR, 1, 0)
                    .sendToTarget();
        }
    }

    protected void manageStateSendingValues() {
        DisplayFunctions.changeTextProgressDialog(this, mConnectedDeviceName,
                getString(R.string.send_meter_values_sending_values), true);
    }

    protected void manageStateSendValuesComplete(boolean success) {
        // Si el valor del pulso al medirlo con el tensiometro es nulo (Medida no correcta)
        if (valuesToSend.get(valuesToSend.size()-1).getValue()== null)
            DisplayFunctions
                .showToast(
                        context,
                        getString(R.string.send_meter_values_sending_values_heart_rate_error));
        else if (success) DisplayFunctions
                    .showToast(
                            context,
                            getString(R.string.send_meter_values_sending_values_success));
        else DisplayFunctions
                    .showToast(
                            context,
                            getString(R.string.send_meter_values_sending_values_error));
        //llamamos al metodo que va a la pagina de resultados
        launchResultActivity(success);
    }

    protected void manageStateNetworkConnectivityError() {
        DisplayFunctions.showToast(context,
                getString(R.string.general_network_connection_problems));
        DisplayFunctions.CancelDialog();
        finish();
    }

    protected void manageStateError() {
        DisplayFunctions.showToast(context,
                getString(R.string.send_meter_values_error_connecting_device));
        DisplayFunctions.CancelDialog();
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v == botonIrHome) {
            launchMainMenuActivity();
        }else if (v == botonReconnect){

            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent,
                        ANDDevicesUtils.REQUEST_ENABLE_BT);
            } else {
                setupTrace();
            }
            //Quitado el dialogo de peticion de bluetooth visible durante 300 segundos
            //ensureDiscoverable();
        }
    }

    private void launchMainMenuActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startActivity(homeIntent);
        finish();
    }
    private void launchResultActivity(boolean success){
        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra("success", success);
        resultIntent.putExtra("imagen", intent.getIntExtra("imagen",0));
        resultIntent.putExtra("value",value);
        startActivity(resultIntent);
        finish();
    }
}
