package es.vivelibre.vsalud.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;

import com.uxcam.UXCam;

import java.io.File;

import es.vivelibre.vsalud.R;
import es.vivelibre.models.Utils.SQLiteModel;
import es.vivelibre.models.Utils.SharedCustomPreferencesUtils;

/**
 * Created by maca- on 16/05/2016.
 */
public class Disconnect extends AppCompatActivity {

    private String TAG = getClass().getName();
    private SQLiteModel model_sqLite = SQLiteModel.getInstance();
    private int ALARM_SERVICE_CHECK_NEWS = R.string.item_menu_notis;
    private int DISCONNECTION_TIME_OUT = 10;
    private int time_disconnection_time_out = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Desconexión");

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.disconnect);

        clearApplicationData();
        Disconnect.doRestart(this);
        //System.exit(0);
    }
    public static void doRestart(Activity anyActivity) {
        anyActivity.startActivity(new Intent(anyActivity.getApplicationContext(), LoginActivity.class));
    }



    private void clearSharedPreferences() {
        Log.d(TAG, "Eliminando SharedPreferences...");
        SharedCustomPreferencesUtils.clearPreferences(getApplicationContext());
    }


    private void clearSQLiteTables() {
        Log.d(TAG, "Eliminando tablas SQLite...");
        model_sqLite.borrarTablaAlertSQLite(getApplicationContext());
        model_sqLite.borrarTablaImagePrfileSQLite(getApplicationContext());
        model_sqLite.borrarTablaMeterSQLite(getApplicationContext());
        model_sqLite.borrarTablaMeterValueSQLite(getApplicationContext());
        model_sqLite.borrarTablaProfileSQLite(getApplicationContext());
        model_sqLite.borrarTablaHelperSQLite(getApplicationContext());
    }
    public void clearApplicationData() {

        //con esto limpiamos el SharedPreferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.clear();
        clearSharedPreferences();
        clearSQLiteTables();

        File cache = this.getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                //if (!s.equals("files")){
                    //if (!s.equals("lib")) {
                    if (!s.equals("files") && !s.equals("databases")) {
                        deleteDir(new File(appDir, s));
                    }
                //}
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
}
