package es.vivelibre.vsalud.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.uxcam.UXCam;

import java.util.UUID;

import es.vivelibre.vsalud.R;

public class ListenerActivity extends AppCompatActivity implements View.OnClickListener {

    //*************************************************
    //* ESTA ACTIVIDAD LA ESTOY USANDO COMO PRUEBAS!! *
    //*************************************************

    private Context context;
    private Activity activity;
    private ImageView imagen;
    private Button botonReinicio;
    private TextView text;
    private FloatingActionButton  botonIrHome;
    private Intent intent;

    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;

    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");

        intent = getIntent();
        setContentView(R.layout.activity_listener);



        botonReinicio = (Button)findViewById(R.id.listenerManual);
        botonIrHome = (FloatingActionButton)findViewById(R.id.listenerClose);
        text=(TextView)findViewById(R.id.listenerState);

        imagen= (ImageView)findViewById(R.id.listenerImage);

        imagen.setImageResource(intent.getIntExtra("imagen",0));

        botonReinicio.setOnClickListener(this);
        botonIrHome.setOnClickListener(this);
        initializeViews();

    }

    private void initializeViews() {
        activity = this;
        context = getApplicationContext();

    }

    @Override
    public void onClick(View view) {
        if (view == botonIrHome) {
            launchMainMenuActivity();
        }else if(view == botonReinicio){

            Intent resultIntent = new Intent(this, ResultActivity.class);

            resultIntent.putExtra("imagen", intent.getIntExtra("imagen",0));

            startActivity(resultIntent);

            finish();
        }
    }
    public void changeT(String str)
    {
        text.setText(str);
    }

    private void launchMainMenuActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startActivity(homeIntent);
        finish();
    }




}
