package es.vivelibre.vsalud.Activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.models.Meter.MeterValueType;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.models.Meter.WeightValue;
import es.vivelibre.models.Utils.CardViewModel;
import es.vivelibre.services.Meter.MeterValueAppService;
import es.vivelibre.vsalud.Adapters.CardViewResultsAdapter;
import es.vivelibre.vsalud.Controllers.MeterValuesController;
import es.vivelibre.vsalud.Functions.ConnectivityFunctions;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.R;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentResults.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentResults extends Fragment{

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;//para la lista de items
    private CardViewResultsAdapter adapter;
    private String typeParameter;
    private LineChart chart;
    private Context context;
    private int meterValueType;
    private ProgressBar progressbar;
    private RecyclerView.LayoutManager lmanager;
    /*private FloatingActionButton calendarResults;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;*/
    private Calendar calendar;
    private View v;
    private TextView dateResult;
    private TextView text_no_results;
    private ArrayList<MeterValue> mMeterValuesList = null;
    private static MeterValuesController _meterValuesController = MeterValuesController
            .getInstance();
    private static MeterValueAppService _meterValueAppService = MeterValueAppService
            .getInstance();
    private SwipeRefreshLayout refreshLayout;

    public FragmentResults() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        calendar =Calendar.getInstance(); //obtiene la fecha de hoy
        calendar.add(Calendar.DATE, -30);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Resultados");
        //Buscamos el tipo
        meterValueType = convertirType();

        this.context = getActivity();

        typeParameter = getArguments().getString("type");

        v = inflater.inflate(R.layout.fragment_results, container, false);
        TextView typeResult = (TextView)v.findViewById(R.id.resultsType);
        dateResult = (TextView)v.findViewById(R.id.resultsDate);
        text_no_results = (TextView)v.findViewById(R.id.text_no_results);
        text_no_results.setVisibility(View.INVISIBLE);

        // Obtener el refreshLayout
        refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.container_refresh);

        // Iniciar la tarea asíncrona al revelar el indicador
        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (ConnectivityFunctions.networkAvailable(context))
                            startAsyncTaskGetMeterValues(null);
                        else
                            DisplayFunctions.showToast(context,
                                    getString(R.string.general_network_connection_problems));

                        configureChart();
                    }
                }
        );

        typeResult.setText(typeParameter);

        progressbar = (ProgressBar) v.findViewById(R.id.progressBar);
        progressbar.setVisibility(View.GONE);

        //Inicializamos la lista de items
        recyclerView = (RecyclerView) v.findViewById(R.id.recicladorResults);
        //Inicializamos grafica
        chart = (LineChart) v.findViewById(R.id.chartResults);

        /*dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());*/
        dateResult.setText("Últimos 30 resultados.");


       /* calendarResults = (FloatingActionButton)v.findViewById(R.id.calendarResults);

        calendarResults.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Calendar newCalendar = Calendar.getInstance();

                datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        long t= newDate.getTimeInMillis();
                        Date afterAddingfiveMins=new Date(t + (5 * 60000));
                        startAsyncTaskGetMeterValues(afterAddingfiveMins);

                        dateResult = (TextView)v.findViewById(R.id.resultsDate);
                        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                        dateResult.setText("Resultados del " + dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });
*/


        if (ConnectivityFunctions.networkAvailable(context))
            startAsyncTaskGetMeterValues(null);
        else
            DisplayFunctions.showToast(context,
                    getString(R.string.general_network_connection_problems));

        configureChart();


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    //Toast.makeText(getActivity(), "Falta implementación", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new FragmentHome();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_home, fragment)
                            .commit();

                    return true;
                }
                return false;
            }
        });

    }

    private String desconvertirType() {

        if(getArguments().getString("type") == getString(R.string.weight))
            return "WeightValue";
        else if(getArguments().getString("type") == getString(R.string.oxygenSaturation))
            return "OxygenSaturationValue";
        else if (getArguments().getString("type") == getString(R.string.hearthRate))
            return "HeartRateValue";
        else if (getArguments().getString("type") == getString(R.string.bloodPressure))
            return "BloodPressureValue";
        else
            return "";
    }

    private int convertirType() {

        if(getArguments().getString("type") == getString(R.string.weight))
            return MeterValueType.WEIGHT.getValue();
        else if(getArguments().getString("type") == getString(R.string.oxygenSaturation))
            return MeterValueType.OXYGEN_SATURATION.getValue();
        else if (getArguments().getString("type") == getString(R.string.hearthRate))
            return MeterValueType.HEART_RATE.getValue();
        else if (getArguments().getString("type") == getString(R.string.bloodPressure))
            return MeterValueType.BLOOD_PRESSURE.getValue();
        else
            return 1;

    }

    private void startAsyncTaskGetMeterValues(final Date valorDate) {
        Log.i("FRAGMENT RESULTS", "AsyncTask Get Meter Values");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {
            private List items;
            private Date fechaIni;
            private Date valorfecha;
            protected void onPreExecute() {
                text_no_results.setVisibility(View.INVISIBLE);
                progressbar.setVisibility(View.VISIBLE);
                items = new ArrayList();
                recyclerView.setHasFixedSize(true);
                // Usar un administrador para LinearLayout
                lmanager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(lmanager);

                if (valorDate == null){
                    Calendar newDate = Calendar.getInstance();
                    long t= newDate.getTimeInMillis();
                    Date afterAddingfiveMins=new Date(t + (10 * 60000));

                    valorfecha = new Date(Long.MIN_VALUE);
                    fechaIni = new Date(Long.MIN_VALUE);
                }else{
                    valorfecha = valorDate;
                    fechaIni = null;
                }
                adapter = new CardViewResultsAdapter(getActivity(),items);
                recyclerView.setAdapter(adapter);

            }

            protected ArrayList<MeterValue> doInBackground(String... params) {
                try {
                    ArrayList<MeterValue> meterValues = null;

                    if ((meterValues == null || meterValues.size() == 0)
                            && (ConnectivityFunctions.networkAvailable(context)))
                        meterValues = _meterValuesController
                                .GetMeterValuesByTypeFromServer(context,desconvertirType(),
                                        getMeterToShow(meterValueType),
                                        fechaIni ,valorfecha);
                    return meterValues;
                } catch (Exception e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "FragmentResults startAsyncTaskGetMeterValues");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(ArrayList<MeterValue> result) {
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                String strDate;

                // Return true if the fragment is currently added to its activity. Con esto comprobamos que si le da atras no pete
                if(isAdded()) {
                    getResources().getString(R.string.app_name);
                    if (result != null) {
                        mMeterValuesList = result;
                        if (mMeterValuesList.size() > 0) {
                            Iterator<MeterValue> it = mMeterValuesList
                                    .iterator();
                            if (!items.isEmpty()) {
                                items.clear();
                            }

                            while (it.hasNext()) {
                                MeterValue meterValue = it.next();
                                strDate = format.format(meterValue.getDateRead());
                                //Meter dato en lista
                                if (typeParameter == getString(R.string.bloodPressure)) {
                                    String[] separated = meterValue.getValue().split("@");
                                    items.add(new CardViewModel(getUnit(typeParameter), separated[0] + "/" + separated[1], R.drawable.nonin_3230, strDate, ""));
                                } else {
                                    if ((typeParameter == getString(R.string.hearthRate) && (meterValue.getValue().equals("E511") || meterValue.getValue().equals("E1")) && !meterValue.isValidate())||
                                            (typeParameter == getString(R.string.oxygenSaturation) && meterValue.getValue().equals("E127") && !meterValue.isValidate()))
                                    {
                                        if (meterValue.getValue().equals("E1")){
                                            items.add(new CardViewModel("Pulso irregular", "", R.drawable.nonin_3230, strDate, ""));
                                        }else {
                                            items.add(new CardViewModel("Medida errónea", "", R.drawable.nonin_3230, strDate, ""));
                                        }
                                    }else{
                                        items.add(new CardViewModel(getUnit(typeParameter), meterValue.getValue(), R.drawable.nonin_3230, strDate, ""));
                                    }
                                }
                                //addEntry(meterValue);
                            }
                            adapter = new CardViewResultsAdapter(getActivity(), items);
                            recyclerView.setAdapter(adapter);
                            addEntry();
                            progressbar.setVisibility(View.GONE);
                            // Parar la animación del indicador
                            refreshLayout.setRefreshing(false);
                        } else {
                            DisplayFunctions
                                    .showToast(
                                            context,
                                            getString(R.string.show_meter_values_empty_results));
                            progressbar.setVisibility(View.GONE);
                            chart.clear();
                            text_no_results.setVisibility(View.VISIBLE);
                            // Parar la animación del indicador
                            refreshLayout.setRefreshing(false);
                        }
                    } else {
                        DisplayFunctions.showToast(context,
                                getString(R.string.show_meter_values_error));
                        progressbar.setVisibility(View.GONE);
                    }
                }

            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }



    private MeterValueComplete getMeterToShow(int type) {
        if (type == MeterValueType.BLOOD_PRESSURE.getValue()) {
            return new MeterValueComplete(null, new BloodPressureValue(),
                    null);
        } else if (type == MeterValueType.HEART_RATE.getValue()) {
            return new MeterValueComplete(null, new HeartRateValue(),
                    null);
        } else if (type == MeterValueType.OXYGEN_SATURATION.getValue()) {
            return new MeterValueComplete(null,
                    new OxygenSaturationValue(), null);
        } else if (type == MeterValueType.WEIGHT.getValue()) {
            return new MeterValueComplete(null, new WeightValue(), null);
        }
        return null;
    }

    private MeterValue getEntityToShow(int type) {
        if (type == MeterValueType.BLOOD_PRESSURE.getValue()) {
            return new BloodPressureValue();
        } else if (type == MeterValueType.HEART_RATE.getValue()) {
            return new HeartRateValue();
        } else if (type == MeterValueType.OXYGEN_SATURATION.getValue()) {
            return new OxygenSaturationValue();
        } else if (type == MeterValueType.WEIGHT.getValue()) {
            return new WeightValue();
        }
        return null;
    }





    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    private void configureChart()
    {



        chart.setDescription("");
        chart.setNoDataTextDescription("No hay datos para mostrar");

        //activamos highlighting
        chart.setHighlightPerDragEnabled(false);

        //Activamos gestos
        chart.setTouchEnabled(true);

        //Activamos drag y scaling
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);

        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);

        chart.setPinchZoom(true);

        chart.setBackgroundColor(Color.WHITE);

        LineData data = new LineData();

        data.setValueTextColor(Color.BLACK);


        chart.setData(data);

        Legend l = chart.getLegend();

        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.BLACK);

        XAxis xl = chart.getXAxis();
        xl.setTextColor(getResources().getColor(R.color.colorPrimary));
        xl.setDrawAxisLine(true);
        xl.setAvoidFirstLastClipping(false);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setLabelsToSkip(0);
        xl.setLabelRotationAngle(-90);

        YAxis yl = chart.getAxisLeft();
        yl.setTextColor(getResources().getColor(R.color.colorPrimary));

        YAxis yl2 = chart.getAxisRight();
        yl2.setEnabled(false);

    }



    private void addEntry() {

        float valMax = 0;

        Collections.reverse(mMeterValuesList);

        if(typeParameter == getString(R.string.bloodPressure)){
            ArrayList<String> xVals = new ArrayList<String>();
            ArrayList<Entry> yValsSYS = new ArrayList<Entry>();
            ArrayList<Entry> yValsDIA = new ArrayList<Entry>();
            ArrayList<Entry> yValsPUL = new ArrayList<Entry>(); // Este de momento no lo pongo



            if (mMeterValuesList != null) {
                for (int i = 0; i < mMeterValuesList.size(); i++) {
                    String[] separated = mMeterValuesList.get(i).getValue().split("@"); //Aqui divido el String por @
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                    xVals.add(format.format(mMeterValuesList.get(i).getDateRead()));

                    yValsSYS.add(new Entry(Integer.valueOf(separated[0]), i));

                    if(!separated[1].equals("-"))yValsDIA.add(new Entry(Integer.valueOf(separated[1]), i));

                    if(Float.valueOf(separated[0]) > valMax ) valMax=Integer.valueOf(separated[0]);
                }

                LineDataSet setSYS;
                LineDataSet setDIA;

                setSYS = new LineDataSet(yValsSYS, "Sist.");
                setSYS.setDrawCubic(false);
                setSYS.setCubicIntensity(0.2f);
                setSYS.setAxisDependency(YAxis.AxisDependency.LEFT);
                setSYS.setColor(ColorTemplate.getHoloBlue());
                setSYS.setCircleColor(Color.BLACK);
                setSYS.setLineWidth(2f);
                setSYS.setFillAlpha(65);
                setSYS.setFillColor(ColorTemplate.getHoloBlue());
                setSYS.setHighLightColor(Color.rgb(244, 177, 177));
                setSYS.setValueTextColor(Color.BLACK);
                setSYS.setValueTextSize(10f);

                setDIA = new LineDataSet(yValsDIA, "Dias.");
                setDIA.setDrawCubic(false);
                setDIA.setCubicIntensity(0.2f);
                setDIA.setAxisDependency(YAxis.AxisDependency.LEFT);
                setDIA.setColor(Color.RED);
                setDIA.setCircleColor(Color.BLACK);
                setDIA.setLineWidth(2f);
                setDIA.setFillAlpha(65);
                setDIA.setFillColor(ColorTemplate.getHoloBlue());
                setDIA.setHighLightColor(Color.rgb(244, 177, 177));
                setDIA.setValueTextColor(Color.BLACK);
                setDIA.setValueTextSize(10f);

                ArrayList<ILineDataSet> dataSets  = new ArrayList<ILineDataSet>();

                dataSets.add(setSYS); // add the datasets
                dataSets.add(setDIA); // add the datasets

                // create a data object with the datasets
                LineData data = new LineData(xVals, dataSets);
                data.setValueTextColor(Color.BLACK);
                data.setValueTextSize(9f);

                // set data
                chart.setData(data);


            }
        }else {

            ArrayList<String> xVals = new ArrayList<String>();
            ArrayList<Entry> yVals1 = new ArrayList<Entry>();

            if (mMeterValuesList != null) {
                int j = 0;
                for (int i = 0; i < mMeterValuesList.size(); i++) {
                    if(typeParameter == getString(R.string.oxygenSaturation) && !mMeterValuesList.get(i).getValue().equals("E127") && mMeterValuesList.get(i).isValidate() ||
                       typeParameter == getString(R.string.hearthRate) && !mMeterValuesList.get(i).getValue().equals("E511") && mMeterValuesList.get(i).isValidate() ||
                       typeParameter == getString(R.string.weight))
                       /* || typeParameter == getString(R.string.bloodPressure) && !mMeterValuesList.get(i).getValue().equals("null") && mMeterValuesList.get(i).isValidate())*/
                    {
                        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                        xVals.add(format.format(mMeterValuesList.get(i).getDateRead()));
                        yVals1.add(new Entry(Float.valueOf(mMeterValuesList.get(i).getValue().replace(",", ".")), j));

                        if(Float.valueOf(mMeterValuesList.get(i).getValue().replace(",", ".")) >= valMax ) valMax=Float.valueOf(mMeterValuesList.get(i).getValue().replace(",", "."));
                        j++;
                    }
                }

                LineDataSet set;

                set = new LineDataSet(yVals1, getUnit(typeParameter));
                set.setDrawCubic(false);
                set.setCubicIntensity(0.2f);
                set.setAxisDependency(YAxis.AxisDependency.LEFT);
                set.setColor(ColorTemplate.getHoloBlue());
                set.setCircleColor(ColorTemplate.getHoloBlue());
                set.setLineWidth(2f);
                set.setCircleSize(4f);
                set.setFillAlpha(65);
                set.setFillColor(ColorTemplate.getHoloBlue());
                set.setHighLightColor(Color.rgb(244, 177, 177));
                set.setValueTextColor(Color.BLACK);
                set.setValueTextSize(10f);

                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                dataSets.add(set); // add the datasets

                // create a data object with the datasets
                LineData data = new LineData(xVals, dataSets);
                data.setValueTextColor(Color.BLACK);
                data.setValueTextSize(9f);

                // set data
                chart.setData(data);

                chart.notifyDataSetChanged();

            }
        }

        chart.notifyDataSetChanged();
        chart.invalidate();

    }


        private LineDataSet createSet() {

            LineDataSet set = new LineDataSet(null, getleyendText(typeParameter));

            set.setDrawCubic(false);
            set.setCubicIntensity(0.2f);
            set.setAxisDependency(YAxis.AxisDependency.LEFT);
            set.setColor(ColorTemplate.getHoloBlue());
            set.setCircleColor(ColorTemplate.getHoloBlue());
            set.setLineWidth(2f);
            set.setCircleSize(4f);
            set.setFillAlpha(65);
            set.setFillColor(ColorTemplate.getHoloBlue());
            set.setHighLightColor(Color.rgb(244, 177, 177));
            set.setValueTextColor(Color.BLACK);
            set.setValueTextSize(10f);

            return set;

        }


        private String getleyendText(String type)
        {
            if(type == getString(R.string.weight))
                return "Peso (kg)";
            else if(type == getString(R.string.oxygenSaturation))
                return "Saturación (%)";
            else if (type == getString(R.string.hearthRate))
                return "Pulso (p.p.m.)";
            else if (type == getString(R.string.bloodPressure))
                return "Tension (mmHg)";
            else
                return "";

        }

    private String getUnit(String type)
    {
        if(type == getString(R.string.weight))
            return "kg";
        else if(type == getString(R.string.oxygenSaturation))
            return "%";
        else if (type == getString(R.string.hearthRate))
            return "p.p.m.";
        else if (type == getString(R.string.bloodPressure))
            return "mmHg";
        else
            return "";

    }

}
