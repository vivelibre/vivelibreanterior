package es.vivelibre.vsalud.Activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.uxcam.UXCam;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.models.Utils.CardViewModel;
import es.vivelibre.vsalud.Adapters.CardViewHistoryAdapter;
import es.vivelibre.vsalud.Controllers.MeterValuesController;
import es.vivelibre.vsalud.Functions.ConnectivityFunctions;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentHistory.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentHistory extends Fragment implements FragmentHome.OnFragmentInteractionListener{

    private OnFragmentInteractionListener mListener;
    private Context context;
    private ProgressBar progressbar;
    private RecyclerView recyclerView;//para la lista de items
    private CardViewHistoryAdapter adapter;
    private RecyclerView.LayoutManager lmanager;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private LineChart chart;

    private View v;
    private Menu bar_menu_item;
    private ArrayList<MeterValue> mMeterValuesList = null;
    private static MeterValuesController _meterValuesController = MeterValuesController
            .getInstance();
    private ArrayList<MeterValue> mMeterTemp;
    private TextView text_no_results;

    private SwipeRefreshLayout refreshLayout;

    public FragmentHistory() {
        // Required empty public constructor
    }
    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    //Toast.makeText(getActivity(), "Falta implementación", Toast.LENGTH_SHORT).show();
                    //return true;

                    Fragment fragment = new FragmentHome();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_home, fragment)
                            .commit();

                    return true;
                }
                return false;
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Historial");

        this.context = getActivity();



        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_history, container, false);

        progressbar = (ProgressBar) v.findViewById(R.id.progressBar);
        progressbar.setVisibility(View.GONE);

        //Inicializamos la lista de items
        recyclerView = (RecyclerView) v.findViewById(R.id.recicladorHistory);

        chart = (LineChart) v.findViewById(R.id.chartHistory);
        text_no_results = (TextView)v.findViewById(R.id.text_no_results);
        text_no_results.setVisibility(v.INVISIBLE);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        // Obtener el refreshLayout
        refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.container_refresh);

        // Iniciar la tarea asíncrona al revelar el indicador
        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (ConnectivityFunctions.networkAvailable(context)) {
                            startAsyncTaskGetMetersValues();

                        }
                        else
                            DisplayFunctions.showToast(context,
                                    getString(R.string.general_network_connection_problems));


                        configureChart();
                    }
                }
        );

        if (ConnectivityFunctions.networkAvailable(context)) {
            startAsyncTaskGetMetersValues();

        }
        else
            DisplayFunctions.showToast(context,
                    getString(R.string.general_network_connection_problems));


        configureChart();


        return v;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_history, menu);
        bar_menu_item = menu;
        bar_menu_item.getItem(4).setChecked(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        item.setChecked(!item.isChecked());

        if(item.isChecked() && item.getItemId() == R.id.action_todos) {
            bar_menu_item.getItem(0).setChecked(false);
            bar_menu_item.getItem(1).setChecked(false);
            bar_menu_item.getItem(2).setChecked(false);
            bar_menu_item.getItem(3).setChecked(false);
        }else{
            bar_menu_item.getItem(4).setChecked(false);
        }

        startAsyncTaskSelectMeter();

        //return super.onOptionsItemSelected(item);
        return true;
    }

    private  void startAsyncTaskSelectMeter(){
        Log.i("FRAGMENT HISTORY", "AsyncTask Send Daily Result");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {
            private List items;


            protected void onPreExecute()
            {
                items = new ArrayList();
                mMeterTemp = new ArrayList<>();
                progressbar.setVisibility(v.VISIBLE);
            }

            protected ArrayList<MeterValue> doInBackground(String... params) {

                if(!bar_menu_item.getItem(4).isChecked())
                {
                    int listSize = mMeterValuesList.size();

                    for (int k=0;k<listSize;k++) {
                        for (int l=0;l<4;l++) {
                            if (bar_menu_item.getItem(l).isChecked() && (getClassName(bar_menu_item.getItem(l).getTitle().toString()).compareTo(mMeterValuesList.get(k).getClass().getSimpleName()) == 0)) {
                                mMeterTemp.add(mMeterValuesList.get(k));
                            }
                        }
                    }

                }else{
                    mMeterTemp=mMeterValuesList;
                }

                Collections.reverse(mMeterTemp);
                return null;
            }

            protected void onPostExecute(ArrayList<MeterValue> result) {
                // Return true if the fragment is currently added to its activity. Con esto comprobamos que si le da atras no pete
                    getResources().getString(R.string.app_name);
                    if (mMeterTemp != null) {
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                        String strDate;


                        if (mMeterTemp.size() > 0) {
                            Iterator<MeterValue> it = mMeterTemp
                                    .iterator();

                            if(!items.isEmpty()){
                                items.clear();
                            }


                            while (it.hasNext()) {
                                MeterValue meterValue = it.next();

                                strDate = format.format(meterValue.getDateRead());
                                //Meter dato en lista
                                switch (meterValue.getClass().getSimpleName()){
                                    case "WeightValue":
                                        items.add(new CardViewModel(getString(R.string.weight), meterValue.getValue() , R.drawable.nonin_3230, strDate, "kg"));
                                        break;
                                    case "HeartRateValue":
                                        items.add(new CardViewModel(getString(R.string.hearthRate), meterValue.getValue(), R.drawable.nonin_3230, strDate, "p.p.m."));
                                        break;
                                    case "OxygenSaturationValue":
                                        items.add(new CardViewModel(getString(R.string.oxygenSaturation), meterValue.getValue(), R.drawable.nonin_3230, strDate, "%"));
                                        break;
                                    case "BloodPressureValue":
                                        String[] separated = meterValue.getValue().split("@");
                                        items.add(new CardViewModel(getString(R.string.bloodPressure), separated[0] + "/" + separated[1], R.drawable.nonin_3230, strDate, "mmHg"));
                                        break;

                                }


                                progressbar.setVisibility(View.GONE);
                            }
                            adapter = new CardViewHistoryAdapter(getActivity(),items);
                            recyclerView.setAdapter(adapter);
                            addEntry();
                            progressbar.setVisibility(View.GONE);
                        } else {
                            DisplayFunctions
                                    .showToast(
                                            context,
                                            getString(R.string.show_meter_values_empty_results));

                            progressbar.setVisibility(v.GONE);
                            chart.clear();
                            items.clear();
                            text_no_results.setVisibility(v.VISIBLE);
                            adapter = new CardViewHistoryAdapter(getActivity(),items);
                            recyclerView.setAdapter(adapter);

                        }
                    } else {
                        DisplayFunctions.showToast(context,
                                getString(R.string.show_meter_values_error));
                        progressbar.setVisibility(View.GONE);
                    }
                }

        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }

    private void startAsyncTaskGetMetersValues() {
        Log.i("FRAGMENT HISTORY", "AsyncTask Send Daily Result");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {
            private List items;
            private Date valorfechaIni;
            private Date valorfechaFin;

            protected void onPreExecute() {

                valorfechaIni = new Date(Long.MIN_VALUE);
                valorfechaFin = new Date(Long.MIN_VALUE);

                progressbar.setVisibility(View.VISIBLE);
                items = new ArrayList();
                recyclerView.setHasFixedSize(true);
                // Usar un administrador para LinearLayout
                lmanager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(lmanager);
                mMeterTemp = new ArrayList<>();

            }

            protected ArrayList<MeterValue> doInBackground(String... params) {
                try {
                    ArrayList<MeterValue> meterValues = null;

                    if ((meterValues == null || meterValues.size() == 0)
                            && (ConnectivityFunctions.networkAvailable(context)))
                        meterValues = _meterValuesController
                                .GetMeterValuesFromServer(context,new MeterValueComplete(null, new HeartRateValue(),
                                        null),valorfechaIni , valorfechaFin);
                    return meterValues;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(ArrayList<MeterValue> result) {
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                String strDate;
                // Return true if the fragment is currently added to its activity. Con esto comprobamos que si le da atras no pete
                if(isAdded()){
                    getResources().getString(R.string.app_name);
                    if (result != null) {
                        mMeterValuesList = result;
                        if (mMeterValuesList.size() > 0) {
                            Iterator<MeterValue> it = mMeterValuesList
                                    .iterator();
                            if(!items.isEmpty()){
                                items.clear();
                            }

                            while (it.hasNext()) {
                                MeterValue meterValue = it.next();

                                strDate = format.format(meterValue.getDateRead());
                                //Meter dato en lista
                                switch (meterValue.getClass().getSimpleName()){
                                    case "WeightValue":
                                        items.add(new CardViewModel(getString(R.string.weight), meterValue.getValue() , R.drawable.nonin_3230, strDate, "kg"));
                                        break;
                                    case "HeartRateValue":
                                        String valueHeart =  meterValue.getValue();
                                        if(valueHeart.equals("E511") || valueHeart.equals("E1")){
                                            if(valueHeart.equals("E511"))
                                                items.add(new CardViewModel(getString(R.string.hearthRate), "", R.drawable.nonin_3230, strDate, "Medida errónea"));
                                            else
                                                items.add(new CardViewModel(getString(R.string.hearthRate), "", R.drawable.nonin_3230, strDate, "Pulso irregular"));
                                        }else{
                                            items.add(new CardViewModel(getString(R.string.hearthRate), meterValue.getValue(), R.drawable.nonin_3230, strDate, "p.p.m."));
                                        }
                                        break;
                                    case "OxygenSaturationValue":
                                        String valueSaturation =  meterValue.getValue();
                                        if(valueSaturation.equals("E127")){
                                            valueSaturation = "";
                                            items.add(new CardViewModel(getString(R.string.oxygenSaturation), valueSaturation, R.drawable.nonin_3230, strDate, "Medida errónea"));
                                        }else{
                                            items.add(new CardViewModel(getString(R.string.oxygenSaturation), meterValue.getValue(), R.drawable.nonin_3230, strDate, "%"));
                                        }
                                        break;
                                    case "BloodPressureValue":
                                        String[] separated = meterValue.getValue().split("@");
                                        items.add(new CardViewModel(getString(R.string.bloodPressure), separated[0] + "/" + separated[1], R.drawable.nonin_3230, strDate, "mmHg"));
                                        break;

                                }


                                progressbar.setVisibility(View.GONE);
                            }
                            adapter = new CardViewHistoryAdapter(getActivity(),items);
                            recyclerView.setAdapter(adapter);
                            mMeterTemp = mMeterValuesList;
                            addEntry();
                            progressbar.setVisibility(View.GONE);
                            // Parar la animación del indicador
                            refreshLayout.setRefreshing(false);
                        } else {
                            DisplayFunctions
                                    .showToast(
                                            context,
                                            getString(R.string.show_meter_values_empty_results));
                            progressbar.setVisibility(View.GONE);
                        }
                    } else {
                        DisplayFunctions.showToast(context,
                                getString(R.string.show_meter_values_error));
                        progressbar.setVisibility(View.GONE); // Parar la animación del indicador
                        refreshLayout.setRefreshing(false);

                    }
                }

            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }

    // TODO: Rename method, update argument and hook method into UI event
   public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void configureChart()
    {

            chart.setDescription("");
            chart.setNoDataTextDescription("No hay datos para mostrar");

            //activamos highlighting
            chart.setHighlightPerDragEnabled(false);

            //Activamos gestos
            chart.setTouchEnabled(true);

            //Activamos drag y scaling
            chart.setDragEnabled(true);
            chart.setScaleEnabled(true);
            chart.setDrawGridBackground(false);

            chart.setPinchZoom(true);

            chart.setBackgroundColor(Color.WHITE);

            LineData data = new LineData();
            data.setValueTextColor(Color.BLACK);

            chart.setData(data);

            Legend l = chart.getLegend();

            l.setForm(Legend.LegendForm.LINE);
            l.setTextColor(Color.BLACK);

            XAxis xl = chart.getXAxis();
            xl.setTextColor(getResources().getColor(R.color.colorPrimary));
            xl.setDrawAxisLine(true);
            xl.setAvoidFirstLastClipping(false);
            xl.setPosition(XAxis.XAxisPosition.BOTTOM);
            xl.setLabelsToSkip(0);
            xl.setLabelRotationAngle(-90);

            YAxis yl = chart.getAxisLeft();
            yl.setTextColor(getResources().getColor(R.color.colorPrimary));

            YAxis yl2 = chart.getAxisRight();
            yl2.setEnabled(false);
    }


    private void addEntry()
    {
        Collections.reverse(mMeterTemp);
        ArrayList<String> fecha = new ArrayList<String>();
        ArrayList<Entry> sys = new ArrayList<Entry>();
        ArrayList<Entry> dia = new ArrayList<Entry>();
        ArrayList<Entry> peso = new ArrayList<Entry>();
        ArrayList<Entry> pulso = new ArrayList<Entry>();
        ArrayList<Entry> saturacion = new ArrayList<Entry>();

        if(mMeterTemp != null)
        {
            int j = 0;
            for (int i = 0; i < mMeterTemp.size(); i++) {

               /* if (mMeterTemp.get(i).isValidate()) {

                }*/
               /* if (!mMeterTemp.get(i).getValue().equals("E127") && mMeterTemp.get(i).getClass().getSimpleName().equals("OxygenSaturationValue") ||
                    (!mMeterTemp.get(i).getValue().equals("E1") ||!mMeterTemp.get(i).getValue().equals("E511")) && mMeterTemp.get(i).getClass().getSimpleName().equals("HeartRateValue") ||
                     mMeterTemp.get(i).getClass().getSimpleName().equals("WeightValue") ||
                     mMeterTemp.get(i).getClass().getSimpleName().equals("BloodPressureValue")) {*/

                if (mMeterTemp.get(i).isValidate()){

                    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                    fecha.add(format.format(mMeterTemp.get(i).getDateRead()));

                    switch (mMeterTemp.get(i).getClass().getSimpleName()){
                        case "WeightValue":
                            peso.add(new Entry(Float.valueOf(mMeterTemp.get(i).getValue().replace(",", ".")), j));
                            break;
                        case "HeartRateValue":
                            pulso.add(new Entry(Float.valueOf(mMeterTemp.get(i).getValue().replace(",", ".")), j));
                            break;
                        case "OxygenSaturationValue":
                            saturacion.add(new Entry(Float.valueOf(mMeterTemp.get(i).getValue().replace(",", ".")), j));
                            break;
                        case "BloodPressureValue":
                            String[] separated = mMeterTemp.get(i).getValue().split("@"); //Aqui divido el String por @
                            sys.add(new Entry(Integer.valueOf(separated[0]), j));
                            if(!separated[1].equals("-"))dia.add(new Entry(Integer.valueOf(separated[1]), j));
                            break;

                    }
                    j++;
                }
            }

            LineDataSet setSys;
            LineDataSet setDia;
            LineDataSet setSat;
            LineDataSet setPul;
            LineDataSet setPes;

            setSys = new LineDataSet(sys, "Sist.");
            setSys.setDrawCubic(false);
            setSys.setCubicIntensity(0.2f);
            setSys.setAxisDependency(YAxis.AxisDependency.LEFT);
            setSys.setColor(ColorTemplate.getHoloBlue());
            setSys.setCircleColor(Color.BLACK);
            setSys.setLineWidth(2f);
            setSys.setFillAlpha(65);
            setSys.setFillColor(ColorTemplate.getHoloBlue());
            setSys.setHighLightColor(Color.rgb(244, 177, 177));
            setSys.setValueTextColor(Color.BLACK);
            setSys.setValueTextSize(10f);

            setDia = new LineDataSet(dia, "Dias.");
            setDia.setDrawCubic(false);
            setDia.setCubicIntensity(0.2f);
            setDia.setAxisDependency(YAxis.AxisDependency.LEFT);
            setDia.setColor(Color.RED);
            setDia.setCircleColor(Color.BLACK);
            setDia.setLineWidth(2f);
            setDia.setFillAlpha(65);
            setDia.setFillColor(ColorTemplate.getHoloBlue());
            setDia.setHighLightColor(Color.rgb(244, 177, 177));
            setDia.setValueTextColor(Color.BLACK);
            setDia.setValueTextSize(10f);

            setPul = new LineDataSet(pulso, "Pulso");
            setPul.setDrawCubic(false);
            setPul.setCubicIntensity(0.2f);
            setPul.setAxisDependency(YAxis.AxisDependency.LEFT);
            setPul.setColor(Color.GREEN);
            setPul.setCircleColor(Color.BLACK);
            setPul.setLineWidth(2f);
            setPul.setFillAlpha(65);
            setPul.setFillColor(ColorTemplate.getHoloBlue());
            setPul.setHighLightColor(Color.rgb(244, 177, 177));
            setPul.setValueTextColor(Color.BLACK);
            setPul.setValueTextSize(10f);

            setPes = new LineDataSet(peso, "Peso");
            setPes.setDrawCubic(false);
            setPes.setCubicIntensity(0.2f);
            setPes.setAxisDependency(YAxis.AxisDependency.LEFT);
            setPes.setColor(Color.MAGENTA);
            setPes.setCircleColor(Color.BLACK);
            setPes.setLineWidth(2f);
            setPes.setFillAlpha(65);
            setPes.setFillColor(ColorTemplate.getHoloBlue());
            setPes.setHighLightColor(Color.rgb(244, 177, 177));
            setPes.setValueTextColor(Color.BLACK);
            setPes.setValueTextSize(10f);

            setSat = new LineDataSet(saturacion, "Saturación");
            setSat.setDrawCubic(false);
            setSat.setCubicIntensity(0.2f);
            setSat.setAxisDependency(YAxis.AxisDependency.LEFT);
            setSat.setColor(Color.BLACK);
            setSat.setCircleColor(Color.BLACK);
            setSat.setLineWidth(2f);
            setSat.setFillAlpha(65);
            setSat.setFillColor(ColorTemplate.getHoloBlue());
            setSat.setHighLightColor(Color.rgb(244, 177, 177));
            setSat.setValueTextColor(Color.BLACK);
            setSat.setValueTextSize(10f);

            ArrayList<ILineDataSet> dataSets  = new ArrayList<ILineDataSet>();

            dataSets.add(setDia);
            dataSets.add(setSys);
            dataSets.add(setPes);
            dataSets.add(setPul);
            dataSets.add(setSat);

            // create a data object with the datasets
            LineData data = new LineData(fecha, dataSets);
            data.setValueTextColor(Color.BLACK);
            data.setValueTextSize(9f);

            // set data
            chart.setData(data);
            chart.notifyDataSetChanged();
            chart.invalidate();


        }
    }

    private String getClassName (String barItem)
    {
        String className= "";

        if(barItem.compareTo("Peso") == 0)
            className = "WeightValue";
        else if(barItem.compareTo("Pulso") == 0)
            className = "HeartRateValue";
        else if(barItem.compareTo("Saturación") == 0)
            className = "OxygenSaturationValue";
        else if(barItem.compareTo("Tensión") == 0)
            className = "BloodPressureValue";

        return className;

    }


}
