package es.vivelibre.vsalud.Activities;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import java.util.ArrayList;
import java.util.List;

import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.Utils.CardViewModel;
import es.vivelibre.vsalud.Adapters.CardViewManualValuesAdapter;
import es.vivelibre.vsalud.Controllers.ManualValuesController;
import es.vivelibre.vsalud.R;

import static com.google.android.gms.internal.zzs.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentManualValues.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentManualValues extends Fragment {

    private OnFragmentInteractionListener mListener;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager lmanager;
    private CardViewManualValuesAdapter adapter;
    private ProgressBar progressbar;
    private static ManualValuesController manualValuesController = ManualValuesController.getInstance();

    public FragmentManualValues() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Manual");
        // Inflate the layout for this fragment
        final View rootView=  inflater.inflate(R.layout.fragment_manual_values, container, false);

        progressbar = (ProgressBar) rootView.findViewById(R.id.manualValuesProgressBar);
        progressbar.setVisibility(View.GONE);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.manualValuesReciclador);

        new Asintask().execute();

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class Asintask extends AsyncTask<String , Integer ,MonitorParameters> {

        private List items;

        protected void onPreExecute() {
            Log.i("FRAGMENT MANUALVALUES", "AsyncTask Get Monitors Parameters");
            progressbar.setVisibility(View.VISIBLE);
            items = new ArrayList();
            // Obtener el Recycler
            //recyclerView = (RecyclerView) findViewById(R.id.reciclador);
            recyclerView.setHasFixedSize(true);

            // Usar un administrador para LinearLayout
            lmanager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(lmanager);

            adapter = new CardViewManualValuesAdapter(getActivity(),items);
            recyclerView.setAdapter(adapter);


        }
        @Override
        protected MonitorParameters doInBackground(String... params) {
            try {
                MonitorParameters monitorParameters = manualValuesController.GetMonitorParameters(getActivity());
                if (monitorParameters != null){
                    if (monitorParameters.bloodOxygenSaturation.isActive()){
                        if (monitorParameters.getBloodOxygenSaturationType().equals("PulseOximeter")) {
                            items.add(new CardViewModel(getResources().getString(R.string.oxygenSaturation), monitorParameters.getBloodOxygenSaturationLastValue().getValue(), R.drawable.nonin_3230, monitorParameters.getBloodOxygenSaturationLastValue().getDateRead().toString(), "%"));
                        }else {
                            items.add(new CardViewModel(getResources().getString(R.string.oxygenSaturation),monitorParameters.getBloodOxygenSaturationLastValue().getValue(), R.drawable.chino, monitorParameters.getBloodOxygenSaturationLastValue().getDateRead().toString(), "%"));
                        }
                    }
                    if (monitorParameters.bloodPressure.isActive()){
                        String[] separated = monitorParameters.getBloodPressureLastValue().getValue().split("@");
                        if (monitorParameters.getBloodPressureType().equals("BloodPressureMonitor")) {
                            items.add(new CardViewModel(getResources().getString(R.string.bloodPressure), separated[0] + "/" + separated[1] , R.drawable.tensiometro, monitorParameters.getBloodPressureLastValue().getDateRead().toString(), "mmHg"));
                        }else {
                            items.add(new CardViewModel(getResources().getString(R.string.bloodPressure), separated[0] + "/" + separated[1], R.drawable.chino, monitorParameters.getBloodPressureLastValue().getDateRead().toString(), "mmHg"));
                        }
                    }
                    if(monitorParameters.heartRate.isActive())
                    {
                        if (monitorParameters.getHeartRateType().equals("HealthMonitor")){
                            items.add(new CardViewModel(getResources().getString(R.string.hearthRate),monitorParameters.getHeartRateLastValue().getValue() , R.drawable.chino, monitorParameters.getHeartRateLastValue().getDateRead().toString(),"p.p.m."));
                        }else if (monitorParameters.getHeartRateType().equals("BloodPressureMonitor")){
                            items.add(new CardViewModel(getResources().getString(R.string.hearthRate), monitorParameters.getHeartRateLastValue().getValue() , R.drawable.tensiometro, monitorParameters.getHeartRateLastValue().getDateRead().toString(),"p.p.m."));
                        }else{
                            items.add(new CardViewModel(getResources().getString(R.string.hearthRate), monitorParameters.getHeartRateLastValue().getValue(), R.drawable.nonin_3230, monitorParameters.getHeartRateLastValue().getDateRead().toString(),"p.p.m."));
                        }

                    }
                    if(monitorParameters.weight.isActive()){
                        items.add(new CardViewModel(getResources().getString(R.string.weight), monitorParameters.getWeightLastValue().getValue(), R.drawable.bascula,  monitorParameters.getWeightLastValue().getDateRead().toString(),"kg"));
                    }
                    return monitorParameters;
                }
            }catch (Exception e){
                FirebaseCrash.logcat(Log.ERROR, TAG, "FragmentManualValues AsyncTask Get Monitors Parameters");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(MonitorParameters result){
            if (result != null) {
                // Crear un nuevo adaptador
                //finish();
                adapter = new CardViewManualValuesAdapter(getActivity(),items);
                progressbar.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
            }
        }
    }
}
