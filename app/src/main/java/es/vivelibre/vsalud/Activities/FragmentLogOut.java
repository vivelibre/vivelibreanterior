package es.vivelibre.vsalud.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import es.vivelibre.vsalud.R;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by maca- on 16/05/2016.
 */
public class FragmentLogOut extends Fragment {

    private OnFragmentInteractionListener mListener;

    public FragmentLogOut() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Logout");
        // Inflate the layout for this fragment
        final View rootView  = inflater.inflate(R.layout.fragment_logout, container, false);

        ShowConfirmDisconnectDialog();
        return rootView;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /*
	 * DISCONNECT DIALOG
	 */

    public void ShowConfirmDisconnectDialog() {
        try {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle( getResources().getString(
                    R.string.alert_dialog_confirmar_desconexion_title));
            dialogo1.setMessage(getResources().getString(
                    R.string.alert_dialog_confirmar_desconexion_summary));
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton(
                    getResources().getString(
                                    R.string.alert_dialog_confirmar_desconexion_aceptar),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            Intent homeIntent = new Intent(getActivity(), Disconnect.class);
                            getActivity().startActivity(homeIntent);
                            getActivity().finish();
                        }
                    });
            dialogo1.setNegativeButton(
                    getResources().getString(
                                    R.string.alert_dialog_confirmar_desconexion_cancelar),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            Intent homeIntent = new Intent(getActivity(), HomeActivity.class);
                            getActivity().startActivity(homeIntent);
                            getActivity().finish();
                        }
                    });
            dialogo1.show();
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "FragmentLogOut ShowConfirmDisconnectDialog");
            FirebaseCrash.report(e);
            Log.e("MainMenuAdapter", "Error mostrando ShowConnectionError: "
                    + e);
        }
    }
}
