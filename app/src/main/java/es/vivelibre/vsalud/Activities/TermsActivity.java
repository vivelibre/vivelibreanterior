package es.vivelibre.vsalud.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import es.vivelibre.vsalud.R;

public class TermsActivity extends AppCompatActivity {

    private final String htmlText="<!DOCTYPE html><html><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'></head><body><p>La App de ViveLibre salud ha sido desarrollada" +
            "por ViveLibre Autonomía Personal S.L y ATAM para el Apoyo Familiar.</p><p>La versión actual de la App está disponible como parte del Estudio que está desarrollando ATAM" +
            " para la monitorización remota de Salud. La participación en dicha prueba Piloto implica la aceptación y firma de las siguientes condiciones recogidas en el “Consentimiento Informado”:" +
            "</p><span><b>El Participante Consiente:</b></span><p>I. Participar en la prueba piloto del Servicio ViveLibre Salud (en adelante, el PILOTO). Dicha prueba, que tiene carácter gratuito y voluntario "+
            "para el participante, tendrá una duración inicial prevista de  tres (3) meses, comenzando el día de la entrega del material y la firma del presente documento. Dicha duración podrá ser ampliada en "+
            "función de las necesidades y el desarrollo del PILOTO. El participante podrá abandonar el PILOTO en cualquier momento poniéndolo en conocimiento de ATAM, y comprometiéndose a devolver "+
            "todo el material que pudiera haber recibido, sin coste alguno.</p><p>II. Que ATAM, domiciliada en la calle Orotava 4, 28660 Boadilla del Monte, y con CIF G28456283, obtenga, use, gestione y "+
            "explote la información y datos personales y de salud necesarios para el correcto desarrollo del PILOTO.</p><span>Estos datos:<br></span><br><ol><li>1. Podrán ser compartidos con los familiares o personas de "+
            "contacto que participen en el PILOTO, no pudiendo compartirse con fines comerciales con ningún tercero no relacionado con la prestación, operación o explotación del Servicio ViveLibre Salud.</li>"+
            "<br><li>2. Podrán ser, entre otros, datos relativos a la persona, necesidades médicas, historia clínica, tratamientos, parámetros de la salud, citas y agenda, etc., así como información de su experiencia "+
            "en la participación de la prueba piloto.</li><br><li>3. ATAM se reserva el derecho de poder utilizar estos datos para la realización de estudios estadísticos y de mejoras de los proyectos desarrollados "+
            "por ATAM. Dichos datos serán en todo caso utilizados debidamente anonimizados y encriptados y cumpliendo siempre la legislación vigente y la normativa de protección de datos.</li></ol><br><br><span>"+
            "<b>PROTECCIÓN DE DATOS</b></span><p>Los datos personales recogidos en el presente documento, así como aquellos que se vayan recopilando durante el desarrollo del PILOTO, serán incorporados y "+
            "tratados en un fichero cuyo responsable es ATAM, con la finalidad de gestionar la información durante la prestación del servicio y con fines científicos y de investigación relacionados con los "+
            "proyectos desarrollados por ATAM. Los interesados podrán ejercer los derechos de acceso, rectificación, cancelación y oposición mediante comunicación escrita dirigida a ATAM, calle Orotava 4, 28660 "+
            "Boadilla del Monte. Para garantizar la protección de datos se estará a lo establecido en las siguientes leyes que regulan estos aspectos:<p><ul><li>- Ley Orgánica 15/1999, de 13 diciembre, " +
            "de Protección de Datos de Carácter Personal.</li><br><li>- Real Decreto 1720/2007 de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la Ley Orgánica 15/1999 de 13 de diciembre " +
            "de Protección de Datos de Carácter Personal (RLOPD).</li><br><li>- Ley 41/2002, de 14 de noviembre, básica reguladora de la autonomía del paciente y de derechos y obligaciones en materia de "+
            "información y documentación clínica.</li><br><li>- Ley 14/2007, de 3 de julio, de Investigación Biomédicas.</li></ul><br><br><span><b>DISPOSITIVOS, FUNCIONAMIENTO Y PROBLEMAS DE FUNCIONAMIENTO </b></span><p>Como participante en la prueba piloto, se le hace entrega "+
            "de los dispositivos que se indican a continuación:</p><ul><l1>1 Báscula Modelo, A&D UC-351 PBT-Ci.</li><br><li>1 Tensiómetro Modelo, A&D UA-767 PBT-Ci.</li><br><li>1 Pulsioxímetro Modelo, "+
            "Nonin 3230 Bluetooth® Smart Pulse Oximeter.</li></ul><p>Así mismo se procederá a la instalación de la Aplicación móvil en su propio dispositivo Smartphone para realizar el envío de las "+
            "medidas de salud de estos dispositivos.</p><p>Por otro lado, como parte de su participación en el PILOTO, usted será dado de alta en el servicio ViveLibre sin coste alguno durante la duración del PILOTO. "+
            "Para este servicio será necesario así mismo instalar en su Smartphone la aplicación de ViveLibre.</p>" +
            "<p>El equipo del PILOTO le instruirá sobre la utilización de los dispositivos y de las Apps. Usted sólo tendrá que tomarse las medidas según las indicaciones y pautas establecidas "+
            "por el equipo médico.</p><p>Los dispositivos se entregan en perfectas condiciones de uso. Estos dispositivos de salud  pueden depender del uso de pilas para su funcionamiento. En el caso en el que "+
            "detectara algún fallo de funcionamiento en los dispositivos o tenga alguna duda, le rogamos pulse el botón verde de ViveLibre para intentar resolver la incidencia.</p>" +
            "<span><b>SERVICIO DE ATENCIÓN TELEFÓNICA A LA PRUEBA PILOTO</b></span><p>Durante el desarrollo del PILOTO, ATAM pone a disposición de todos los participantes el teléfono de "+
            "contacto  917098074, en horario de oficina (de 9:00 a 17:00 de lunes a viernes) y la dirección de email piloto@atam.es, para atender y resolver las dudas que puedan generarse y "+
            "para facilitar cualquier gestión o información sobre el PILOTO.</p><span><b>RESPONSABILIDAD</b></span>"+
            "<p>El participante reconoce y acepta que se trata de una prueba PILOTO y, por tanto, ATAM no asume responsabilidad alguna en cuanto al correcto funcionamiento del Servicio "+
            "y cualesquiera consecuencias se pudieran derivar de los defectos, disfunciones o errores que pudieran producirse durante el desarrollo del mismo.</p>"+
            "<p>Concretamente, el participante en la prueba piloto asume que ATAM se encuentra eximida de responsabilidad ante cualquier alarma o emergencia de salud producida durante la misma, "+
            "no estando obligada a prestar servicio en tiempo real, por lo que el participante en la prueba piloto renuncia a realizar cualquier acción legal contra la misma.</p>" +
            "<p>El propósito de la presente prueba piloto es el testeo de un sistema para la monitorización de variables de la salud y  del estado de salud general de los participantes en la misma, "+
            "no siendo su objetivo en ningún momento el diagnóstico de enfermedades o el establecimiento de tratamientos o pautas terapéuticas, los cuales serán llevados a cabo por el propio participante "+
            "a través de los circuitos que normalmente use en la gestión de su salud (seguridad social, seguro sanitario, etc.).</p>" +
            "<p>Los resultados obtenidos en la presente prueba piloto tendrán validez únicamente a efectos de investigación y testado de las soluciones de monitorización remota de su salud, " +
            "no estando validados aún desde el punto de vista clínico y careciendo por tanto de validez sanitaria.</p> <span><b>COMPROMISO</b></span> " +
            "<p>El participante reconoce que todos los dispositivos así como la documentación, programas y cualquier otro tipo de material entregado son propiedad de ATAM.</p>" +
            "<p>En ese sentido, el participante procurará dar el mejor uso de todo el material recibido. En caso de que se produzca algún incidente, avería, robo, " +
            "pérdida o cualquier otra situación que implique no disponer del material deberá ser comunicado inmediatamente a ATAM a través de los medios suministrados para que actúen según determinen.</p> " +
            "<p>El participante se compromete a no usar los dispositivos entregados, para otros fines no relacionados con la prueba en la que participa.</p>" +
            "<p>Así mismo, el participante se compromete a no desvelar ningún tipo de información a ninguna empresa o persona no participante en la prueba piloto durante el desarrollo de la misma y en un plazo posterior a un año, " +
            "reconociendo la propiedad intelectual de ATAM.</p></body></html>";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        TextView htmlView = (TextView)findViewById(R.id.html_text);
        htmlView.setText(Html.fromHtml(htmlText, null, null));
        htmlView.setMovementMethod(new ScrollingMovementMethod());
    }
}
