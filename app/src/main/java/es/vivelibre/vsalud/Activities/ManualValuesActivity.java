package es.vivelibre.vsalud.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.mikephil.charting.utils.ColorTemplate;
import com.uxcam.UXCam;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import at.grabner.circleprogress.UnitPosition;
import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.models.Meter.WeightValue;
import es.vivelibre.vsalud.Controllers.MeterValueController;
import es.vivelibre.vsalud.Functions.ConnectivityFunctions;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.R;

/**
 * Created by mfernandez on 24/05/2016.
 */
public class ManualValuesActivity extends AppCompatActivity implements View.OnClickListener{

    private MeterValueController _meterValueController = MeterValueController
            .getInstance();

    private Context context;
    private Intent intent;
    private ArrayList<MeterValue> meterValuesToResult;
    private EditText Num;
    private EditText NumExtra;
    private TextView textType;
    private TextView textViewValue;

    private CircleProgressView mCircleView;//Circulo porcentaje
    private CircleProgressView mCircleViewExtra;//Circulo porcentaje extra Tension

    private Button saveValue;

    private ArrayList<MeterValue> valuesToSend = new ArrayList<MeterValue>();
    private  String type;

    private MeterValue value;
    private FloatingActionButton botonIrHome;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Valores manual");

        intent = getIntent();

        setContentView(R.layout.manual_values);

        context = getApplicationContext();

        saveValue = (Button)findViewById(R.id.buttonSaveManualValue);
        botonIrHome = (FloatingActionButton)findViewById(R.id.resultClose);

        saveValue.setOnClickListener(this);
        botonIrHome.setOnClickListener(this);

        mCircleView = (CircleProgressView)findViewById(R.id.circleView);
        Num = (EditText)findViewById(R.id.textView1);
        textType = (TextView)findViewById(R.id.textViewType);
        textViewValue = (TextView)findViewById(R.id.textViewValue);
        botonIrHome = (FloatingActionButton)findViewById(R.id.resultClose);

        if (intent.getStringExtra("tipo").equals(context.getString(R.string.bloodPressure))) {
            //Si es medida de tension tenemos que abrir un layout diferente
            // aqui definimos de nuevo todos los elementos

            setContentView(R.layout.manual_values_tension);
            mCircleViewExtra = (CircleProgressView)findViewById(R.id.circleViewExtra);
            mCircleView = (CircleProgressView)findViewById(R.id.circleView);
            Num = (EditText)findViewById(R.id.textView1);
            NumExtra = (EditText)findViewById(R.id.textView1Extra);
            textType = (TextView)findViewById(R.id.textViewType);
            saveValue = (Button)findViewById(R.id.buttonSaveManualValue);
            botonIrHome = (FloatingActionButton)findViewById(R.id.resultClose);

            botonIrHome.setOnClickListener(this);
            saveValue.setOnClickListener(this);
            mCircleView.setVisibility(View.GONE);
            mCircleViewExtra.setVisibility(View.VISIBLE);

            type = context.getString(R.string.bloodPressure);
            textType.setText(type);

            mCircleViewExtra.setRimColor(Color.RED);
            mCircleViewExtra.setUnit("mmHg");
            mCircleViewExtra.setTextMode(TextMode.VALUE);
            mCircleViewExtra.setUnitPosition(UnitPosition.BOTTOM);
            mCircleViewExtra.setUnitVisible(true);
            mCircleViewExtra.setDecimalFormat(new DecimalFormat("0.0"));

            mCircleView.setUnit("mmHg");
            mCircleView.setRimColor(ColorTemplate.getHoloBlue());

            mCircleViewExtra.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
                @Override
                public void onProgressChanged(float value) {
                    Log.d("TAG", "Progress Changed: " + value);
                    Log.d("TAG", "Progress Changed: " + (int)value);

                    //mSeekBar.setProgress(Math.round(value)*10);
                    //String seekbarValue = String.valueOf(value);
                    NumExtra.setText(String.format("%.1f",value).replace(",","."));
                }
            });
            NumExtra.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (intent.getStringExtra("tipo").equals(context.getString(R.string.bloodPressure))){
                        mCircleView.setVisibility(View.GONE);
                        mCircleViewExtra.setVisibility(View.VISIBLE);
                    }
                }
            });
            NumExtra.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        float value = Float.parseFloat(String.valueOf(NumExtra.getText()));
                        mCircleViewExtra.setValueAnimated(value , 1500);
                        return true;
                    }
                    return false;
                }
            });

        }else {
            if (intent.getStringExtra("tipo").equals(context.getString(R.string.weight))) {
                type = context.getString(R.string.weight);
                textType.setText(type);
                textViewValue.setText("Peso");
                mCircleView.setUnit("kg");
                mCircleView.setRimColor(Color.GREEN);

            } else if (intent.getStringExtra("tipo").equals(context.getString(R.string.oxygenSaturation))) {
                type = context.getString(R.string.oxygenSaturation);
                textType.setText(type);
                textViewValue.setText("Saturación");
                mCircleView.setUnit("%");
                mCircleView.setRimColor(Color.BLACK);
            } else if (intent.getStringExtra("tipo").equals(context.getString(R.string.hearthRate))) {
                type = context.getString(R.string.hearthRate);
                textViewValue.setText("Pulso");
                textType.setText(type);
                mCircleView.setUnit("p.p.m.");
                mCircleView.setRimColor(Color.MAGENTA);
            }
        }

        Num.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (intent.getStringExtra("tipo").equals(context.getString(R.string.bloodPressure))){
                    mCircleView.setVisibility(View.VISIBLE);
                    mCircleViewExtra.setVisibility(View.GONE);
                }
            }
        });

        Num.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    float value = Float.parseFloat(String.valueOf(Num.getText()));

                    mCircleView.setValueAnimated(value , 1500);
                    return true;
                }
                return false;
            }
        });



        mCircleView.setTextMode(TextMode.VALUE);
        mCircleView.setUnitPosition(UnitPosition.BOTTOM);
        mCircleView.setUnitVisible(true);
        mCircleView.setDecimalFormat(new DecimalFormat("0.0"));


        mCircleView.setOnProgressChangedListener(new CircleProgressView.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(float value) {
                Log.d("TAG", "Progress Changed: " + value);
                Log.d("TAG", "Progress Changed: " + (int)value);

               //mSeekBar.setProgress(Math.round(value)*10);
                //String seekbarValue = String.valueOf(value);
                Num.setText(String.format("%.1f",value).replace(",","."));
            }
        });




    }

    @Override
    public void onClick(View v) {

       if(v == saveValue){
            if (ConnectivityFunctions.networkAvailable(context)) {
                if (!intent.getStringExtra("tipo").equals(context.getString(R.string.bloodPressure))){
                    if (Num.getText().toString().equals("") || Num.getText().toString().equals("0.0")){
                        DisplayFunctions.showToast(context,"Datos Incorrectos");
                    }else{
                        startAsyncTaskSendResults();
                    }
                }else {
                    if (Num.getText().toString().equals("") || Num.getText().toString().equals("0.0") || NumExtra.getText().toString().equals("")
                            || NumExtra.getText().toString().equals("0.0")) {
                        DisplayFunctions.showToast(context, "Datos Incorrectos");
                    } else {
                        startAsyncTaskSendResults();
                    }
                }
            } else {
                DisplayFunctions.showToast(context,"Error de Conexión");
            }
        }else if(v == botonIrHome){
           launchMainMenuManualActivity();
        }

    }


    private void launchMainMenuManualActivity() {
        Intent homeManualIntent = new Intent(this, HomeActivity.class);
        startActivity(homeManualIntent);
        finish();
    }

    private void startAsyncTaskSendResults() {
        Log.i("MANUAL ACTIVIY VALUES", "AsyncTask Get Send Manual Values");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {

            protected void onPreExecute() {
                DisplayFunctions.showToast(context,"Enviando Datos");

                if (type.equals( context.getString(R.string.weight))) {
                    WeightValue weightValue;
                    weightValue= new WeightValue();
                    weightValue.setValue(Num.getText().toString());
                    weightValue.setDateRead(new Date());
                    weightValue.setMeterId("00000000-0000-0000-0000-000000000000");
                    valuesToSend.add(weightValue);

                } else if (type.equals(context.getString(R.string.bloodPressure))) {
                    BloodPressureValue bloodPressureValue;
                    bloodPressureValue= new BloodPressureValue();
                    int sys  =(int)Float.parseFloat(Num.getText().toString());
                    int dia  =(int)Float.parseFloat(NumExtra.getText().toString());
                    bloodPressureValue.setValue(String.valueOf(sys)+ "@" + String.valueOf(dia) + "@-");
                    bloodPressureValue.setDateRead(new Date());
                    bloodPressureValue.setMeterId("00000000-0000-0000-0000-000000000000");
                    valuesToSend.add(bloodPressureValue);

                } else if (type.equals(context.getString(R.string.oxygenSaturation))) {
                    OxygenSaturationValue oxigenSaturationValue;
                    oxigenSaturationValue= new OxygenSaturationValue();
                    int oxigen  =(int)Float.parseFloat(Num.getText().toString());
                    oxigenSaturationValue.setValue(String.valueOf(oxigen));
                    oxigenSaturationValue.setDateRead(new Date());
                    oxigenSaturationValue.setMeterId("00000000-0000-0000-0000-000000000000");
                    valuesToSend.add(oxigenSaturationValue);
                }
                else if (type.equals(context.getString(R.string.hearthRate))) {
                    HeartRateValue heartRateValue;
                    heartRateValue= new HeartRateValue();
                    int pulso =(int)Float.parseFloat(Num.getText().toString());
                    heartRateValue.setValue(String.valueOf(pulso));
                    heartRateValue.setDateRead(new Date());
                    heartRateValue.setMeterId("00000000-0000-0000-0000-000000000000");
                    valuesToSend.add(heartRateValue);

                }

            }

            protected ArrayList<MeterValue> doInBackground(String... params) {
                value = valuesToSend.get(valuesToSend.size()-1);
                return _meterValueController.AddNewListMeterValues(context,valuesToSend);
            }

            protected void onPostExecute(ArrayList<MeterValue> meterValues) {
                meterValuesToResult = meterValues;

                if (meterValues == null
                        || (meterValues != null && meterValues.size() > 0)) {
                    mHandler.obtainMessage(
                            8, 1, 0)
                            .sendToTarget();
                  valuesToSend = new ArrayList<MeterValue>();
                } else {
                    mHandler.obtainMessage(
                            8, 0, 0)
                            .sendToTarget();
                    valuesToSend = new ArrayList<MeterValue>();
                }
            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }


    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 8:
                    manageStateSendValuesComplete(msg.arg1 == 0);
                    break;
                case 11:
                    DisplayFunctions.showToast(context,"Error De Conexión");
                    break;
                case 10:
                    DisplayFunctions.showToast(context,"Error Envio");
                    break;
            }
        }
    };

    protected void manageStateSendValuesComplete(boolean success) {
        DisplayFunctions
                .showToast(
                        context,
                        (success) ? getString(R.string.send_meter_values_sending_values_success)
                                : getString(R.string.send_meter_values_sending_values_error));
        //llamamos al metodo que va a la pagina de resultados
        launchResultActivity(success);
    }
    private void launchResultActivity(boolean success){
        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra("success", success);
        if (type.equals(context.getString(R.string.weight))) {
            resultIntent.putExtra("value", value.getValue().toString() + " kg");
        } else if ((type.equals(context.getString(R.string.bloodPressure)))) {
            String[] separated = value.getValue().split("@");
            resultIntent.putExtra("value",   separated[0]+ ":" + separated[1] +  " mmHg");
        } else if ((type.equals( context.getString(R.string.oxygenSaturation)))) {
            resultIntent.putExtra("value", value.getValue().toString()+ " %");
        }
        else if ((type.equals(context.getString(R.string.hearthRate)))) {
            resultIntent.putExtra("value", value.getValue().toString()+ " p.p.m.");
        }
        startActivity(resultIntent);
        finish();
    }



}
