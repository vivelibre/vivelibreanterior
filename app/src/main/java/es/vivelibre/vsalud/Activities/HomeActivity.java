package es.vivelibre.vsalud.Activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.vsalud.Controllers.HomeController;
import es.vivelibre.vsalud.R;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        FragmentManualValues.OnFragmentInteractionListener, FragmentHome.OnFragmentInteractionListener,
        FragmentResults.OnFragmentInteractionListener, FragmentAbout.OnFragmentInteractionListener,
        FragmentHelp.OnFragmentInteractionListener, FragmentHistory.OnFragmentInteractionListener{

    private Context context;
    private String userName;
    private TextView nameText;
    private ImageView photoUser;
    private Bitmap myBitmap;
    public NavigationView navigationView;

    private static HomeController homeController = HomeController.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Home");
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = getApplicationContext();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        LinearLayout headerView = (LinearLayout) LayoutInflater.from(getApplicationContext()).inflate(R.layout.nav_header_main, null);
        nameText = (TextView)headerView.findViewById(R.id.nameUser);
        photoUser = (ImageView)headerView.findViewById(R.id.photoUser);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.addHeaderView(headerView);

        navigationView.setNavigationItemSelectedListener(this);


       if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1 || Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP ){
           //Esto lo usamos para ver si la version del movil es 21 o 22 y darle un margen en el top a la imagen del desplegable
            ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(headerView.getLayoutParams());
            marginParams.setMargins(0, 25, 0, 0);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(marginParams);
            headerView.setLayoutParams(layoutParams);
        }
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M){
            //Esto lo usamos para ver si la version del movil es 23 y darle un margen en el top a la imagen del desplegable
            ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(headerView.getLayoutParams());
            marginParams.setMargins(0, 25, 0, 0);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(marginParams);
            headerView.setLayoutParams(layoutParams);
        }

        startAsyncTaskTakeProfile();

        photoUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment =  new FragmentProfile();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_home, fragment)
                        .commit();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_home, new FragmentHome())
                .commit();


    }

    private void startAsyncTaskTakeProfile() {
        Log.i("HOME ACTIVITY", "AsyncTask Get Profile");
        AsyncTask<String, Float, Boolean> AsyncTaskLogin = new AsyncTask<String, Float, Boolean>() {

            protected void onPreExecute() {

            }

            protected Boolean doInBackground(String... params) {
                try {

                    Profile profile = homeController.GetProfile(context);
                    profile.setImageProfile(homeController.GetImageProfile(context));
                    if (!(homeController.GetImageProfile(context) == null))
                        myBitmap = homeController.getContactPhotoFromBase64(profile.getImageProfile().getData());
                    else
                        profile.setImageProfile(new ImageProfile());
                    userName = profile.getName()+ " " + profile.getFirst_name() + " " + profile.getLast_name();

                } catch (Exception e) {
                    FirebaseCrash.logcat(Log.ERROR,"AsyncTask Get Profile", "NPE caught");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
                return false;
            }

            protected void onPostExecute(Boolean result) {

                nameText.setText(userName);

                if (myBitmap != null) {
                    photoUser.setImageBitmap(myBitmap);
                }else
                    photoUser.setImageResource(R.drawable.ic_person_black);

            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskLogin
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskLogin.execute("");
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        boolean fragmentTransaction = false;
        Fragment fragment = null;


        if (id == R.id.homeOptionMenu)
        {
            fragment = new FragmentHome();
            fragmentTransaction = true;
       /* } else if ( id == R.id.manualOptionMenu) {
            fragment = new FragmentManualValues();
            fragmentTransaction = true;*/
        }else if (id == R.id.historyValuesOptionMenu){
            fragment = new FragmentHistory();
            fragmentTransaction = true;
        } else if ( id  == R.id.helpOptionMenu)
        {
            fragment = new FragmentHelp();
            fragmentTransaction = true;
        } else if (id == R.id.aboutOptionMenu)
        {
            fragment = new FragmentAbout();
            fragmentTransaction = true;

        }else if (id == R.id.profileMenu){
            fragment  = new FragmentProfile();
            fragmentTransaction = true;

        } else if ( id == R.id.logoutOption)
        {
            //Toast.makeText(this, "Falta implementación", Toast.LENGTH_SHORT).show();
            fragment = new FragmentLogOut();
            fragmentTransaction = true;
        }else if(id == R.id.closeApp)
        {
            //finish();
            System.exit(0);
        }


        if(fragmentTransaction)
        {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_home, fragment)
                    .commit();

            //item.setChecked(true);
            //getSupportActionBar().setTitle(item.getTitle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }





    @Override
    public void onBackPressed()
    {
       DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void switchToResultContent(String type)
    {

        Bundle args = new Bundle();
        args.putString("type", type);

        Fragment results = new FragmentResults();
        results.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_home, results)
                .commit();
    }

}
