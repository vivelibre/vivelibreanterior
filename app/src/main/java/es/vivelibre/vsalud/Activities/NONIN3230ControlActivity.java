package es.vivelibre.vsalud.Activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.crash.FirebaseCrash;
import com.uxcam.UXCam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.vsalud.Bluetooth.le.BluetoothLeService;
import es.vivelibre.vsalud.Controllers.MeterValueController;
import es.vivelibre.vsalud.Functions.ConnectivityFunctions;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.Bluetooth.nonin.N3230Utils;
import es.vivelibre.vsalud.R;

/**
 * Created by mfernandez on 11/05/2016.
 */
public class NONIN3230ControlActivity extends AppCompatActivity implements DialogInterface.OnClickListener, View.OnClickListener {

    private final static String TAG = NONIN3230ControlActivity.class
            .getSimpleName();

    private MeterValueController _meterValueController = MeterValueController
            .getInstance();

    private FloatingActionButton botonIrHome;
    private Button botonReconnect;
    private ImageView imagen;
    private String mDeviceName;
    private String mDeviceAddress = null;
    private String mMeterSerialNumber = null;
    private String mMeasurePulseRate = null;
    private String mMeasureSPO2 = null;
    private BluetoothLeService mBluetoothLeService;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private Context context;
    private Activity activity;
    private Intent intent;
    private String value = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Nonin3230");

        intent = getIntent();

        setContentView(R.layout.nonin3230_control_activity);

        imagen = (ImageView)findViewById(R.id.listenerImage);
        imagen.setImageResource(R.drawable.nonin_3230);

        botonIrHome = (FloatingActionButton)findViewById(R.id.listenerClose);

        //Cambiar nombre en vista
        botonReconnect = (Button)findViewById(R.id.listenerManual);
        botonReconnect.setOnClickListener(this);
        botonIrHome.setOnClickListener(this);


        context = getApplicationContext();
        //setActionBar();
        activity = this;
        final Intent intent = getIntent();



        mDeviceName = intent.getStringExtra(N3230Utils.EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(N3230Utils.EXTRAS_DEVICE_ADDRESS);

        connectDevice();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    private void connectDevice() {
        if (mDeviceAddress != null && !mDeviceAddress.equals("")
                && mDeviceName.contains("Nonin3230")) {
            DisplayFunctions.showProgressDialog(this,
                    getString(R.string.meter_type_nonin_3230),
                    getString(R.string.send_meter_values_waiting_valid_values),
                    true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent gattServiceIntent = new Intent(activity,
                            BluetoothLeService.class);
                    bindService(gattServiceIntent, mServiceConnection,
                            BIND_AUTO_CREATE);
                }
            }, N3230Utils.TIME_TAKING_MEASURES);
        } else {
            DisplayFunctions
                    .showToast(
                            context,
                            getString(R.string.send_meter_values_error_connecting_device));
        }
    }
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service)
                    .getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            manageGattConnecting();
            // Automatically connects to the device upon successful start-up
            // initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private void startAsyncTaskSendResults() {
        Log.i("NONIN3230 CONTROL", "AsyncTask Send Results");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {

            protected void onPreExecute() {
                DisplayFunctions.changeTextProgressDialog(activity,
                        getString(R.string.meter_type_nonin_3230),
                        getString(R.string.send_meter_values_sending_values),
                        true);
            }

            protected ArrayList<MeterValue> doInBackground(String... params) {
                ArrayList<MeterValue> valuesToSend = new ArrayList<MeterValue>();
                MeterValue heartRateResult = getHeartRateResult();
                if (heartRateResult != null) {
                    valuesToSend.add(heartRateResult);
                    if (!heartRateResult.getValue().equals("E511")){
                        value = heartRateResult.getValue() + "/min";
                        heartRateResult.setValidate(true);
                    }else {
                        value = heartRateResult.getValue();
                        heartRateResult.setValidate(false);
                    }
                }
                MeterValue bloodOxygenSaturationResult = getBloodOxygenSaturationResult();
                if (bloodOxygenSaturationResult != null){
                    valuesToSend.add(bloodOxygenSaturationResult);
                    if (!bloodOxygenSaturationResult.getValue().equals("E127")) {
                        value += " " + bloodOxygenSaturationResult.getValue() + "/Sp02";
                        bloodOxygenSaturationResult.setValidate(true);
                    }else {
                        value += " " + bloodOxygenSaturationResult.getValue();
                        bloodOxygenSaturationResult.setValidate(false);
                    }
                }

                return _meterValueController.AddNewListMeterValues(context,
                        valuesToSend);
            }

            protected void onPostExecute(ArrayList<MeterValue> meterValues) {
                String message = getString(R.string.send_meter_values_sending_values_success);
                for (int i=0;i < meterValues.size();i++){
                    if(meterValues.get(i).getValue().equals("511") || meterValues.get(i).getValue().equals("127") ){
                        message = getString(R.string.send_meter_values_sending_values);
                    }
                }
                DisplayFunctions.CancelDialog();
                DisplayFunctions
                        .showToast(
                                context,
                                (meterValues == null || (meterValues != null && meterValues
                                        .size() > 0)) ? getString(R.string.send_meter_values_sending_values_error)
                                        : message);
                    launchResultActivity(true);
            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }
    private void launchResultActivity(boolean success){
        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra("success", success);
        resultIntent.putExtra("imagen", R.drawable.nonin_3230);
        resultIntent.putExtra("value",value);
        startActivity(resultIntent);
        finish();
    }

    private MeterValue getHeartRateResult() {
        HeartRateValue heartRateValue = new HeartRateValue();
        heartRateValue.setMeterId(mMeterSerialNumber);
        heartRateValue.setValue(mMeasurePulseRate);
        heartRateValue.setDateRead(new Date());
        if(mMeasurePulseRate.equals("511")){
            heartRateValue.setValidate(false);
        }else{
            heartRateValue.setValidate(true);
        }

        return heartRateValue;
    }

    private MeterValue getBloodOxygenSaturationResult() {
        OxygenSaturationValue oxygenSaturationValue = new OxygenSaturationValue();
        oxygenSaturationValue.setMeterId(mMeterSerialNumber);
        oxygenSaturationValue.setValue(mMeasureSPO2);
        oxygenSaturationValue.setDateRead(new Date());
        if(mMeasureSPO2.equals("127")){
            oxygenSaturationValue.setValidate(false);
        }else{
            oxygenSaturationValue.setValidate(true);
        }

        return oxygenSaturationValue;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(N3230Utils.ACTION_GATT_CONNECTED);
        intentFilter.addAction(N3230Utils.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(N3230Utils.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(N3230Utils.ACTION_SERIAL_NUMBER_AVAILABLE);
        intentFilter.addAction(N3230Utils.ACTION_MEASURE_AVAILABLE);
        return intentFilter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mGattUpdateReceiver);
           // unregisterReceiver(mGattUpdateReceiver);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "Nonin3230ControlActivity onPauseNonin3230");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unbindService(mServiceConnection);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "Nonin3230ControlActivity OnDestroy");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
        mBluetoothLeService = null;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void readCharacteristic(UUID bluetoothGattServiceUuid,
                                   UUID bluetoothGattCharacteristicUuid) {

        BluetoothGattService bluetoothGattService = getBluetoothGattServiceFromUUID(bluetoothGattServiceUuid);

        if (bluetoothGattService == null) {
            Log.e(TAG, "service not found!");
            return;
        }

        BluetoothGattCharacteristic bluetoothGattCharacteristic = bluetoothGattService
                .getCharacteristic(bluetoothGattCharacteristicUuid);
        if (bluetoothGattCharacteristic == null) {
            Log.e(TAG, "char not found!");
            return;
        }

        final int charaProp = bluetoothGattCharacteristic.getProperties();
        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
            // If there is an active notification on a characteristic,
            // clear
            // it first so it doesn't update the data field on the user
            // interface.
            if (mNotifyCharacteristic != null) {
                mBluetoothLeService.setCharacteristicNotification(
                        mNotifyCharacteristic, false);
                mNotifyCharacteristic = null;
            }
            mBluetoothLeService.readCharacteristic(bluetoothGattService,
                    bluetoothGattCharacteristic);
        }
        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
            mNotifyCharacteristic = bluetoothGattCharacteristic;
            mBluetoothLeService.setCharacteristicNotification(
                    bluetoothGattCharacteristic, true);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void writeCharacteristic(UUID bluetoothGattServiceUuid,
                                     UUID bluetoothGattCharacteristicUuid, String command) {

        BluetoothGattService bluetoothGattService = getBluetoothGattServiceFromUUID(bluetoothGattServiceUuid);

        if (bluetoothGattService == null) {
            Log.e(TAG, "service not found!");
            return;
        }

        BluetoothGattCharacteristic bluetoothGattCharacteristic = bluetoothGattService
                .getCharacteristic(bluetoothGattCharacteristicUuid);

        if (bluetoothGattCharacteristic == null) {
            Log.e(TAG, "char not found!");
            return;
        }

        mBluetoothLeService.writeCharacteristic(bluetoothGattService,
                bluetoothGattCharacteristic, command);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private BluetoothGattService getBluetoothGattServiceFromUUID(
            UUID bluetoothGattServiceUuid) {
        List<BluetoothGattService> gattServices = mBluetoothLeService
                .getSupportedGattServices();

        for (BluetoothGattService gattService : gattServices) {
            if (gattService.getUuid().equals(bluetoothGattServiceUuid))
                return gattService;
        }
        return null;
    }

	/*
	 * HANDLER EVENTS
	 */

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {
                case N3230Utils.ACTION_GATT_CONNECTING:
                    manageGattConnecting();
                    break;
                case N3230Utils.ACTION_GATT_CONNECTED:
                    manageGattConnected();
                    break;
                case N3230Utils.ACTION_GATT_DISCONNECTED:
                    manageGattDisconnected();
                    break;
                case N3230Utils.ACTION_GATT_SERVICES_DISCOVERED:
                    manageGattServicesDiscovered();
                    break;
                case N3230Utils.ACTION_SERIAL_NUMBER_AVAILABLE:
                    manageSerialNumberAvailable(intent);
                    break;
                case N3230Utils.ACTION_MEASURE_AVAILABLE:
                    manageMeasureAvailable(intent);
                    break;
                default:
                    break;
            }
        }
    };

    protected void manageGattConnecting() {
        DisplayFunctions.changeTextProgressDialog(this,
                getString(R.string.meter_type_nonin_3230),
                getString(R.string.send_meter_values_connecting), true);
    }

    private void manageGattConnected() {
        DisplayFunctions
                .changeTextProgressDialog(
                        this,
                        getString(R.string.meter_type_nonin_3230),
                        getString(R.string.send_meter_values_getting_device_info),
                        true);
    }

    private void manageGattDisconnected() {

    }

    private void manageGattServicesDiscovered() {
        readCharacteristic(N3230Utils.DEVICE_INFORMATION_SERVICE,
                N3230Utils.SERIAL_NUMBER_CHARACTERISTIC);
    }

    private void manageSerialNumberAvailable(Intent intent) {
        DisplayFunctions.changeTextProgressDialog(this,
                getString(R.string.meter_type_nonin_3230),
                getString(R.string.send_meter_values_reading_data), true);
        String serialNumber = intent.getStringExtra(N3230Utils.EXTRAS_DATA);
        if (serialNumber != null && !serialNumber.isEmpty()) {
            mMeterSerialNumber = serialNumber.split("\n")[0];
            readCharacteristic(N3230Utils.NONIN_OXIMETRY_SERVICE,
                    N3230Utils.NONIN_OXIMETRY_MEASUREMENT_CHARACTERISTIC);
        } else {
            manageStateError();
        }
    }

    protected void manageStateError() {
        DisplayFunctions.showToast(context,
                getString(R.string.send_meter_values_error_connecting_device));
        DisplayFunctions.CancelDialog();
        finish();
    }

    private void manageMeasureAvailable(Intent intent) {
        String pulseRate = intent
                .getStringExtra(N3230Utils.EXTRAS_MEASURE_PULSE_RATE);
        String spo2 = intent.getStringExtra(N3230Utils.EXTRAS_MEASURE_SPO2);
        if (pulseRate != null && spo2 != null) {
            if (pulseRate.equals("511") || spo2.equals("127")){
                pulseRate = "E511";
                spo2 = "E127";
            }
            mMeasurePulseRate = pulseRate;
            mMeasureSPO2 = spo2;

            writeCharacteristic(
                    N3230Utils.NONIN_OXIMETRY_SERVICE,
                    N3230Utils.NONIN_CONTROL_POINT_CHARACTERISTIC,
                    N3230Utils.CONTROL_POINT_MEASUREMENT_COMPLETE_PROCEDURE_COMMAND);
            unregisterReceiver(mGattUpdateReceiver);
            sendMeasure();
        } else {
            manageStateError();
        }
    }

    protected void sendMeasure() {
        if (ConnectivityFunctions.networkAvailable(context)) {
            startAsyncTaskSendResults();
        } else {
            DisplayFunctions.showToast(context,
                    getString(R.string.general_network_connection_problems));
            DisplayFunctions.CancelDialog();
            finish();
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public void onClick(View v) {
        if (v == botonIrHome) {
            launchMainMenuActivity();
        }else if (v == botonReconnect){

        }
    }
    private void launchMainMenuActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startActivity(homeIntent);
        finish();
    }
}
