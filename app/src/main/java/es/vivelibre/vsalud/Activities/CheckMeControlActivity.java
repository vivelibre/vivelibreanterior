package es.vivelibre.vsalud.Activities;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.uxcam.UXCam;
import com.viatom.azur.bluetooth.BTConnectListener;
import com.viatom.azur.bluetooth.BTUtils;
import com.viatom.azur.bluetooth.BTUtils.BTBinder;
import com.viatom.azur.bluetooth.GetInfoThreadListener;
import com.viatom.azur.bluetooth.PingThreadListener;
import com.viatom.azur.bluetooth.ReadFileListener;
import com.viatom.azur.measurement.DailyCheckItem;
import com.viatom.azur.measurement.ECGItem;
import com.viatom.azur.measurement.SPO2Item;
import com.viatom.azur.measurement.User;
import com.viatom.azur.utils.FileDriver;
import com.viatom.azur.utils.FileUtils;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.CheckMeDevice;
import es.vivelibre.models.Meter.CheckMeFile;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.vsalud.Bluetooth.Viatom.CheckMeUtils;
import es.vivelibre.vsalud.Bluetooth.and.ANDDevicesUtils;
import es.vivelibre.vsalud.Controllers.MeterValueController;
import es.vivelibre.vsalud.Functions.ConnectivityFunctions;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.R;

public class CheckMeControlActivity extends AppCompatActivity implements
        BTConnectListener, PingThreadListener, GetInfoThreadListener,
        ReadFileListener, OnClickListener {

    private BTBinder mBinder;
    private Context context;
    private BluetoothDevice btDevice = null;
    private BluetoothAdapter btAdapter;
    private File root;
    private File dir;

    private Button btn_oxi;
    private Button btn_daily_check;

    private ArrayList<User> dailyCheckUsers = null;
    private DailyCheckItem dailyCheckItemToSend = null;

    private CheckMeDevice checkMeDevice = null;

    private MeterValueController _meterValueController = MeterValueController
            .getInstance();

    private ImageView imagen;
    private FloatingActionButton botonIrHome;
    private Intent intent;
    private Button botonReconnect;
    private String value;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Checkme");

        intent = getIntent();

        Calendar calendar = Calendar.getInstance();
        calendar.getTimeZone();
        setContentView(R.layout.checkme_control_activity);

        imagen = (ImageView)findViewById(R.id.listenerImage);
        imagen.setImageResource(intent.getIntExtra("imagen",0));

        //Cambiar nombre en vista
        botonReconnect = (Button)findViewById(R.id.listenerManual);
        botonIrHome = (FloatingActionButton)findViewById(R.id.listenerClose);
        botonReconnect.setOnClickListener(this);
        botonIrHome.setOnClickListener(this);

        manageOnCreate();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    private void manageOnCreate() {
        context = getApplicationContext();
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,
                    ANDDevicesUtils.REQUEST_ENABLE_BT);
        } else {
            startBluetoothScanDevices();
        }

        btn_oxi = (Button) findViewById(R.id.oxi);
        btn_oxi.setOnClickListener(this);

        btn_daily_check = (Button) findViewById(R.id.daily_check);
        btn_daily_check.setOnClickListener(this);
    }

    private void startBluetoothScanDevices() {
        getBTDevice();
        if (btDevice == null) {
            mHandler.obtainMessage(CheckMeUtils.MESSAGE_ERROR, 1, 0)
                    .sendToTarget();
        } else {
            bindService();
        }
    }

    @Override
    protected void onDestroy() {
        /*if (conn != null)
            unbindService(conn);*/
        finish();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ANDDevicesUtils.REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    startBluetoothScanDevices();
                } else {
                    DisplayFunctions.showToast(context,
                            getString(R.string.general_bluetooth_not_available));
                    finish();
                }
                break;
            case CheckMeUtils.SELECT_USER_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    int userId = Integer.valueOf(data.getStringExtra("userId"));
                    readFile(
                            CheckMeUtils.XDLC_FILE.replace("x",
                                    String.valueOf(userId)), (byte) 0);
                } else {
                    DisplayFunctions.showToast(context,
                            getString(R.string.viatom_checkme_select_user_error));
                }
                break;
        }
    }

    private void getBTDevice() {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_STATE_SEARCHING_DEVICE, 1,
                0).sendToTarget();
        btDevice = null;
        Set<BluetoothDevice> pairedBTDevices = btAdapter.getBondedDevices();

        if (pairedBTDevices.size() > 0) {
            for (BluetoothDevice device : pairedBTDevices) {
                String name = device.getName();
                if (name.contains("Checkme")) {
                    btDevice = device;
                    return;
                }
            }
        }
    }

    private void bindService() {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_CONNECTING, 1, 0)
                .sendToTarget();
        bindService(new Intent(context,BTUtils.class), conn,
                Service.BIND_AUTO_CREATE);
    }

    private ServiceConnection conn = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            mBinder = (BTUtils.BTBinder) service;
            connectCheckMe(btDevice.getAddress());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };



    private void connectCheckMe(String macAddress) {
        if (mBinder != null) {
            mBinder.interfaceConnect(macAddress, this);
        }
    }

    @Override
    public void onConnectFailed(byte arg0) {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_ERROR, 1, 0).sendToTarget();
    }

    @Override
    public void onConnectSuccess() {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_CONNECTED, 1, 0)
                .sendToTarget();
    }

    private void ping() {
        if (mBinder != null) {
            mBinder.interfacePing(1000, this);
        }
    }

    @Override
    public void onPingFailed(byte arg0) {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_ERROR, 1, 0).sendToTarget();
    }

    @Override
    public void onPingSuccess() {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_PING, 1, 0).sendToTarget();
    }

    private void getCheckMeInfo() {
        if (mBinder != null) {
            mBinder.interfaceGetInfo(1000, this);
        }
    }

    @Override
    public void onGetInfoFailed(byte errCode) {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_ERROR, 1, 0).sendToTarget();
    }

    @Override
    public void onGetInfoSuccess(String checkMeInfo) {
        checkMeDevice = CheckMeUtils.GetCheckMeDevice(checkMeInfo);
        if (checkMeDevice != null) {
            mHandler.obtainMessage(CheckMeUtils.MESSAGE_DEVICE_INFO, 1, 0)
                    .sendToTarget();
        }
    }

    private void readFile(String fileName, byte fileType) {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_READING_FILE, 1, 0)
                .sendToTarget();
        if (mBinder != null) {
            mBinder.interfaceReadFile(fileName, fileType, 5000, this);
        }
    }

    @Override
    public void onReadFailed(String filename, byte fileType, byte errCode) {
        mHandler.obtainMessage(CheckMeUtils.MESSAGE_ERROR, 1, 0).sendToTarget();
    }

    @Override
    public void onReadPartFinished(String filename, byte fileType,
                                   float percentage) {

    }

    @Override
    public void onReadSuccess(String filename, byte fileType, byte[] fileBuf) {
        if (filename.equals(CheckMeUtils.USR_FILE)) {
            manageReadUsersSuccess(filename, fileType, fileBuf);
        } else if (filename.contains(CheckMeUtils.XDLC_FILE.substring(1))) {
            manageReadDailyCheckFileComplete(filename, fileBuf);
        } else {
            mHandler.obtainMessage(CheckMeUtils.MESSAGE_READING_FILE_COMPLETE,
                    new CheckMeFile(filename, fileType, fileBuf))
                    .sendToTarget();
        }
    }

    private void manageReadUsersSuccess(String filename, byte fileType,
                                        byte[] fileBuf) {
        if (initDir()) {
            File file = new File(dir, filename);
            if (file != null && file.exists())
                FileDriver.delFile(dir, filename);
            FileDriver.write(dir, filename, fileBuf);

            User[] USRList = FileUtils.readUserList(dir, filename);
            mHandler.obtainMessage(CheckMeUtils.MESSAGE_READING_USERS_COMPLETE,
                    USRList).sendToTarget();
        } else {
            mHandler.obtainMessage(CheckMeUtils.MESSAGE_ERROR, 1, 0)
                    .sendToTarget();
        }
    }

    private void manageReadDailyCheckFileComplete(String filename,
                                                  byte[] fileBuf) {

        if (initDir()) {
            FileDriver.delFile(dir, filename);
            FileDriver.write(dir, filename, fileBuf);
        }

        List<DailyCheckItem> newDLCList = FileUtils.readDailyCheckList(dir,
                filename);

        if ((newDLCList != null) && (newDLCList.size() > 0)) {
            if (dailyCheckItemToSend == null) {
                dailyCheckItemToSend = newDLCList.get(newDLCList.size() - 1);
            } else {
                DailyCheckItem aux = newDLCList.get(newDLCList.size() - 1);
                if (aux.getDate().getTime() > dailyCheckItemToSend.getDate()
                        .getTime()) {
                    dailyCheckItemToSend = aux;
                }
            }
        }

        if (dailyCheckUsers.size() > 0) {
            int userId = dailyCheckUsers.get(0).getUserInfo().getID();
            dailyCheckUsers.remove(0);
            readFile(
                    CheckMeUtils.XDLC_FILE.replace("x", String.valueOf(userId)),
                    (byte) 0);
        } else {
            mHandler.obtainMessage(
                    CheckMeUtils.MESSAGE_READING_DAILY_CHECK_FILES_COMPLETE,
                    dailyCheckItemToSend).sendToTarget();
            dailyCheckItemToSend = null;
            dailyCheckUsers = null;
        }
    }

    private ArrayList<MeterValue> manageReadSuccess(String filename,
                                                    byte fileType, byte[] fileBuf) {
        if (initDir()) {
            FileDriver.delFile(dir, filename);
            FileDriver.write(dir, filename, fileBuf);

            if (filename.equals(CheckMeUtils.ECG_FILE)) {
                // return readECGFile(oldECGList);
            } else if (filename.equals(CheckMeUtils.OXI_FILE)) {
                return manageOXIFile();
            }
        }
        return null;
    }

    private boolean initDir() {
        root = Environment.getExternalStorageDirectory();
        if (root == null)
            root = Environment.getDataDirectory();
        dir = new File(root, "CheckmeMobile/" + checkMeDevice.getSerialNumber());
        return dir.exists() || dir.mkdirs();
    }

    public void readECGFile(List<ECGItem> oldECGList) {
        List<ECGItem> newECGList = FileUtils.readECGList(dir,
                CheckMeUtils.ECG_FILE);
        if ((newECGList != null) && (newECGList.size() != 0)) {
            ECGItem localECGItem = (ECGItem) newECGList.get(0);
            String str = new SimpleDateFormat("yyyyMMddHHmmss")
                    .format(localECGItem.getDate());
            localECGItem.setInnerItem(FileUtils.readECGInnerItem(dir, str));
        }
    }

	/*
	 * SPO2
	 */

    public ArrayList<MeterValue> manageOXIFile() {
        List<SPO2Item> newOXIList = FileUtils.readSPO2List(dir,
                CheckMeUtils.OXI_FILE);

        if ((newOXIList != null) && (newOXIList.size() > 0)) {
            ArrayList<MeterValue> valuesToSend = new ArrayList<MeterValue>();
            SPO2Item SPO2Item = newOXIList.get(newOXIList.size() - 1);
            HeartRateValue heartRateValueDTO = (HeartRateValue) CheckMeUtils
                    .GetHeartRateResult(checkMeDevice, SPO2Item);
            if (heartRateValueDTO != null)
                valuesToSend.add(heartRateValueDTO);
            OxygenSaturationValue oxygenSaturationValueDTO = (OxygenSaturationValue) CheckMeUtils
                    .GetBloodOxygenSaturationResult(checkMeDevice, SPO2Item);
            if (oxygenSaturationValueDTO != null)
                valuesToSend.add(oxygenSaturationValueDTO);

            if (valuesToSend.size() > 0) {
                value = valuesToSend.get(0).getValue() + " P.P.M \n " +  valuesToSend.get(1).getValue() + " %";
                return _meterValueController.AddNewListMeterValues(context,
                        valuesToSend);
            }
        }

        return new ArrayList<MeterValue>();
    }

	/*
	 * DAILY CHECK
	 */

    public ArrayList<MeterValue> manageDLCFile(DailyCheckItem DailyCheckItem) {
        ArrayList<MeterValue> valuesToSend = new ArrayList<MeterValue>();
        HeartRateValue heartRateValue = (HeartRateValue) CheckMeUtils
                .GetHeartRateResult(checkMeDevice, DailyCheckItem);
        if (heartRateValue != null)
            valuesToSend.add(heartRateValue);
        OxygenSaturationValue oxygenSaturationValue = (OxygenSaturationValue) CheckMeUtils
                .GetBloodOxygenSaturationResult(checkMeDevice, DailyCheckItem);
        if (oxygenSaturationValue != null)
            valuesToSend.add(oxygenSaturationValue);

        BloodPressureValue bloodPressureValue = (BloodPressureValue) CheckMeUtils
                .GetBloodPressureResult(checkMeDevice, DailyCheckItem);
        if (bloodPressureValue != null)
            valuesToSend.add(bloodPressureValue);

        if (valuesToSend.size() > 0) {
            return _meterValueController.AddNewListMeterValues(context,
                    valuesToSend);
        }
        return new ArrayList<MeterValue>();
    }

    @Override
    public void onClick(View v) {
        if (v == btn_oxi) {
            readFile(CheckMeUtils.OXI_FILE, (byte) 0);
        } else if (v == btn_daily_check) {
            readFile(CheckMeUtils.USR_FILE, (byte) 0);
        }else if (v == botonIrHome) {
            launchMainMenuActivity();
        }else if (v == botonReconnect){
            if (!btAdapter.isEnabled()) {
                Intent enableIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent,
                        ANDDevicesUtils.REQUEST_ENABLE_BT);
            } else {
                startBluetoothScanDevices();
            }
        }
    }

	/*
	 * SEND RESULT
	 */

    private void startAsyncTaskSendResults(final String filename,
                                           final byte fileType, final byte[] fileBuf) {
        Log.i("CHECKME CONTROLACTIVITY", "AsyncTask Send Result");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {

            protected void onPreExecute() {
                mHandler.obtainMessage(CheckMeUtils.MESSAGE_SENDING_VALUES, 1,
                        0).sendToTarget();

            }

            protected ArrayList<MeterValue> doInBackground(String... params) {
                return manageReadSuccess(filename, fileType, fileBuf);
            }

            protected void onPostExecute(ArrayList<MeterValue> meterValues) {
                DisplayFunctions.CancelDialog();
                if (meterValues == null
                        || (meterValues != null && meterValues.size() > 0)) {
                    mHandler.obtainMessage(
                            CheckMeUtils.MESSAGE_SEND_VALUES_COMPLETE, 1, 0)
                            .sendToTarget();
                } else {
                    mHandler.obtainMessage(
                            CheckMeUtils.MESSAGE_SEND_VALUES_COMPLETE, 0, 0)
                            .sendToTarget();
                }
            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }

	/*
	 * SEND DAILY CHECK RESULT
	 */

    private void startAsyncTaskSendDailyCheckResults(
            final DailyCheckItem DailyCheckItem) {
        Log.i("CHECKMECONTROLACTIVITY", "AsyncTask Send Daily Result");
        AsyncTask<String, Float, ArrayList<MeterValue>> AsyncTaskAddNewMeterValue = new AsyncTask<String, Float, ArrayList<MeterValue>>() {

            protected void onPreExecute() {
                mHandler.obtainMessage(CheckMeUtils.MESSAGE_SENDING_VALUES, 1,
                        0).sendToTarget();
            }

            protected ArrayList<MeterValue> doInBackground(String... params) {
                return manageDLCFile(DailyCheckItem);
            }

            protected void onPostExecute(ArrayList<MeterValue> meterValues) {
                DisplayFunctions.CancelDialog();
                if (meterValues == null
                        || (meterValues != null && meterValues.size() > 0)) {
                    mHandler.obtainMessage(
                            CheckMeUtils.MESSAGE_SEND_VALUES_COMPLETE, 1, 0)
                            .sendToTarget();
                } else {
                    mHandler.obtainMessage(
                            CheckMeUtils.MESSAGE_SEND_VALUES_COMPLETE, 0, 0)
                            .sendToTarget();
                }
            }

        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            AsyncTaskAddNewMeterValue.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            AsyncTaskAddNewMeterValue.execute("");
    }

	/*
	 * HANDLER EVENTS
	 */

    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CheckMeUtils.MESSAGE_STATE_SEARCHING_DEVICE:
                    manageStateSearchingDevice();
                    break;
                case CheckMeUtils.MESSAGE_CONNECTING:
                    manageStateConnecting();
                    break;
                case CheckMeUtils.MESSAGE_CONNECTED:
                    manageStateConnected();
                    break;
                case CheckMeUtils.MESSAGE_DEVICE_INFO:
                    manageStateDeviceInfoObtained();
                    break;
                case CheckMeUtils.MESSAGE_PING:
                    manageStatePing();
                    break;
                case CheckMeUtils.MESSAGE_READING_FILE:
                    manageStateReadingFile();
                    break;
                case CheckMeUtils.MESSAGE_READING_FILE_COMPLETE:
                    CheckMeFile checkMeFile = (CheckMeFile) msg.obj;
                    manageStateReadingFileComplete(checkMeFile);
                    break;
                case CheckMeUtils.MESSAGE_READING_USERS_COMPLETE:
                    User[] users = (User[]) msg.obj;
                    manageStateReadingUsersFileComplete(users);
                    break;
                case CheckMeUtils.MESSAGE_READING_DAILY_CHECK_FILES_COMPLETE:
                    DailyCheckItem dailyCheckItem = (DailyCheckItem) msg.obj;
                    manageStateReadingDalilyCheckFilesComplete(dailyCheckItem);
                    break;
                case CheckMeUtils.MESSAGE_SENDING_VALUES:
                    manageStateSendingValues();
                    break;
                case CheckMeUtils.MESSAGE_SEND_VALUES_COMPLETE:
                    manageStateSendValuesComplete(msg.arg1 == 0);
                    break;
                case CheckMeUtils.MESSAGE_NETWORK_CONNECTIVITY_ERROR:
                    manageStateNetworkConnectivityError();
                    break;
                case CheckMeUtils.MESSAGE_ERROR:
                    manageStateError();
                    break;
            }
        }
    };

    protected void manageStateSearchingDevice() {
        DisplayFunctions.showProgressDialog(this,
                getString(R.string.meter_type_viatom_checkme),
                getString(R.string.send_meter_values_searching_device), true);
    }

    protected void manageStateConnecting() {
        DisplayFunctions.changeTextProgressDialog(this,
                getString(R.string.meter_type_viatom_checkme),
                getString(R.string.send_meter_values_connecting), true);
    }

    protected void manageStateConnected() {
        DisplayFunctions.changeTextProgressDialog(this,
                getString(R.string.meter_type_viatom_checkme),
                getString(R.string.send_meter_values_checking_connectivity),
                true);
        ping();
    }

    private void manageStateDeviceInfoObtained() {
        DisplayFunctions.CancelDialog();
        btn_oxi.setEnabled(true);
        btn_daily_check.setEnabled(true);
    }

    protected void manageStatePing() {
        DisplayFunctions
                .changeTextProgressDialog(
                        this,
                        getString(R.string.meter_type_viatom_checkme),
                        getString(R.string.send_meter_values_getting_device_info),
                        true);
        getCheckMeInfo();
    }

    protected void manageStateReadingFile() {
        DisplayFunctions.changeTextProgressDialog(this,
                getString(R.string.meter_type_viatom_checkme),
                getString(R.string.send_meter_values_reading_file), true);
    }

    protected void manageStateReadingFileComplete(CheckMeFile checkMeFile) {
        if (ConnectivityFunctions.networkAvailable(context)) {
            startAsyncTaskSendResults(checkMeFile.getFileName(),
                    checkMeFile.getFileType(), checkMeFile.getFileBuf());
        } else {
            mHandler.obtainMessage(
                    CheckMeUtils.MESSAGE_NETWORK_CONNECTIVITY_ERROR, 1, 0)
                    .sendToTarget();
        }
    }

    protected void manageStateReadingUsersFileComplete(User[] arrayUsers) {
        if (arrayUsers != null && arrayUsers.length > 0) {
            dailyCheckUsers = new ArrayList<User>();
            for (User user : arrayUsers) {
                dailyCheckUsers.add(user);
            }

            DisplayFunctions
                    .showProgressDialog(
                            this,
                            getString(R.string.meter_type_viatom_checkme),
                            getString(R.string.viatom_checkme_reading_daily_check_files),
                            true);

            int userId = dailyCheckUsers.get(0).getUserInfo().getID();
            dailyCheckUsers.remove(0);
            readFile(
                    CheckMeUtils.XDLC_FILE.replace("x", String.valueOf(userId)),
                    (byte) 0);
        } else {
            mHandler.obtainMessage(CheckMeUtils.MESSAGE_ERROR, 1, 0)
                    .sendToTarget();
        }
    }

    protected void manageStateReadingDalilyCheckFilesComplete(
            DailyCheckItem dailyCheckItem) {
        if (dailyCheckItem != null) {
            startAsyncTaskSendDailyCheckResults(dailyCheckItem);
        }
    }

    protected void manageStateSendingValues() {
        DisplayFunctions.changeTextProgressDialog(this,
                getString(R.string.meter_type_viatom_checkme),
                getString(R.string.send_meter_values_sending_values), true);
    }

    protected void manageStateSendValuesComplete(boolean success) {
        DisplayFunctions
                .showToast(
                        context,
                        (success) ? getString(R.string.send_meter_values_sending_values_success)
                                : getString(R.string.send_meter_values_sending_values_error));
        if (success) {
            finish();
            launchResultActivity(success);
        } else {

        }
    }

    protected void manageStateNetworkConnectivityError() {
        DisplayFunctions.showToast(context,
                getString(R.string.general_network_connection_problems));
        DisplayFunctions.CancelDialog();
        finish();
    }

    protected void manageStateError() {
        DisplayFunctions.showToast(context,
                getString(R.string.send_meter_values_error_connecting_device));
        DisplayFunctions.CancelDialog();
        finish();
    }
    private void launchMainMenuActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startActivity(homeIntent);
        finish();
    }
    private void launchResultActivity(boolean success){
        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra("success", success);
        resultIntent.putExtra("imagen", intent.getIntExtra("imagen",0));
        resultIntent.putExtra("value",value);
        startActivity(resultIntent);
        finish();
    }

}