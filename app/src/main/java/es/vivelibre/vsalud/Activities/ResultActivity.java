package es.vivelibre.vsalud.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.uxcam.UXCam;

import es.vivelibre.vsalud.R;


public class ResultActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Activity activity;
    private FloatingActionButton botonIrHome;
    private Button botonTomarMedida;
    public ImageView imagen;
    public TextView textoSuccess;
    public  Intent intent;
    public TextView resultValue;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Resultado");

        intent = getIntent();

        setContentView(R.layout.activity_result);


        botonTomarMedida = (Button)findViewById(R.id.listenerManual);
        botonIrHome = (FloatingActionButton)findViewById(R.id.resultClose);
        imagen =(ImageView)findViewById(R.id.resultImage);
        textoSuccess = (TextView)findViewById(R.id.resultStatus);

        botonTomarMedida.setOnClickListener(this);
        botonIrHome.setOnClickListener(this);

        //Aqui estamos colocando la imagen que viene del home
        imagen.setImageResource(intent.getIntExtra("imagen", 0));

        resultValue = (TextView)findViewById(R.id.resultValue);

        if (intent.getIntExtra("imagen", 0) ==  R.drawable.tensiometro) {
            String[] parts = intent.getStringExtra("value").split(":");
            if (parts[2].equals("Pulso irregular")){
                resultValue.setText(String.format("%1$3s /%2$3s mmHg\r\n%3$3s", parts[0], parts[1], parts[2]));
            }
            else
                resultValue.setText(String.format("%1$3s /%2$3s mmHg\r\n%3$3s p.p.m.", parts[0], parts[1], parts[2]));
        }else if (intent.getIntExtra("imagen", 0) ==  R.drawable.nonin_3230) {
            String[] parts = intent.getStringExtra("value").split(" ");
            resultValue.setText(String.format("%1$3s p.p.m.\r\n%2$3s Sp02", parts[0].split("/")[0], parts[1].split("/")[0]));
        }else {
            resultValue.setText(intent.getStringExtra("value"));
        }
        if (intent.getBooleanExtra("success", false)){
            if (intent.getIntExtra("imagen", 0) ==  R.drawable.tensiometro) {
                String[] parts = intent.getStringExtra("value").split(":");
                if (parts[2].equals("Pulso irregular")){
                    textoSuccess.setText(getResources().getString(R.string.send_meter_values_sending_values));
                }else{
                    textoSuccess.setText(getResources().getString(R.string.resultStatus_ok));
                }
            }else if (intent.getIntExtra("imagen", 0) ==  R.drawable.nonin_3230) {
                String[] parts = intent.getStringExtra("value").split(" ");
                if (parts[0].equals("E511") || parts[1].equals("E127")){
                    textoSuccess.setText(getResources().getString(R.string.send_meter_values_sending_values));
                }else{
                    textoSuccess.setText(getResources().getString(R.string.resultStatus_ok));
                }
            }else{
                textoSuccess.setText(getResources().getString(R.string.resultStatus_ok));
            }
        }

    }

    private void initializeViews() {
        activity = this;
        context = getApplicationContext();

    }

    @Override
    public void onClick(View v) {
        if (v == botonIrHome) {
            launchMainMenuActivity();
        }else if(v == botonTomarMedida){
            Intent backIntent;
            if (intent.getIntExtra("imagen", 0) ==  R.drawable.bascula || intent.getIntExtra("imagen", 0) ==  R.drawable.tensiometro) {
                backIntent = new Intent(this, ANDControlActivity.class);
            }else if(intent.getIntExtra("imagen", 0) ==  R.drawable.chino){
                backIntent = new Intent(this, CheckMeControlActivity.class);
            }else if(intent.getIntExtra("imagen", 0) ==  R.drawable.nonin_3230){
                backIntent = new Intent(this, BluetoothLeScanActivity.class);
            }
            else{
                backIntent = new Intent(this, HomeActivity.class);
            }

            //Esto es para pasar datos a la vista nueva
            backIntent.putExtra("imagen", intent.getIntExtra("imagen", 0));

            startActivity(backIntent);
            finish();
        }

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent backIntent = new Intent(this, HomeActivity.class);
            //Esto es para pasar datos a la vista nueva
            backIntent.putExtra("imagen", intent.getIntExtra("imagen", 0));

            startActivity(backIntent);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void launchMainMenuActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        startActivity(homeIntent);
        finish();
    }


}
