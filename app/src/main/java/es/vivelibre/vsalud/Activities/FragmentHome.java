package es.vivelibre.vsalud.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.uxcam.UXCam;

import java.util.ArrayList;
import java.util.List;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.models.Meter.WeightValue;
import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.Utils.CardViewModel;
import es.vivelibre.vsalud.Adapters.CardViewAdapter;
import es.vivelibre.vsalud.Controllers.HomeController;
import es.vivelibre.vsalud.R;

import static com.google.android.gms.internal.zzs.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentHome.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentHome extends Fragment{

    private OnFragmentInteractionListener mListener;
    private static HomeController homeController = HomeController.getInstance();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager lmanager;
    private CardViewAdapter adapter;
    private ProgressBar progressbar;
    private SwipeRefreshLayout refreshLayout;

    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        UXCam.startWithKey("877c7902aa8d1d2");
        UXCam.tagScreenName("Principal");

        final View rootView =  inflater.inflate(R.layout.fragment_home, container, false);

        progressbar = (ProgressBar) rootView.findViewById(R.id.progressBar1);
        progressbar.setVisibility(View.GONE);

        // Obtener el refreshLayout
        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.homeFragment);


        // Iniciar la tarea asíncrona al revelar el indicador
        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new Asintask().execute();
                    }
                }
        );

        recyclerView = (RecyclerView) rootView.findViewById(R.id.reciclador);

        new Asintask().execute();
        return rootView;
    }



    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    //Toast.makeText(getActivity(), "Falta implementación", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    return true;
                }
                return false;
            }
        });

    }

    //Variable booleana, comprobacion doble click
    boolean doubleBackToExitPressedOnce = false;

    //click en backButton
    public void onBackPressed() {
        //Creacion de Dialog si hay doble click
        if (doubleBackToExitPressedOnce) {
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle( getResources().getString(
                    R.string.closeApp));
            dialogo1.setMessage(getResources().getString(
                    R.string.alert_dialog_confirmar_desconexion_summary));
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton(
                    getResources().getString(
                            R.string.goOut),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //getActivity().finish();
                            System.exit(0);
                        }
                    });
            dialogo1.setNegativeButton(
                    getResources().getString(
                            R.string.alert_dialog_confirmar_desconexion_cancelar),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            //Sin funcionalidad
                        }
                    });
            dialogo1.show();
        }

        this.doubleBackToExitPressedOnce = true;
        /*Toast.makeText(this.getActivity(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();*/

        //Manejador de cambio en la variable booleana
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class Asintask extends AsyncTask<String , Integer ,MonitorParameters> {

        private List items;

        protected void onPreExecute() {
            Log.i("FRAGMENT HOME", "AsyncTask Get Monitors Parameters");
            progressbar.setVisibility(View.VISIBLE);
            items = new ArrayList();
            // Obtener el Recycler
            //recyclerView = (RecyclerView) findViewById(R.id.reciclador);
            recyclerView.setHasFixedSize(true);

            // Usar un administrador para LinearLayout
            lmanager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(lmanager);

            adapter = new CardViewAdapter(getActivity(),items);
            recyclerView.setAdapter(adapter);


        }
        @Override
        protected MonitorParameters doInBackground(String... params) {
            try {
                MonitorParameters monitorParameters = homeController.GetMonitorParameters(getActivity());
                if (monitorParameters != null ){

                    monitorParameters = comprobarIds(monitorParameters);

                    if (monitorParameters.bloodOxygenSaturation.isActive()){
                        if (monitorParameters.getBloodOxygenSaturationType().equals("PulseOximeter")) {
                            items.add(new CardViewModel(getResources().getString(R.string.oxygenSaturation), monitorParameters.getBloodOxygenSaturationLastValue().getValue(), R.drawable.nonin_3230, monitorParameters.getBloodOxygenSaturationLastValue().getDateRead().toString(), "%"));
                        }else {
                            items.add(new CardViewModel(getResources().getString(R.string.oxygenSaturation),monitorParameters.getBloodOxygenSaturationLastValue().getValue(), R.drawable.chino, monitorParameters.getBloodOxygenSaturationLastValue().getDateRead().toString(), "%"));
                        }
                    }
                    if (monitorParameters.bloodPressure.isActive()){
                        String[] separated = monitorParameters.getBloodPressureLastValue().getValue().split("@");
                        if (monitorParameters.getBloodPressureType().equals("BloodPressureMonitor")) {
                            items.add(new CardViewModel(getResources().getString(R.string.bloodPressure), separated[0] + "/" + separated[1] , R.drawable.tensiometro, monitorParameters.getBloodPressureLastValue().getDateRead().toString(), "mmHg"));
                        }else {
                            items.add(new CardViewModel(getResources().getString(R.string.bloodPressure), separated[0] + "/" + separated[1], R.drawable.chino, monitorParameters.getBloodPressureLastValue().getDateRead().toString(), "mmHg"));
                        }
                    }
                    if(monitorParameters.heartRate.isActive())
                    {
                        if (monitorParameters.getHeartRateType().equals("HealthMonitor")){
                            items.add(new CardViewModel(getResources().getString(R.string.hearthRate),monitorParameters.getHeartRateLastValue().getValue() , R.drawable.chino, monitorParameters.getHeartRateLastValue().getDateRead().toString(),"p.p.m."));
                        }else if (monitorParameters.getHeartRateType().equals("BloodPressureMonitor")){
                            items.add(new CardViewModel(getResources().getString(R.string.hearthRate), monitorParameters.getHeartRateLastValue().getValue() , R.drawable.tensiometro, monitorParameters.getHeartRateLastValue().getDateRead().toString(),"p.p.m."));
                        }else{
                            items.add(new CardViewModel(getResources().getString(R.string.hearthRate), monitorParameters.getHeartRateLastValue().getValue(), R.drawable.nonin_3230, monitorParameters.getHeartRateLastValue().getDateRead().toString(),"p.p.m."));
                        }

                    }
                    if(monitorParameters.weight.isActive()){
                        items.add(new CardViewModel(getResources().getString(R.string.weight), monitorParameters.getWeightLastValue().getValue(), R.drawable.bascula,  monitorParameters.getWeightLastValue().getDateRead().toString(),"kg"));
                    }
                    if (items.size() == 0){
                        items.add(new CardViewModel("Sin dispositivos", "", R.drawable.ic_cancel_black,  "",""));
                        return monitorParameters;
                    }
                    return monitorParameters;
                }
            }catch (Exception e){
                FirebaseCrash.logcat(Log.ERROR, TAG, "FragmentHome AsyncTask Get Monitors Parameters");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(MonitorParameters result){
            if (result != null) {
                // Crear un nuevo adaptador
                //finish();
                adapter = new CardViewAdapter(getActivity(),items);
                progressbar.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                // Parar la animación del indicador
                refreshLayout.setRefreshing(false);
            }
        }
    }

    private MonitorParameters comprobarIds(MonitorParameters monitorParameters) {
        // Aqui lo que vamos hacer es crear una lista de los monitor parameters y vamos cambiando la hora
        // si el id del Shared es igual al Id del monitorParameters sabemos que es la ultima toma
        // si no son iguales no es la ultima toma y no actualizamos la fecha

        MonitorParameters monitorParametersCambiada = monitorParameters;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (monitorParameters.bloodOxygenSaturation.isActive()){
            Gson gson = new Gson();
            String json = prefs.getString("OxygenSaturationValue", "");
            OxygenSaturationValue obj = gson.fromJson(json,OxygenSaturationValue.class);

            if (obj != null) {
                if (obj.getId().equals(monitorParameters.getBloodOxygenSaturationLastValue().getIdValue())) {
                    // Los ids son iguales por lo tanto es la ultima medida que meti
                    // Pillamos el dataTime del Shared
                    monitorParametersCambiada.getBloodOxygenSaturationLastValue().setDateRead(obj.getDateRead());
                }
            }

        }
        if (monitorParameters.bloodPressure.isActive())
        {
            Gson gson = new Gson();
            String json = prefs.getString("BloodPressureValue", "");
            BloodPressureValue obj = gson.fromJson(json,BloodPressureValue.class);
            if (obj != null) {
                if (obj.getId().equals(monitorParameters.getBloodPressureLastValue().getIdValue())) {
                    // Los ids son iguales por lo tanto es la ultima medida que meti
                    // Pillamos el dataTime del Shared
                    monitorParametersCambiada.getBloodPressureLastValue().setDateRead(obj.getDateRead());
                }
            }
        }
        if(monitorParameters.heartRate.isActive())
        {
            Gson gson = new Gson();
            String json = prefs.getString("HeartRateValue", "");
            HeartRateValue obj = gson.fromJson(json,HeartRateValue.class);
            if (obj != null) {
                if (obj.getId().equals(monitorParameters.getHeartRateLastValue().getIdValue())) {
                    // Los ids son iguales por lo tanto es la ultima medida que meti
                    // Pillamos el dataTime del Shared
                    monitorParametersCambiada.getHeartRateLastValue().setDateRead(obj.getDateRead());
                }
            }
        }
        if(monitorParameters.weight.isActive())
        {
            // Los ids son iguales por lo tanto es la ultima medida que meti
            // Pillamos el dataTime del Shared
            Gson gson = new Gson();
            String json = prefs.getString("WeightValue", "");
            WeightValue obj = gson.fromJson(json,WeightValue.class);
            if (obj != null) {
                if (obj.getId().equals(monitorParameters.getWeightLastValue().getIdValue())) {
                    // Los ids son iguales por lo tanto es la ultima medida que meti
                    // Pillamos el dataTime del Shared
                    monitorParametersCambiada.getWeightLastValue().setDateRead(obj.getDateRead());
                }
            }
        }

        return monitorParameters;
    }


}
