package es.vivelibre.vsalud.Bluetooth.le;

/**
 * Created by mfernandez on 11/05/2016.
 */
public class BluetoothLeUtils {

    // Configuration
    public static final long SCAN_PERIOD = 10000;

    //Request
    public static final int REQUEST_ENABLE_BT = 1;

    // Bluetooth states
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
}
