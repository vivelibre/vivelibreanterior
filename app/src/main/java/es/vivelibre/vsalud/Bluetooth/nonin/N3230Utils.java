package es.vivelibre.vsalud.Bluetooth.nonin;

/**
 * Created by mfernandez on 11/05/2016.
 */

import java.util.UUID;

/**
 * This class contains generic constants to the Pulse Oximeter 3230
 */
public class N3230Utils {

    private static final String PACKAGE = "technology.vivelibre.health";
    public static final long TIME_TAKING_MEASURES = 10000;

    // GATT attributes
    public static UUID NONIN_PULSE_OX_PROFILE = UUID
            .fromString("0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b");
    public static UUID CLIENT_CHARACTERISTIC_CONFIG = UUID
            .fromString("00002902-0000-1000-8000-00805f9b34fb");

    public static UUID NONIN_OXIMETRY_SERVICE = UUID
            .fromString("46a970e0-0d5f-11e2-8b5e-0002a5d5c51b");
    public static UUID DEVICE_INFORMATION_SERVICE = UUID
            .fromString("0000180a-0000-1000-8000-00805f9b34fb");
    public static UUID GEMERIC_ACCESS_SERVICE = UUID
            .fromString("00001800-0000-1000-8000-00805f9b34fb");

    public static UUID NONIN_OXIMETRY_MEASUREMENT_CHARACTERISTIC = UUID
            .fromString("0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b");
    public static UUID MANUFACTURER_CHARACTERISTIC = UUID
            .fromString("00002a29-0000-1000-8000-00805f9b34fb");
    public static UUID MODEL_NUMBER_CHARACTERISTIC = UUID
            .fromString("00002a24-0000-1000-8000-00805f9b34fb");
    public static UUID SERIAL_NUMBER_CHARACTERISTIC = UUID
            .fromString("00002a25-0000-1000-8000-00805f9b34fb");
    public static UUID SOFTWARE_REVISION_CHARACTERISTIC = UUID
            .fromString("00002a28-0000-1000-8000-00805f9b34fb");
    public static UUID FIRMWARE_REVISION_CHARACTERISTIC = UUID
            .fromString("00002a26-0000-1000-8000-00805f9b34fb");
    public static UUID DEVICE_NAME_CHARACTERISTIC = UUID
            .fromString("00002a00-0000-1000-8000-00805f9b34fb");
    public static UUID APPERANCE_CHARACTERISTIC = UUID
            .fromString("00002a01-0000-1000-8000-00805f9b34fb");
    public static UUID NONIN_CONTROL_POINT_CHARACTERISTIC = UUID
            .fromString("1447af80-0d60-11e2-88b6-0002a5d5c51b");

    // Broadcast actions
    public static final String ACTION_GATT_CONNECTING = PACKAGE
            + ".bluetooth.le.ACTION_GATT_CONNECTING";
    public static final String ACTION_GATT_CONNECTED = PACKAGE
            + ".bluetooth.le.ACTION_GATT_CONNECTED";
    public static final String ACTION_GATT_DISCONNECTED = PACKAGE
            + ".bluetooth.le.ACTION_GATT_DISCONNECTED";
    public static final String ACTION_GATT_SERVICES_DISCOVERED = PACKAGE
            + ".bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public static final String ACTION_SERIAL_NUMBER_AVAILABLE = PACKAGE
            + ".bluetooth.le.ACTION_SERIAL_NUMBER_AVAILABLE";
    public static final String ACTION_MEASURE_AVAILABLE = PACKAGE
            + ".bluetooth.le.ACTION_MEASURE_AVAILABLE";

    // Intent extra keys
    public static final String EXTRAS_MEASURE_PULSE_RATE = PACKAGE
            + ".bluetooth.le.EXTRAS_MEASURE_PULSE_RATE";
    public static final String EXTRAS_MEASURE_SPO2 = PACKAGE
            + ".bluetooth.le.EXTRAS_MEASURE_SPO2";
    public static final String EXTRAS_DATA = PACKAGE
            + ".bluetooth.le.EXTRA_DATA";
    public static final String EXTRAS_DEVICE_NAME = PACKAGE + ".DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = PACKAGE
            + ".DEVICE_ADDRESS";

    // Commands
    public static final String CONTROL_POINT_MEASUREMENT_COMPLETE_PROCEDURE_COMMAND = "bNMI";


}

