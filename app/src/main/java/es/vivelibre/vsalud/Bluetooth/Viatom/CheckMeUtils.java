package es.vivelibre.vsalud.Bluetooth.Viatom;

import android.util.Log;

import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.CheckMeDevice;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.OxygenSaturationValue;
import es.vivelibre.models.Utils.DateFunctions;
import es.vivelibre.vsalud.R;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.viatom.azur.measurement.DailyCheckItem;
import com.viatom.azur.measurement.SPO2Item;

import org.json.JSONException;
import org.json.JSONObject;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 12/05/2016.
 */
public class CheckMeUtils {
    public static final String USR_FILE = "usr.dat";
    public static final String OXI_FILE = "oxi.dat";
    public static final String ECG_FILE = "ecg.dat";
    public static final String XDLC_FILE = "xdlc.dat";

    public static final int MESSAGE_STATE_SEARCHING_DEVICE = 1;
    public static final int MESSAGE_CONNECTING = 2;
    public static final int MESSAGE_CONNECTED = 3;
    public static final int MESSAGE_DEVICE_INFO = 4;
    public static final int MESSAGE_PING = 5;
    public static final int MESSAGE_ERROR = 6;
    public static final int MESSAGE_READING_FILE = 7;
    public static final int MESSAGE_READING_FILE_COMPLETE = 8;
    public static final int MESSAGE_SENDING_VALUES = 9;
    public static final int MESSAGE_SEND_VALUES_COMPLETE = 10;
    public static final int MESSAGE_NETWORK_CONNECTIVITY_ERROR = 11;
    public static final int MESSAGE_READING_USERS_COMPLETE = 12;
    public static final int MESSAGE_READING_DAILY_CHECK_FILES_COMPLETE = 13;

    public static final int SELECT_USER_REQUEST_CODE = R.string.viatom_checkme_select_user;

    public static MeterValue GetHeartRateResult(CheckMeDevice checkMeDevice,
                                                SPO2Item SPO2Item) {
        if (SPO2Item.getPr() == 0)
            return null;
        HeartRateValue heartRateValue = new HeartRateValue();
        heartRateValue.setMeterId(checkMeDevice.getSerialNumber());
        heartRateValue.setValue(String.valueOf(SPO2Item.getPr()));
        heartRateValue.setDateRead(SPO2Item.getDate());

        return heartRateValue;
    }

    public static HeartRateValue GetHeartRateResult(
            CheckMeDevice checkMeDevice, DailyCheckItem DailyCheckItem) {
        if (DailyCheckItem.getHR() == 0)
            return null;
        HeartRateValue heartRateValue = new HeartRateValue();
        heartRateValue.setMeterId(checkMeDevice.getSerialNumber());
        heartRateValue.setValue(String.valueOf(DailyCheckItem.getHR()));
        heartRateValue.setDateRead(DailyCheckItem.getDate());

        return heartRateValue;
    }

    public static MeterValue GetBloodOxygenSaturationResult(
            CheckMeDevice checkMeDevice, SPO2Item SPO2Item) {
        if (String.valueOf(SPO2Item.getOxygen()).equals("0"))
            return null;
        OxygenSaturationValue oxygenSaturationValue = new OxygenSaturationValue();
        oxygenSaturationValue.setMeterId(checkMeDevice.getSerialNumber());
        oxygenSaturationValue.setValue(String.valueOf(SPO2Item.getOxygen()));
        oxygenSaturationValue.setDateRead(SPO2Item.getDate());

        return oxygenSaturationValue;
    }

    public static OxygenSaturationValue GetBloodOxygenSaturationResult(
            CheckMeDevice checkMeDevice, DailyCheckItem DailyCheckItem) {
        if (String.valueOf(DailyCheckItem.getSPO2()).equals("0"))
            return null;
        OxygenSaturationValue oxygenSaturationValue = new OxygenSaturationValue();
        oxygenSaturationValue.setMeterId(checkMeDevice.getSerialNumber());
        oxygenSaturationValue.setValue(String.valueOf(DailyCheckItem
                .getSPO2()));
        oxygenSaturationValue.setDateRead(DailyCheckItem.getDate());

        return oxygenSaturationValue;
    }

    public static BloodPressureValue GetBloodPressureResult(
            CheckMeDevice checkMeDevice, DailyCheckItem DailyCheckItem) {
        if (String.valueOf(DailyCheckItem.getBPFlag()).equals("0")
                || String.valueOf(DailyCheckItem.getBPFlag()).equals("-1")
                || String.valueOf(DailyCheckItem.getBPFlag()).equals("255")
                || String.valueOf(DailyCheckItem.getBP()).equals("-1")
                || String.valueOf(DailyCheckItem.getBP()).equals("0")
                || String.valueOf(DailyCheckItem.getBP()).equals("255"))
            return null;
        BloodPressureValue bloodPressureValue = new BloodPressureValue();
        bloodPressureValue.setMeterId(checkMeDevice.getSerialNumber());
        bloodPressureValue.setValue(String.valueOf(
                DailyCheckItem.getBP()).concat("@-@-"));

        bloodPressureValue.setDateRead(DailyCheckItem.getDate());

        return bloodPressureValue;
    }

    public static CheckMeDevice GetCheckMeDevice(String checkMeInfo) {
        try {
            JSONObject jsonObject = new JSONObject(checkMeInfo);
            Gson gson = new GsonBuilder().setDateFormat(
                    DateFunctions.DATE_FORMAT).create();
            return gson.fromJson(jsonObject.toString(),
                    new CheckMeDevice().getClass());
        } catch (JSONException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "CheckMeUtils GetCheckMeDevice");
            FirebaseCrash.report(e);
            e.printStackTrace();
            return null;
        }
    }
}

