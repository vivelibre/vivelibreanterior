package es.vivelibre.vsalud.Bluetooth.nonin;

import java.util.UUID;

/**
 * Created by mfernandez on 11/05/2016.
 */
public class BluetoothNoninServiceUtils {
    public static final UUID SPP_UUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");

    public static final int MESSAGE_STATE_CHANGE = 1;

    public static final int MESSAGE_CONNECTION_ERROR = 2;

    public static final int MAX_CONNECT_ATTEMPS = 10;

    public static final int STATE_NONE = 0;

    public static final int STATE_DISCOVERING = 1;

    public static final int STATE_CONNECTING = 2;

    public static final int STATE_CONNECTED = 3;
}
