package es.vivelibre.vsalud.Bluetooth.and;

import java.util.Calendar;
import es.vivelibre.models.Meter.BloodPressureValue;
import es.vivelibre.models.Meter.HeartRateValue;
import es.vivelibre.models.Meter.WeightValue;
import es.vivelibre.vsalud.Functions.CommunicationPacket;
import es.vivelibre.vsalud.Functions.Sections.DataBloodPressureSection;
import es.vivelibre.vsalud.Functions.Sections.DataScaleSection;


/**
 * Created by mfernandez on 10/05/2016.
 */
public class ANDDevicesUtils {
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_SENDING_VALUES = 7;
    public static final int MESSAGE_SEND_VALUES_COMPLETE = 8;
    public static final int MESSAGE_PROCESS_INPUTS = 9;
    public static final int MESSAGE_ERROR = 10;
    public static final int MESSAGE_NETWORK_CONNECTIVITY_ERROR = 11;

    private static final String HEXES = "0123456789ABCDEF";

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    public static final int REQUEST_ENABLE_BT = 1;

    public static int DISCOVERABLE_DURATION = 300;

    // BATTERY STATUS
    public static final String BATTERY_GOOD_LEVEL = "F0";
    public static final String BATTERY_BAD_LEVEL = "39";

    // Convert a byte array to a hex string
    public static String getHex(byte[] raw, int len) {
        if (raw == null) {
            return null;
        }
        final StringBuilder hex = new StringBuilder(3 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F))).append(" ");
            if (--len == 0)
                break;
        }
        return hex.toString();
    }

    // BLOOD PRESSURE MONITOR
    public static final String BP_CORRECT_MEASUREMENT = "80";
    public static final String BP_ERROR_MEASUREMENT_1 = "81";
    public static final String BP_ERROR_MEASUREMENT_2 = "82";
    public static final String BP_CUFF_ERROR = "83";
    public static final String BP_MOVEMENT_AND_IHB = "84";
    public static final String BP_MOVEMENT_AND_IHB_AND_LOW_BATTERY = "85";
    public static final String BP_LOW_BATTERY_STATUS = "8A";
    public static final String BP_LOW_BATTERY_AND_IHB = "8B";
    public static final String BP_BODY_MOVEMENT_AND_LOW_BATTERY = "8C";
    public static final String BP_BODY_MOVEMENT = "8E";
    public static final String BP_IRREGULAR_HEART_BEAT = "8F";

    // SCALE
    public static final String SCALE_VALID_MEASUREMENT = "ST";
    public static final String SCALE_VALID_MEASUREMENT_AND_LOW_BATTERY = "SL";
    public static final String SCALE_INVALID_MEASUREMENT = "SE";

    public static HeartRateValue getHeartRateResult(
            CommunicationPacket communicationPacket) {
        if (((DataBloodPressureSection) communicationPacket.getDataSection())
                .isIsValidMeasure()) {
            HeartRateValue heartRateValue = new HeartRateValue();
            heartRateValue.setMeterId(communicationPacket.getHeaderSection()
                    .getSerialNumber());
            heartRateValue
                    .setValue(((DataBloodPressureSection) communicationPacket
                            .getDataSection()).getHearRateValue());
            heartRateValue.setDateRead(communicationPacket
                    .getHeaderSection().getDateMeasurement());

            return heartRateValue;
        }
        return null;
    }

    public static BloodPressureValue getBloodPressureResult(
            CommunicationPacket communicationPacket) {
        if (((DataBloodPressureSection) communicationPacket.getDataSection())
                .isIsValidMeasure()) {

            BloodPressureValue bloodPressureValue = new BloodPressureValue();
            bloodPressureValue.setMeterId(communicationPacket
                    .getHeaderSection().getSerialNumber());
            bloodPressureValue
                    .setValue(((DataBloodPressureSection) communicationPacket
                            .getDataSection()).getValueComplete());
            bloodPressureValue.setDateRead(communicationPacket
                    .getHeaderSection().getDateMeasurement());

            return bloodPressureValue;
        }
        return null;
    }

    public static WeightValue getWeightResult(
            CommunicationPacket communicationPacket) {
        if (((DataScaleSection) communicationPacket.getDataSection())
                .isValidMeasure()) {

            WeightValue weightValue = new WeightValue();
            weightValue.setMeterId(communicationPacket.getHeaderSection()
                    .getSerialNumber());
            weightValue.setValue(String
                    .valueOf(((DataScaleSection) communicationPacket
                            .getDataSection()).getWeight()));
            weightValue.setDateRead(communicationPacket.getHeaderSection()
                    .getDateMeasurement());

            return weightValue;
        }
        return null;
    }

    public static byte[] GetDate() {

        Calendar calendar = Calendar.getInstance();

        int year 	= calendar.get(Calendar.YEAR);
        int month 	= calendar.get(Calendar.MONTH)+1;
        int day 	= calendar.get(Calendar.DAY_OF_MONTH);
        int hour 	= calendar.get(Calendar.HOUR_OF_DAY);
        int min 	= calendar.get(Calendar.MINUTE);
        int sec 	= calendar.get(Calendar.SECOND);

        byte[] value = {
                (byte)(year & 0x0FF),	// year 2bit
                (byte)(year >> 8),		//
                (byte)month,			// month
                (byte)day,				// day
                (byte)hour,				// hour
                (byte)min,				// min
                (byte)sec				// sec
        };

        return value;
    }
}
