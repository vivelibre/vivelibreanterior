package es.vivelibre.vsalud.Bluetooth.nonin;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 11/05/2016.
 */
public class BluetoothNoninService {
    private final String TAG = getClass().getName();

    private BluetoothAdapter mBluetoothAdapter;
    private Handler mhandlerBluetooth;
    private boolean mAllowInsecureConnections;

    private int mState;

    public BluetoothDevice bluetoothDeviceConnected = null;

    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;

    public BluetoothNoninService(Handler handlerBluetooth) {
        this.mState = BluetoothNoninServiceUtils.STATE_NONE;
        this.mhandlerBluetooth = handlerBluetooth;
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.bluetoothDeviceConnected = null;
    }

    private synchronized void setState(int state) {
        Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        mhandlerBluetooth.obtainMessage(
                BluetoothNoninServiceUtils.MESSAGE_STATE_CHANGE, state, -1)
                .sendToTarget();
    }

    public synchronized int getState() {
        return mState;
    }

    public synchronized void start() {
        Log.d(TAG, "start");

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        bluetoothDeviceConnected = null;
        setState(BluetoothNoninServiceUtils.STATE_NONE);

    }
    public synchronized void connect(BluetoothDevice device, boolean secure) {
        Log.d(TAG, "connect to: " + device);

        if (mState == BluetoothNoninServiceUtils.STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mConnectThread = new ConnectThread(device, secure);
        mConnectThread.start();
        setState(BluetoothNoninServiceUtils.STATE_CONNECTING);
    }

    public synchronized void connected(BluetoothSocket socket,
                                       BluetoothDevice device, final String socketType) {
        Log.d(TAG, "connected, Socket Type:" + socketType);

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mConnectedThread = new ConnectedThread(socket, socketType);
        mConnectedThread.start();

        setState(BluetoothNoninServiceUtils.STATE_CONNECTED);
    }

    public synchronized void stop() {
        Log.d(TAG, "stop");

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        bluetoothDeviceConnected = null;
        setState(BluetoothNoninServiceUtils.STATE_NONE);
    }

    public boolean writePort(byte[] out) {
        ConnectedThread r;
        synchronized (this) {
            if (mState != BluetoothNoninServiceUtils.STATE_CONNECTED)
                return false;
            r = mConnectedThread;
        }
        return r.write(out);
    }

    public boolean writePort(char paramChar) {
        ConnectedThread r;
        synchronized (this) {
            if (mState != BluetoothNoninServiceUtils.STATE_CONNECTED)
                return false;
            r = mConnectedThread;
        }
        return r.write(paramChar);
    }

    public String readPort(String paramString, boolean isSetting) {
        ConnectedThread r;
        synchronized (this) {
            if (mState != BluetoothNoninServiceUtils.STATE_CONNECTED)
                return "";
            r = mConnectedThread;
        }
        return r.read(paramString, isSetting);
    }

    public byte[] readPort(int lenght) {
        ConnectedThread r;
        synchronized (this) {
            if (mState != BluetoothNoninServiceUtils.STATE_CONNECTED)
                return null;
            r = mConnectedThread;
        }
        return r.read(lenght);
    }

    public int readPort(byte[] frame) {
        ConnectedThread r;
        synchronized (this) {
            if (mState != BluetoothNoninServiceUtils.STATE_CONNECTED)
                return -1;
            r = mConnectedThread;
        }
        return r.read(frame);
    }

    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device, boolean secure) {
            mmDevice = device;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectThread");
            setName("ConnectThread");

            // pairDevice(mmDevice);

            mBluetoothAdapter.cancelDiscovery();
            bluetoothDeviceConnected = null;
            int port = 0;
            while ((bluetoothDeviceConnected == null)
                    && port < BluetoothNoninServiceUtils.MAX_CONNECT_ATTEMPS) {
                try {
                    mmSocket = getBluetoothSocket();
                    if (mmSocket != null)
                        mmSocket.connect();
                    bluetoothDeviceConnected = mmDevice;
                } catch (IOException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService run");
                    FirebaseCrash.report(e);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e1) {
                        FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService run");
                        FirebaseCrash.report(e1);
                        e1.printStackTrace();
                        try {
                            mmSocket.close();
                        } catch (IOException e2) {
                            FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService unable to close() socket during connection failure");
                            FirebaseCrash.report(e2);
                            Log.e(TAG,
                                    "unable to close() socket during connection failure",
                                    e2);
                            connectionFailed();
                            return;
                        }
                    }
                }
                port++;
            }

            if (bluetoothDeviceConnected != null) {
                synchronized (BluetoothNoninService.this) {
                    mConnectThread = null;
                }

                connected(mmSocket, mmDevice, "");
            } else {
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService unable to close() socket during connection failure");
                    FirebaseCrash.report(e2);
                    Log.e(TAG,
                            "unable to close() socket during connection failure",
                            e2);
                }
                connectionFailed();
            }
        }

        private BluetoothSocket getBluetoothSocket() {
            try {
                if (mAllowInsecureConnections) {
                    Method method;

                    method = mmDevice.getClass().getMethod(
                            "createInsecureRfcommSocket",
                            new Class[] { int.class });
                    return (BluetoothSocket) method.invoke(mmDevice, 1);
                } else {
                    return mmDevice
                            .createInsecureRfcommSocketToServiceRecord(BluetoothNoninServiceUtils.SPP_UUID);
                }
            } catch (Exception e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService create() failed");
                FirebaseCrash.report(e);
                Log.e(TAG, "create() failed", e);
            }

            // try {
            // return mmDevice
            // .createInsecureRfcommSocketToServiceRecord(BluetoothServiceUtils.SPP_UUID);
            // } catch (Exception e) {
            // // Log.e(TAG, "create() failed", e);
            // }
            return null;
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService close() of connect socket failed");
                FirebaseCrash.report(e);
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    private void connectionFailed() {
        bluetoothDeviceConnected = null;
        setState(BluetoothNoninServiceUtils.STATE_NONE);

        Message msg = mhandlerBluetooth
                .obtainMessage(BluetoothNoninServiceUtils.MESSAGE_CONNECTION_ERROR);
        mhandlerBluetooth.sendMessage(msg);

        BluetoothNoninService.this.start();
    }

    private void connectionLost() {
        bluetoothDeviceConnected = null;
        setState(BluetoothNoninServiceUtils.STATE_NONE);
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket, String socketType) {
            Log.d(TAG, "create ConnectedThread: " + socketType);
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                if (socket != null) {
                    tmpIn = socket.getInputStream();
                    tmpOut = socket.getOutputStream();
                }
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService temp sockets not created");
                FirebaseCrash.report(e);
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");
        }

        public boolean write(char paramChar) {
            try {
                Log.d(TAG, "DCS-------"
                        + ((paramChar == '\r') ? "CR"
                        : ((paramChar == '\n') ? "LF" : paramChar))
                        + "------>Meter");
                this.mmOutStream.write(paramChar);
                return true;
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService Error al escribir socket");
                FirebaseCrash.report(e);
                Log.e(TAG, "Error al escribir socket", e);
                connectionLost();
            }
            return false;
        }

        public boolean write(byte[] bytes) {
            try {
                this.mmOutStream.write(bytes);
                return true;
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService Error al escribir socket");
                FirebaseCrash.report(e);
                Log.e(TAG, "Error al escribir socket", e);
                connectionLost();
            }
            return false;
        }

        public String read(String paramString, boolean isSetting) {
            Log.i(TAG, "INICIO lectura del puerto");
            byte[] arrayOfByte = new byte[1];
            Object localObject = "";

            while (true) {
                if (isSetting
                        && (((String) localObject).equals("F")
                        || ((String) localObject).equals("P")
                        || ((String) localObject).equals("\r\nF") || ((String) localObject)
                        .equals("\r\nP"))) {
                    return ((String) localObject);
                } else if (((String) localObject).contains(paramString)) {
                    return ((String) localObject);
                } else {
                    try {
                        this.mmInStream.read(arrayOfByte);
                        String str1 = new String(arrayOfByte);
                        String str2 = localObject + str1;
                        localObject = str2;
                    } catch (IOException localIOException) {
                        FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService INICIO lectura del puerto");
                        FirebaseCrash.report(localIOException);
                        localIOException.printStackTrace();
                        connectionLost();
                        return "";
                    }
                }
            }
        }

        public byte[] read(int lenght) {
            byte[] arrayOfByte = new byte[lenght];
            try {
                this.mmInStream.read(arrayOfByte);
                return arrayOfByte;
            } catch (IOException localIOException) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService read");
                FirebaseCrash.report(localIOException);
                localIOException.printStackTrace();
                connectionLost();
            }
            return null;
        }

        public int read(byte[] frame) {
            try {
                return mmInStream.read(frame);
            } catch (IOException localIOException) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService read");
                FirebaseCrash.report(localIOException);
                localIOException.printStackTrace();
                connectionLost();
            }
            return -1;
        }

        public void cancel() {
            try {
                mmInStream.close();
                mmOutStream.close();
                mmSocket.close();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothNoninService cancel");
                FirebaseCrash.report(e);
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    public void setAllowInsecureConnections(boolean allowInsecureConnections) {
        mAllowInsecureConnections = allowInsecureConnections;
    }

    public boolean getAllowInsecureConnections() {
        return mAllowInsecureConnections;
    }

    public void disableBluetoothAdapter() {
        if (mBluetoothAdapter != null)
            mBluetoothAdapter.disable();
        mBluetoothAdapter = null;
    }

    public BluetoothDevice getBluetoothDeviceConnected() {
        return bluetoothDeviceConnected;
    }
}
