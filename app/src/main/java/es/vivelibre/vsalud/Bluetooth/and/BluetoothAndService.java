package es.vivelibre.vsalud.Bluetooth.and;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;
import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class BluetoothAndService {
    private static final String NAME = "PWAccessP";
    private static final UUID MY_UUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private int mPort = -1;

    private final BluetoothAdapter mAdapter;
    private final BluetoothDevice mDevice;
    private final Handler mHandler;
    private AcceptThread mAcceptThread;
    private ConnectedThread mConnectedThread;
    private int mState;
    private BluetoothServerSocket mServerSocket;

    public static final int STATE_NONE = 0;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECTED = 3;

    /**
     * Constructor. Prepares a new BluetoothChat session.
     *
     * @param context
     *            The UI Activity Context
     * @param handler
     *            A Handler to send messages back to the UI Activity
     */
    public BluetoothAndService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mDevice = mAdapter.getRemoteDevice("BC:47:60:FE:FD:65");
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of the chat connection
     *
     * @param state
     *            An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        mState = state;

        mHandler.obtainMessage(ANDDevicesUtils.MESSAGE_STATE_CHANGE, state, -1)
                .sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start() {
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread(true, mAdapter, mDevice);
            mAcceptThread.start();
        }
        setState(STATE_LISTEN);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket
     *            The BluetoothSocket on which the connection was made
     * @param device
     *            The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket,
                                       BluetoothDevice device) {

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }

        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        Message msg = mHandler
                .obtainMessage(ANDDevicesUtils.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(ANDDevicesUtils.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }
        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out
     *            The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        ConnectedThread r;
        synchronized (this) {
            if (mState != STATE_CONNECTED)
                return;
            r = mConnectedThread;
        }
        r.write(out);
    }

    private int getPortNr() {
        try {
            Field mSocketField = BluetoothServerSocket.class
                    .getDeclaredField("mSocket");
            mSocketField.setAccessible(true);
            BluetoothSocket socket = (BluetoothSocket) mSocketField
                    .get(mServerSocket);
            mSocketField.setAccessible(false);

            Field mPortField = BluetoothSocket.class.getDeclaredField("mPort");
            mPortField.setAccessible(true);
            int port = (Integer) mPortField.get(socket);
            mPortField.setAccessible(false);

            return port;
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService getPortNr");
            FirebaseCrash.report(e);
            return -1;
        }
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        setState(STATE_LISTEN);

        Message msg = mHandler
                .obtainMessage(ANDDevicesUtils.MESSAGE_CONNECTION_LOST);
        Bundle bundle = new Bundle();
        bundle.putString(ANDDevicesUtils.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted (or
     * until cancelled).
     */
    @SuppressLint("NewApi")
    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread(boolean secure, BluetoothAdapter adapter,
                            BluetoothDevice device) {
            BluetoothServerSocket tmp = null;

            if (mPort == -1) {
                try {
                    tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(
                            NAME, MY_UUID);
                    // tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME,
                    // MY_UUID);
                    // tmp =
                    // mAdapter.getRemoteDevice("BC:47:60:FE:FD:65").createRfcommSocketToServiceRecord(MY_UUID);
                    // mServerSocket = tmp;
                    mPort = getPortNr();

                } catch (IOException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService AcceptThread");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
            } else {

                Method m;
                try {
                    m = mAdapter.getClass().getMethod(
                            "listenUsingInsecureRfcommOn",
                            new Class[] { int.class });
                    tmp = (BluetoothServerSocket) m.invoke(mAdapter, mPort);
                } catch (NoSuchMethodException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService AcceptThread");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService AcceptThread");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService AcceptThread");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService AcceptThread");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
            }
            mmServerSocket = tmp;
        }

        @Override
        public void run() {
            setName("AcceptThread");
            BluetoothSocket socket = null;

            while (mState != STATE_CONNECTED) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService run");
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                    break;
                }
                if (socket == null) {

                }

                if (socket != null) {
                    cancel();

                    synchronized (BluetoothAndService.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                connected(socket, socket.getRemoteDevice());
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                try {
                                    mServerSocket = mmServerSocket;
                                    socket.close();
                                } catch (IOException e) {
                                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService Close");
                                    FirebaseCrash.report(e);
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }
                }
            }
        }

        public void cancel() {
            try {
                mServerSocket = mmServerSocket;
                mmServerSocket.close();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService Cancel");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device. It handles all
     * incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService ConnectedThread");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

        }

        @Override
        public void run() {
            while (true) {
                try {
                    byte[] buffer = new byte[1024];
                    int bytes;

                    bytes = mmInStream.read(buffer);
                    mHandler.obtainMessage(ANDDevicesUtils.MESSAGE_READ, bytes,
                            -1, buffer).sendToTarget();
                } catch (IOException e) {
                    FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService run");
                    FirebaseCrash.report(e);
                    connectionLost();
                    break;
                }
            }
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer
         *            The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
                mHandler.obtainMessage(ANDDevicesUtils.MESSAGE_WRITE, -1, -1,
                        buffer).sendToTarget();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService write");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "BluetoothAndService Cancel");
                FirebaseCrash.report(e);
                e.printStackTrace();
            }
        }
    }



}
