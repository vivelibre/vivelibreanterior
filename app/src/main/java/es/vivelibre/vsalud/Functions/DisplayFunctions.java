package es.vivelibre.vsalud.Functions;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 25/04/2016.
 */
public class DisplayFunctions {
    private static ProgressDialog progressDialog;

    public static void showToast(Context context, String message) {
        CharSequence text = message;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public static void showProgressDialog(final Activity activity,
                                          String title, String message, final boolean finishActivity) {
        try {

            progressDialog = new ProgressDialog(activity);
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setButton("Cancelar", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which)
                {
                    // Use either finish() or return() to either close the activity or just the dialog
                    onBackButtonPressed(activity, finishActivity);
                    return;
                }
            });

            if (progressDialog != null && progressDialog.isShowing() )
                CancelDialog();
            if (progressDialog == null) {
                progressDialog = ProgressDialog.show(activity, title, message, true);
            }
            progressDialog.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode,
                                     KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && !event.isCanceled()) {
                        onBackButtonPressed(activity, finishActivity);
                    }
                    return false;
                }
            });

            progressDialog.show();

        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "DisplayFunction showProgressDialog");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }
    }

    public static void changeTextProgressDialog(final Activity activity,
                                                String title, String message, final boolean finishActivity) {
        if (progressDialog != null) {
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
        } else
            showProgressDialog(activity, title, message, finishActivity);
    }

    protected static void onBackButtonPressed(Activity activity,
                                              boolean finishActivity) {
        if (progressDialog.isShowing()) {
            CancelDialog();
            if (finishActivity) {
                activity.finish();
            }
        }
    }

    public static void CancelDialog() {
        if (progressDialog != null) {
            progressDialog.cancel();
        }
    }
}

