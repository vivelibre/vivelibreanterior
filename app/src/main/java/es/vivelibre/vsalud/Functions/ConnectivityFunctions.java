package es.vivelibre.vsalud.Functions;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import es.vivelibre.models.Utils.LogFunction;
import es.vivelibre.vsalud.R;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class ConnectivityFunctions {

    private static String TAG = "ConnectivityFunctions";

    public static boolean networkAvailable(Context context) {
        ConnectivityManager connectMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectMgr != null) {
            NetworkInfo[] netInfo = connectMgr.getAllNetworkInfo();
            if (netInfo != null) {
                for (NetworkInfo net : netInfo) {
                    if (net.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        } else {
            LogFunction.writeLogCat(TAG, LogFunction.DEBUG_LOG,
                    context.getString(R.string.log_connectivity_not_available));
        }
        return false;
    }
}