package es.vivelibre.vsalud.Functions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import android.util.Pair;

import es.vivelibre.vsalud.Functions.Sections.DataBloodPressureSection;
import es.vivelibre.vsalud.Functions.Sections.DataScaleSection;
import es.vivelibre.vsalud.Functions.Sections.DataSection;
import es.vivelibre.vsalud.Functions.Sections.HeaderSection;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class CommunicationPacket {

    private HeaderSection HeaderSection;
    private DataSection DataSection;

    public CommunicationPacket(ByteArrayInputStream pdu) {
        setHeaderSection(new HeaderSection(pdu));
        if (isBloodPressureMonitor(getHeaderSection().getDeviceType())) {
            setDataSection(new DataBloodPressureSection(pdu));
        } else if (isScaleMonitor(getHeaderSection().getDeviceType())) {
            setDataSection(new DataScaleSection(pdu));
        } else {
            setDataSection(null);
        }
    }

    public HeaderSection getHeaderSection() {
        return HeaderSection;
    }

    public void setHeaderSection(HeaderSection headerSection) {
        HeaderSection = headerSection;
    }

    public DataSection getDataSection() {
        return DataSection;
    }

    public void setDataSection(DataSection dataSection) {
        DataSection = dataSection;
    }

    private boolean isValidMeassurement() {
        if (getDataSection() == null)
            return false;
        return (getDataSection() instanceof DataBloodPressureSection) ? ((DataBloodPressureSection) getDataSection())
                .isIsValidMeasure() : ((DataScaleSection) getDataSection())
                .isValidMeasure();
    }

    public boolean isCorrectInput() {
        return getHeaderSection() != null && getDataSection() != null
                && isValidMeassurement();
    }

    public static Pair<Boolean, Boolean> checkPacket(ByteArrayInputStream pdu,
                                                     ByteArrayOutputStream mPacket) {
        boolean ok = true;
        boolean done = false;
        int pktLen = 0;

        int pktType = pdu.read() + (pdu.read() << 8);

        if (pktType != 2) {
            ok = false;
        }

        if (ok) {
            pktLen = pdu.read() + (pdu.read() << 8) + (pdu.read() << 16)
                    + (pdu.read() << 24);

            if ((pktLen + 60) == mPacket.size()) {
                done = true;
            } else if ((pktLen + 60) < mPacket.size()) {
                ok = false;
            }
        }
        return new Pair<Boolean, Boolean>(ok, done);
    }

    private boolean isScaleMonitor(int devType) {
        return devType == 321 || devType == 322 || devType == 323;
    }

    private boolean isBloodPressureMonitor(int devType) {
        return devType == 766 || devType == 767 || devType == 768;
    }

    public boolean isPBTMonitor() {
        return getHeaderSection().getDeviceType() == 767;
    }

    public boolean isLowBattery() {
        double batteryVoltage = -1;
        if (isBloodPressureMonitor(HeaderSection.getDeviceType())) {
            batteryVoltage = getBloodPressureMonitorBatteryLevel();
        } else if (isScaleMonitor(HeaderSection.getDeviceType())) {
            batteryVoltage = getScaleMonitorBatteryLevel();
        }
        if (batteryVoltage != -1)
            return batteryVoltage < 4.6;
        return false;
    }

    private double getBloodPressureMonitorBatteryLevel() {
        return (HeaderSection.getDeviceBatteryStatus() * 0.03) + 2.3;
    }

    private double getScaleMonitorBatteryLevel() {
        return (HeaderSection.getDeviceBatteryStatus() * 0.02) + 1.9;
    }
}
