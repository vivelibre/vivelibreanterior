package es.vivelibre.vsalud.Functions.Sections;

import java.io.ByteArrayInputStream;
import java.util.Date;

import es.vivelibre.models.Utils.DateFunctions;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class HeaderSection {
    private int PacketType;
    private int PacketLenght;
    private int DeviceType;
    private String Flag;
    private Date DateMeasurement;
    private Date DateTransmission;
    private String BluetoothId;
    private String DeviceUpperName;
    private String SerialNumber;
    private String DeviceLowerName;
    private int DeviceBatteryStatus;
    private String Reserved;
    private String DeviceFirmware;

    public HeaderSection(ByteArrayInputStream pdu) {
        setDeviceType(pdu.read() + (pdu.read() << 8));
        pdu.skip(1); // Skip the flag
        setDateMeasurement(getDateFromByteArray(pdu));
        setDateTransmission(getDateFromByteArray(pdu));
        pdu.skip(6); // Skip the bdaddr
        pdu.skip(6); // Skip the bdaddr
        setSerialNumber(getSerialNumberFromByteArray(pdu));
        pdu.skip(10); // Skip reserved
        setDeviceBatteryStatus(pdu.read());
        pdu.skip(1); // Skip reserved
        pdu.skip(1); // Skip firmware
    }

    public int getPacketType() {
        return PacketType;
    }

    public void setPacketType(int packetType) {
        PacketType = packetType;
    }

    public int getPacketLenght() {
        return PacketLenght;
    }

    public void setPacketLenght(int packetLenght) {
        PacketLenght = packetLenght;
    }

    public int getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(int deviceType) {
        DeviceType = deviceType;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public Date getDateMeasurement() {
        return DateMeasurement;
    }

    public void setDateMeasurement(Date dateMeasurement) {
        DateMeasurement = dateMeasurement;
    }

    public Date getDateTransmission() {
        return DateTransmission;
    }

    public void setDateTransmission(Date dateTransmission) {
        DateTransmission = dateTransmission;
    }

    public String getBluetoothId() {
        return BluetoothId;
    }

    public void setBluetoothId(String bluetoothId) {
        BluetoothId = bluetoothId;
    }

    public String getDeviceUpperName() {
        return DeviceUpperName;
    }

    public void setDeviceUpperName(String deviceUpperName) {
        DeviceUpperName = deviceUpperName;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getDeviceLowerName() {
        return DeviceLowerName;
    }

    public void setDeviceLowerName(String deviceLowerName) {
        DeviceLowerName = deviceLowerName;
    }

    public int getDeviceBatteryStatus() {
        return DeviceBatteryStatus;
    }

    public void setDeviceBatteryStatus(int deviceBatteryStatus) {
        DeviceBatteryStatus = deviceBatteryStatus;
    }

    public String getReserved() {
        return Reserved;
    }

    public void setReserved(String reserved) {
        Reserved = reserved;
    }

    public String getDeviceFirmware() {
        return DeviceFirmware;
    }

    public void setDeviceFirmware(String deviceFirmware) {
        DeviceFirmware = deviceFirmware;
    }

    private Date getDateFromByteArray(ByteArrayInputStream pdu) {
        int mYearMeasure = pdu.read() + (pdu.read() << 8);
        int mMonthMeasure = pdu.read();
        int mDayMeasure = pdu.read();
        int mHourMeasure = pdu.read();
        int mMinuteMeasure = pdu.read();
        int mSecondMeasure = pdu.read();
        return DateFunctions.ParamsToDate(mYearMeasure, mMonthMeasure,
                mDayMeasure, mHourMeasure, mMinuteMeasure, mSecondMeasure);
    }

    private String getSerialNumberFromByteArray(ByteArrayInputStream pdu) {
        byte sn[] = new byte[16];
        pdu.read(sn, 0, 12);
        return new String(sn, 0, 11);
    }

}
