package es.vivelibre.vsalud.Functions;

import java.util.Locale;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class MathFunctions {
    private static String digits = "0123456789ABCDEF";

    public static int hex2decimal(String s) {
        s = s.toUpperCase(Locale.US);
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }

    public static String decimal2hex(int d) {
        if (d == 0)
            return "0";
        String hex = "";
        while (d > 0) {
            int digit = d % 16;
            hex = digits.charAt(digit) + hex;
            d = d / 16;
        }
        return hex;
    }
}
