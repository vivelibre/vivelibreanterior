package es.vivelibre.vsalud.Functions.Sections;

import java.io.ByteArrayInputStream;

import es.vivelibre.vsalud.Bluetooth.and.ANDDevicesUtils;
import es.vivelibre.vsalud.Functions.MathFunctions;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class DataBloodPressureSection extends DataSection {

    private String Flag;
    private int SystolicValue;
    private int DiastolicValue;
    private int MPAValue;
    private String HearRateValue;
    private boolean IsValidMeasure;
    private boolean IsErrorHeartRate;

    public DataBloodPressureSection(ByteArrayInputStream pdu) {
        byte reading[] = new byte[16];
        String strReading = null;
        pdu.read(reading, 0, 10);
        strReading = new String(reading, 0, 10);

        setIsValidMeasure(strReading.substring(0, 2).equals(
                ANDDevicesUtils.BP_CORRECT_MEASUREMENT)
                || strReading.substring(0, 2).equals(ANDDevicesUtils.BP_IRREGULAR_HEART_BEAT));

        if (isIsValidMeasure()) {
            setFlag(strReading.substring(0, 2));
            setSystolicValue(MathFunctions.hex2decimal(strReading.substring(2,
                    4)));
            setDiastolicValue(MathFunctions.hex2decimal(strReading.substring(4,
                    6)));
            setMPAValue(MathFunctions.hex2decimal(strReading.substring(8, 10)));

            setIsErrorHeartRate(strReading.substring(0, 2).equals("8F"));
            if (!isIsErrorHeartRate()) {
                setHearRateValue(String.valueOf(MathFunctions
                        .hex2decimal(strReading.substring(6, 8))));
            } else {
                setHearRateValue(null);
            }
        }
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public int getSystolicValue() {
        return SystolicValue;
    }

    public void setSystolicValue(int systolicValue) {
        SystolicValue = systolicValue;
    }

    public int getDiastolicValue() {
        return DiastolicValue;
    }

    public void setDiastolicValue(int diastolicValue) {
        DiastolicValue = diastolicValue;
    }

    public int getMPAValue() {
        return MPAValue;
    }

    public void setMPAValue(int mPAValue) {
        MPAValue = mPAValue;
    }

    public String getHearRateValue() {
        return HearRateValue;
    }

    public void setHearRateValue(String hearRateValue) {
        HearRateValue = hearRateValue;
    }

    public boolean isIsValidMeasure() {
        return IsValidMeasure;
    }

    public void setIsValidMeasure(boolean isValidMeasure) {
        IsValidMeasure = isValidMeasure;
    }

    public boolean isIsErrorHeartRate() {
        return IsErrorHeartRate;
    }

    public void setIsErrorHeartRate(boolean isErrorHeartRate) {
        IsErrorHeartRate = isErrorHeartRate;
    }

    public String getValueComplete() {
        return (getSystolicValue() + getDiastolicValue()) + "@"
                + getDiastolicValue() + "@" + getMPAValue();
    }

}
