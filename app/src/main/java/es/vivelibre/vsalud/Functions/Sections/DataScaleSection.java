package es.vivelibre.vsalud.Functions.Sections;

import java.io.ByteArrayInputStream;

import es.vivelibre.vsalud.Bluetooth.and.ANDDevicesUtils;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class DataScaleSection extends DataSection {
    private String Flag;
    private double Weight;
    private String Unit;
    private boolean IsValidMeasure;

    public DataScaleSection(ByteArrayInputStream pdu) {
        byte reading[] = new byte[16];
        String strReading = null;
        pdu.read(reading, 0, 14);
        strReading = new String(reading, 0, 13);

        setValidMeasure(!strReading.substring(0, 2).equals(
                ANDDevicesUtils.SCALE_INVALID_MEASUREMENT));

        if (isValidMeasure()) {
            setFlag(strReading.substring(0, 2));
            setWeight(Double.parseDouble(strReading.substring(3, 10)));
            setUnit(strReading.substring(10));
        }
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public double getWeight() {
        return Weight;
    }

    public void setWeight(double weight) {
        Weight = weight;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public boolean isValidMeasure() {
        return IsValidMeasure;
    }

    public void setValidMeasure(boolean isValidMeasure) {
        this.IsValidMeasure = isValidMeasure;
    }

}
