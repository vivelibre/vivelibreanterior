package es.vivelibre.vsalud.Controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.services.Meter.MeterValueAppRestServiceOauth;
import es.vivelibre.services.MonitorParameters.MonitorParametersAppService;
import es.vivelibre.services.Profile.ImageProfileAppService;
import es.vivelibre.services.Profile.ProfileAppService;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 27/04/2016.
 */
/*
Controlador del Home simplemente hacemos referencia a los servicios que vamos a consumir
y sus correspondientes usos
*/
public class HomeController  {

    private static ProfileAppService _profileAppService = ProfileAppService
            .getInstance();
    private ImageProfileAppService _imageProfileAppService = ImageProfileAppService
            .getInstance();
    private static MeterValueAppRestServiceOauth _meterValueAppRestServiceOauth = MeterValueAppRestServiceOauth
            .getInstance();
    private static MonitorParametersAppService _monitorParametersAppService = MonitorParametersAppService
            .getInstance();
    private static HomeController INSTANCE = new HomeController();

    public static HomeController getInstance(){
        return INSTANCE;
    }
       /**
     * Get client monitor parameters
     *
     * @return Monitor parameters representation if exist or null if not exist.
     */

    public MonitorParameters GetMonitorParameters(Context context) {
        try {
            Log.i("Home Controller", "GetProfile");
            return _monitorParametersAppService.GetMonitorParameters(context);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "HomeController GetMonitorParameters");
            FirebaseCrash.report(e);
            Log.e("Home Controller", "GetMonitorParameters");
            e.printStackTrace();

        }
        return null;
    }

    public Profile GetProfile(Context context){
        try{
            Log.i("Home Controller", "GetProfile");
            return _profileAppService.GetProfile(context);
        }catch (Exception e){
            FirebaseCrash.logcat(Log.ERROR, TAG, "HomeController GetProfile");
            FirebaseCrash.report(e);
            Log.e("Home Controller", "GetProfile");
            e.printStackTrace();
        }
        return null;
    }
    public ImageProfile GetImageProfile (Context context){
        try {
            Log.i("Home Controller", "GetImageProfile");
            return _imageProfileAppService.GetImageProfile(context);
        }catch (Exception e){
        FirebaseCrash.logcat(Log.ERROR, TAG, "HomeController GetImageProfile");
        FirebaseCrash.report(e);
        Log.e("Home Controller", "GetImageProfile");
        e.printStackTrace();
        }
    return null;

    }
    public Bitmap getContactPhotoFromBase64(String photo) {
        byte[] decodedString = Base64.decode(photo, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
                decodedString.length);
        return decodedByte;

    }




}
