package es.vivelibre.vsalud.Controllers;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.services.Meter.MeterValueAppRestServiceOauth;
import es.vivelibre.services.Meter.MeterValueAppService;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 13/05/2016.
 */
public class MeterValuesController {
    private static MeterValueAppService _meterValueAppService = MeterValueAppService
            .getInstance();
    private static MeterValueAppRestServiceOauth _meterValueAppRestServiceOauth = MeterValueAppRestServiceOauth
            .getInstance();

    private static MeterValuesController INSTANCE = new MeterValuesController();

    public static MeterValuesController getInstance() {
        return INSTANCE;
    }

    /**
     * Get meter values
     *
     * @return A collection of meter values representation.
     */
    public ArrayList<MeterValue> GetMeterValues(Context context,
                                                MeterValue meterValue) {
        try {
            Log.i("MeterValuesController", "GetMeterValues");
            return _meterValueAppService.GetMeterValues(context, meterValue);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValuesController GetMeterValues");
            FirebaseCrash.report(e);
            Log.e("MeterValuesController", "GetMeterValues");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get meter values from server
     *
     * @return A collection of meter values representation.
     */
    public ArrayList<MeterValue> GetMeterValuesFromServer(Context context,
                                                          MeterValueComplete meterValueDTO, Date dateToFind) {
        try {
            Log.i("MeterValuesController", "GetMeterValuesFromServer");
            ArrayList<MeterValueComplete> meterValuesComplete = _meterValueAppRestServiceOauth
                    .GetMeterValues(context, meterValueDTO, dateToFind);
            ArrayList<MeterValue> meterValues = new ArrayList<MeterValue>();
            if (meterValuesComplete != null
                    && meterValuesComplete.size() > 0) {
                Iterator<MeterValueComplete> it = meterValuesComplete
                        .iterator();
                while (it.hasNext()) {
                    MeterValueComplete meterValueComplete = it.next();
                    if (meterValueComplete != null
                            && meterValueComplete.getMeterValue() != null) {
                        meterValues.add(meterValueComplete.getMeterValue());
                    }
                }
            }
            return meterValues;
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValuesController GetMeterValuesFromServer");
            FirebaseCrash.report(e);
            Log.e("MeterValuesController", "GetMeterValuesFromServer");
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<MeterValue> GetMeterValuesByTypeFromServer(Context context, String type,
                                                                MeterValueComplete meterValueDTO, Date dateIni , Date dateFin) {
        try {
            Log.i("MeterValuesController", "GetMeterValuesByTypeFromServer");
            ArrayList<MeterValueComplete> meterValuesComplete = _meterValueAppRestServiceOauth
                    .GetMeterValues(context,type, meterValueDTO, dateIni, dateFin);
            ArrayList<MeterValue> meterValues = new ArrayList<MeterValue>();
            if (meterValuesComplete != null
                    && meterValuesComplete.size() > 0) {
                Iterator<MeterValueComplete> it = meterValuesComplete
                        .iterator();
                while (it.hasNext()) {
                    MeterValueComplete meterValueComplete = it.next();
                    if (meterValueComplete != null
                            && meterValueComplete.getMeterValue() != null) {
                        String value = meterValueComplete.getMeterValue().getValue();
                        meterValues.add(meterValueComplete.getMeterValue());
                    }
                }
            }
            return meterValues;

        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValuesController GetMeterValuesByTypeFromServer");
            FirebaseCrash.report(e);
            Log.e("MeterValuesController", "GetMeterValuesByTypeFromServer");
            e.printStackTrace();
        }
        return null;

    }

    public ArrayList<MeterValue> GetMeterValuesFromServer(Context context,
                                                          MeterValueComplete meterValueDTO, Date dateIni , Date dateFin) {
        try {
            Log.i("MeterValuesController", "GetMeterValuesFromServer");
            ArrayList<MeterValueComplete> meterValuesComplete = _meterValueAppRestServiceOauth
                    .GetMeterValues(context, meterValueDTO, dateIni, dateFin);
            ArrayList<MeterValue> meterValues = new ArrayList<MeterValue>();
            if (meterValuesComplete != null
                    && meterValuesComplete.size() > 0) {
                Iterator<MeterValueComplete> it = meterValuesComplete
                        .iterator();
                while (it.hasNext()) {
                    MeterValueComplete meterValueComplete = it.next();
                    if (meterValueComplete != null
                            && meterValueComplete.getMeterValue() != null) {
                        meterValues.add(meterValueComplete.getMeterValue());
                    }
                }
            }
            return meterValues;
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValuesController GetMeterValuesFromServer");
            FirebaseCrash.report(e);
            Log.e("MeterValuesController", "GetMeterValuesFromServer");
            e.printStackTrace();
        }
        return null;
    }
}
