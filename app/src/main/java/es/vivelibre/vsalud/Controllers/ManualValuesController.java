package es.vivelibre.vsalud.Controllers;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.services.MonitorParameters.MonitorParametersAppService;
import es.vivelibre.services.Profile.ProfileAppService;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 24/05/2016.
 */
public class ManualValuesController {

    private static MonitorParametersAppService _monitorParametersAppService = MonitorParametersAppService
            .getInstance();
    private static ProfileAppService _profileAppService = ProfileAppService
            .getInstance();

    private static ManualValuesController INSTANCE = new ManualValuesController();

    public static ManualValuesController getInstance(){
        return INSTANCE;
    }

    public MonitorParameters GetMonitorParameters(Context context) {
        try {
            Log.i("ManualValuesController", "GetMonitorParameters");
            return _monitorParametersAppService.GetMonitorParameters(context);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "ManualValuesController GetMonitorParameters");
            FirebaseCrash.report(e);
            Log.e("ManualValuesController", "GetMonitorParameters");
            e.printStackTrace();
        }
        return null;
    }

    public Profile GetProfile(Context context){
        try{
            Log.i("ManualValuesController", "GetProfile");
            return _profileAppService.GetProfile(context);
        }catch (Exception e){
            FirebaseCrash.logcat(Log.ERROR, TAG, "ManualValuesController GetProfile");
            FirebaseCrash.report(e);
            Log.e("ManualValuesController", "GetProfile");
            e.printStackTrace();
        }
        return null;
    }
}
