package es.vivelibre.vsalud.Controllers;

import android.content.Context;
import android.util.Log;

import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.models.Utils.SharedCustomPreferencesUtils;
import es.vivelibre.services.Login.LoginAppRestServiceOauth;
import es.vivelibre.services.Profile.ImageProfileAppService;
import es.vivelibre.services.Profile.ProfileAppRestServiceOauth;
import es.vivelibre.services.Profile.ProfileAppService;

/**
 * Created by mfernandez on 22/04/2016.
 */
public class LoginController {
    private ProfileAppService _profileAppService = ProfileAppService
            .getInstance();
    private LoginAppRestServiceOauth _loginRestServiceOauth = LoginAppRestServiceOauth
            .getInstance();
    private ProfileAppRestServiceOauth _profileAppRestServiceOauth = ProfileAppRestServiceOauth
            .getInstance();
    private ImageProfileAppService _imageProfileAppService = ImageProfileAppService
            .getInstance();

    private static LoginController INSTANCE = new LoginController();

    public static LoginController getInstance() {
        return INSTANCE;
    }

    /**
     * Performs the login operation.
     *
     * @return True if login successful.
     */
    public boolean isLoginProcessSuccessful(Context context, String username,
                                            String password) throws Exception {
        return _loginRestServiceOauth.Login(context, username, password);
    }

    /**
     * Get profile from SQLite
     *
     * @return Profile representation if exist or null if not exist.
     */
    public Profile getProfile(Context context) throws Exception {
        return _profileAppRestServiceOauth.GetProfile(context);
    }

    /**
     * Store profile in SQLite
     *
     */
    public void storeProfile(Context context, Profile profile)
            throws Exception {

        ImageProfile imageProfileAux = profile.getImageProfile();

        if (imageProfileAux != null){
            _imageProfileAppService
                    .CreateImageProfile(context, imageProfileAux);
            Log.i("TAG","store ImageProfile Correcto");
        }
        if (profile != null){
            _profileAppService.CreateProfile(context, profile);
            Log.i("TAG","store Profile Correcto");
        }
    }

    /**
     * Check if the user is logged.
     *
     * @return Return true if the user is logged.
     */
    public boolean isLogged(Context context) {
        return !SharedCustomPreferencesUtils.getRefreshToken(context).isEmpty();
    }

    /**
     * Check if the application is locked by security code.
     *
     * @return Return true if the application is locked by security code.
     */
    public boolean isLocked(Context context) {
        return !SharedCustomPreferencesUtils.getSecurityLock(context).isEmpty();
    }
}
