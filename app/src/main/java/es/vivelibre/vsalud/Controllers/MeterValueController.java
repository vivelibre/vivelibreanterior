package es.vivelibre.vsalud.Controllers;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.util.ArrayList;
import java.util.Iterator;

import es.vivelibre.models.Meter.MeterValue;
import es.vivelibre.models.Meter.MeterValueComplete;
import es.vivelibre.models.MonitorParameters.MonitorParameters;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.services.Meter.MeterValueAppRestServiceOauth;
import es.vivelibre.services.Meter.MeterValueAppService;
import es.vivelibre.services.MonitorParameters.MonitorParametersAppService;
import es.vivelibre.services.Profile.ProfileAppService;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 10/05/2016.
 */
public class MeterValueController {

    private static ProfileAppService _profileAppService = ProfileAppService
            .getInstance();
    private static MeterValueAppRestServiceOauth _meterValueAppRestServiceOauth = MeterValueAppRestServiceOauth
            .getInstance();
    private static MonitorParametersAppService _monitorParametersAppService = MonitorParametersAppService
            .getInstance();
    private static MeterValueAppService _meterValueAppService = MeterValueAppService
            .getInstance();

    private static MeterValueController INSTANCE = new MeterValueController();

    public static MeterValueController getInstance() {
        return INSTANCE;
    }

    /**
     * Add new meter value
     *
     * @return Meter value representation if added or null if not added.
     */
    public MeterValue AddNewMeterValue(Context context,
                                       MeterValue meterValue) {
        try {
            Log.i("MeterValueController", "AddNewMeterValue");
            Profile profile = _profileAppService.GetProfile(context);
            if (profile != null) {
                meterValue.setClient_id(profile.getId());
                MeterValueComplete meterValueComplete = new MeterValueComplete(
                        null, meterValue, null);
                MeterValueComplete meterValueAddedCompleteDTO = _meterValueAppRestServiceOauth
                        .AddNewMeterValue(context, meterValueComplete);
                return _meterValueAppService.AddNewMeterValue(context,
                        meterValueAddedCompleteDTO.getMeterValue());
            }
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValueController AddNewMeterValue");
            FirebaseCrash.report(e);
            Log.e("MeterValueController", "AddNewMeterValue");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Add new list of meter values
     *
     * @return List meter values representation if added or null if not added.
     */
    public ArrayList<MeterValue> AddNewListMeterValues(Context context,
                                                       ArrayList<MeterValue> meterValues) {
        try {
            Log.i("MeterValueController", "AddNewListMeterValues");
            Profile profile = _profileAppService.GetProfile(context);
            if (profile != null) {

                ArrayList<MeterValueComplete> meterValuesComplete = new ArrayList<MeterValueComplete>();
                Iterator<MeterValue> it = meterValues.iterator();
                while (it.hasNext()) {
                    MeterValue meterValue = it.next();
                    meterValue.setClient_id(profile.getId());
                    meterValuesComplete.add(new MeterValueComplete(null,meterValue, null));
                }
                if (meterValuesComplete.size() > 0) {
                    ArrayList<MeterValueComplete> meterValuesAddedComplete = _meterValueAppRestServiceOauth
                            .AddNewListMeterValues(context,
                                    meterValuesComplete);
                    return CheckErrors(context, meterValues,meterValuesAddedComplete);
                }
            }
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValueController AddNewListMeterValues");
            FirebaseCrash.report(e);
            Log.e("MeterValueController", "AddNewListMeterValues");
            e.printStackTrace();
        }
        return CheckErrors(context, meterValues,new ArrayList<MeterValueComplete>());
    }

    /**
     * Get client monitor parameters
     *
     * @return Monitor parameters representation if exist or null if not exist.
     */
    public MonitorParameters GetMonitorParameters(Context context) {
        try {
            Log.i("MeterValueController", "GetMonitorParameters");
            return _monitorParametersAppService.GetMonitorParameters(context);
        } catch (Exception e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValueController GetMonitorParameters");
            FirebaseCrash.report(e);
            Log.e("MeterValueController", "GetMonitorParameters");
            e.printStackTrace();
        }
        return null;
    }

	/*
	 * PRIVATE METHODS
	 */

    private ArrayList<MeterValue> CheckErrors(Context context,
                                                 ArrayList<MeterValue> meterValuesToAddDTO,
                                                 ArrayList<MeterValueComplete> meterValuesAddedCompleteDTO) {

        ArrayList<MeterValue> meterValuesError = new ArrayList<MeterValue>();

        Iterator<MeterValue> it = meterValuesToAddDTO.iterator();
        while (it.hasNext()) {
            MeterValue meterValueDTO = it.next();
            if ((meterValuesToAddDTO.size() != meterValuesAddedCompleteDTO
                    .size())
                    && !FoundInAddedList(meterValueDTO,
                    meterValuesAddedCompleteDTO)) {
                meterValueDTO.setError(1);
                meterValuesError.add(meterValueDTO);
            }
            try {
                Log.i("MeterValueController", "AddNewMeterValue");
                _meterValueAppService.AddNewMeterValue(context, meterValueDTO);
            } catch (Exception e) {
                FirebaseCrash.logcat(Log.ERROR, TAG, "MeterValueController AddNewMeterValue");
                FirebaseCrash.report(e);
                Log.e("MeterValueController", "CheckErrors");
                e.printStackTrace();
            }
        }
        return meterValuesError;
    }

    private boolean FoundInAddedList(MeterValue meterValueDTO,
                                     ArrayList<MeterValueComplete> meterValuesAddedCompleteDTO) {
        Iterator<MeterValueComplete> it = meterValuesAddedCompleteDTO
                .iterator();
        while (it.hasNext()) {
            MeterValueComplete meterValueCompleteDTO = it.next();
            if (meterValueCompleteDTO.getMeterValue().equals(meterValueDTO))
                return true;
        }
        return false;
    }
}
