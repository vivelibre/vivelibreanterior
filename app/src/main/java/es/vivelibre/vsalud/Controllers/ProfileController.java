package es.vivelibre.vsalud.Controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import es.vivelibre.models.Profile.ImageProfile;
import es.vivelibre.models.Profile.Profile;
import es.vivelibre.services.Profile.ImageProfileAppService;
import es.vivelibre.services.Profile.ProfileAppRestServiceOauth;
import es.vivelibre.services.Profile.ProfileAppService;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 08/07/2016.
 */
public class ProfileController {

    private static ProfileAppService _profileAppService = ProfileAppService
            .getInstance();
    private ImageProfileAppService _imageProfileAppService = ImageProfileAppService
            .getInstance();
    private ProfileAppRestServiceOauth _profileAppRestServiceOauth = ProfileAppRestServiceOauth
            .getInstance();

    private static ProfileController INSTANCE = new ProfileController();

    public static ProfileController getInstance(){ return  INSTANCE;}


    public Profile GetProfile(Context context){
        try{
            Log.i("Profile Controller", "GetProfile");
            return _profileAppService.GetProfile(context);
        }catch (Exception e){
            FirebaseCrash.logcat(Log.ERROR, TAG, "ProfileController GetProfile");
            FirebaseCrash.report(e);
            Log.e("Profile Controller", "GetProfile");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get profile from SQLite
     *
     * @return Profile representation if exist or null if not exist.
     */
    public Profile GetProfileFromService(Context context) throws Exception {
        return _profileAppRestServiceOauth.GetProfile(context);
    }


    public ImageProfile GetImageProfile (Context context){
        try {
            Log.i("Profile Controller", "GetImageProfile");
            return _imageProfileAppService.GetImageProfile(context);
        }catch (Exception e){
            FirebaseCrash.logcat(Log.ERROR, TAG, "ProfileController GetImageProfile");
            FirebaseCrash.report(e);
            Log.e("Profile Controller", "GetImageProfile");
            e.printStackTrace();
        }
        return null;

    }

    public void UpdateImageProfile (Context context, ImageProfile imageProfile){
        try {
            Log.i("Profile Controller", "GetImageProfile");
            _imageProfileAppService.UpdateImageProfile(context,imageProfile);
        }catch (Exception e){
            FirebaseCrash.logcat(Log.ERROR, TAG, "ProfileController GetImageProfile");
            FirebaseCrash.report(e);
            Log.e("Profile Controller", "GetImageProfile");
            e.printStackTrace();
        }

    }

    public Bitmap getContactPhotoFromBase64(String photo) {
        byte[] decodedString = Base64.decode(photo, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
                decodedString.length);
        return decodedByte;

    }

    public Profile getProfile(Context context) throws Exception {
        return _profileAppRestServiceOauth.GetProfile(context);
    }


    public boolean sendImageProfile (Context context , ImageProfile imageProfile){
        try {
            return _imageProfileAppService.sendImageProfile(context,imageProfile);

        }catch (Exception e){
            FirebaseCrash.logcat(Log.ERROR, TAG, "ProfileController sendImageProfile");
            FirebaseCrash.report(e);
            Log.e("Profile Controller", "sendImageProfile");
            return false;
        }
    }


    /**
     * Store profile in SQLite
     *
     */
    public void storeProfile(Context context, Profile profile)
            throws Exception {

        ImageProfile imageProfileAux = profile.getImageProfile();

        if (imageProfileAux != null){
            _imageProfileAppService
                    .CreateImageProfile(context, imageProfileAux);
            Log.i("TAG","store ImageProfile Correcto");
        }
        if (profile != null){
            _profileAppService.CreateProfile(context, profile);
            Log.i("TAG","store Profile Correcto");
        }
    }

}
