package es.vivelibre.vsalud.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.astuetz.PagerSlidingTabStrip;

import es.vivelibre.vsalud.Activities.FragmentHelp_1;
import es.vivelibre.vsalud.Activities.FragmentHelp_2;
import es.vivelibre.vsalud.Activities.FragmentHelp_3;
import es.vivelibre.vsalud.Activities.FragmentHelp_4;
import es.vivelibre.vsalud.R;

public class HelpViewPagerAdapter extends FragmentStatePagerAdapter implements PagerSlidingTabStrip.IconTabProvider{

    private int icons[] = {R.drawable.ic_home_black, R.drawable.ic_system_update_black_24dp, R.drawable.ic_vibration_black_24dp, R.drawable.ic_error_outline_black_24dp, };

    public HelpViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch(i){
            case 0:{
                return new FragmentHelp_1();
            }case 1:{
                return new FragmentHelp_2();
            }case 2:{
                return new FragmentHelp_3();
            }case 3:{
                return new FragmentHelp_4();
            }
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }



    @Override
    public int getPageIconResId(int position) {
        return icons[position];

    }
}
