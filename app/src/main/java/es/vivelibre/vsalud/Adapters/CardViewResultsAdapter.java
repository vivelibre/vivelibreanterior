package es.vivelibre.vsalud.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.vivelibre.models.Utils.CardViewModel;
import es.vivelibre.vsalud.R;

/**
 * Created by mariofernandezmendez on 14/5/16.
 */
public class CardViewResultsAdapter extends RecyclerView.Adapter<CardViewResultsAdapter.CardViewResultHolder> {

    private List<CardViewModel> items;
    private Context context;

    public CardViewResultsAdapter(Context con, List<CardViewModel> items){
        this.items = items;
        this.context = con;
    }

    public class CardViewResultHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //Campos respectivos al item del cardview de la ventana del home
        public TextView tipo;
        public TextView medida;
        //public ImageView imagen;
        public TextView fecha_medida;
        public RelativeLayout currentItem;
        public Context context;
        public List<CardViewModel> list = new ArrayList();


        public CardViewResultHolder(View v, Context context, List<CardViewModel> lista){
            super(v);
            this.context= context;
            this.list = lista;

            tipo = (TextView)v.findViewById(R.id.resultsRowValueType);
            medida =(TextView)v.findViewById(R.id.valueRowResult);
            //imagen =(ImageView)v.findViewById(R.id.valueRowImage);
            fecha_medida =(TextView)v.findViewById(R.id.resultsRowDate);
            currentItem = (RelativeLayout)v.findViewById(R.id.recicladorResults);
        }

        @Override
        public void onClick(View v) {

            //Esto de momento no esta implementado

            int position = getAdapterPosition();
            CardViewModel cardView = this.list.get(position);
            Intent intent;
            String type = "";

            boolean fragmentTransaction = false;

            }

        }

    @Override
    public CardViewResultsAdapter.CardViewResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_results, parent, false);
        return new CardViewResultHolder(v,context,items);
    }

    @Override
    public void onBindViewHolder(CardViewResultHolder holder, int position) {
        holder.medida.setText(items.get(position).getMedida().toString());
        holder.fecha_medida.setText(items.get(position).getFecha_medida().toString());
        holder.tipo.setText(items.get(position).getTipo());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
