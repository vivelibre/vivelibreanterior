package es.vivelibre.vsalud.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import es.vivelibre.vsalud.R;

import es.vivelibre.models.Utils.CardViewModel;

/**
 * Created by mgonzalez on 18/05/2016.
 */
public class CardViewHistoryAdapter extends RecyclerView.Adapter<CardViewHistoryAdapter.CardViewHistoryHolder> {


    private List<CardViewModel> items;
    private Context context;

    public CardViewHistoryAdapter(Context con, List<CardViewModel> items)
    {
        this.items = items;
        this.context = con;
    }


    public class CardViewHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tipo;
        public TextView medida;
        public TextView fecha_medida;
        public TextView unidad;
        public RelativeLayout currentItem;
        public Context context;
        public List<CardViewModel> list = new ArrayList<>();

        public CardViewHistoryHolder(View v, Context context, List<CardViewModel> lista)
        {
            super(v);
            this.context = context;
            this.list = lista;

            tipo = (TextView)v.findViewById(R.id.historyRowValueType);
            medida = (TextView)v.findViewById(R.id.valueRowHistory);
            fecha_medida = (TextView)v.findViewById(R.id.historyRowDate);
            currentItem = (RelativeLayout)v.findViewById(R.id.recicladorHistory);
            unidad = (TextView)v.findViewById(R.id.historyRowValueUnit);


        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            CardViewModel cardView = this.list.get(position);
            Intent intent;
            String type = "";
        }
    }

    @Override
    public CardViewHistoryAdapter.CardViewHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_history, parent, false);
        return new CardViewHistoryHolder(v, context, items);
    }

    @Override
    public void onBindViewHolder(CardViewHistoryHolder holder, int position)
    {
        holder.medida.setText(items.get(position).getMedida().toString());
        holder.fecha_medida.setText(items.get(position).getFecha_medida().toString());
        holder.tipo.setText(items.get(position).getTipo());
        holder.unidad.setText(items.get(position).getValueType().toString());
        /*holder.unidad.setText(getUnit(items.get(position).getTipo()));*/
    }

    @Override
    public int getItemCount() {return items.size();}


    private String getUnit(String type)
    {
        if(type.equals("Peso"))
            return "kg";
        else if(type.equals("Saturación"))
            return "%";
        else if (type.equals("Pulso"))
            return "p.p.m.";
        else if (type.equals("Tensión"))
            return "mmHg";
        else
            return "";

    }

}
