package es.vivelibre.vsalud.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import es.vivelibre.models.Utils.CardViewModel;
import es.vivelibre.vsalud.Activities.HomeActivity;
import es.vivelibre.vsalud.Activities.ManualValuesActivity;
import es.vivelibre.vsalud.Functions.DisplayFunctions;
import es.vivelibre.vsalud.R;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mfernandez on 24/05/2016.
 */
public class CardViewManualValuesAdapter extends RecyclerView.Adapter<CardViewManualValuesAdapter.CardViewHolder>{
    private List<CardViewModel> items;
    private Context context;


    public CardViewManualValuesAdapter(Context con, List<CardViewModel> items){
        this.items = items;
        this.context = con;
    }

    public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //Campos respectivos al item del cardview de la ventana del home
        public TextView tipo;
        public TextView medida;
        public ImageView imagen;
        public TextView fecha_medida;
        public TextView valueUnit;
        public RelativeLayout currentItem;
        public Context context;
        public List<CardViewModel> list = new ArrayList();

        public RelativeLayout newValue;
        public TextView showResults;

        public CardViewHolder(View v, Context context, List<CardViewModel> lista){
            super(v);
            this.context= context;
            this.list = lista;

            this.newValue = (RelativeLayout) v.findViewById(R.id.valueRowNewValue);
            this.showResults = (TextView) v.findViewById(R.id.valueRowMoreResult);

            newValue.setOnClickListener(this);
            showResults.setOnClickListener(this);

            tipo = (TextView)v.findViewById(R.id.valueRowType);
            medida =(TextView)v.findViewById(R.id.valueRowLastValue);
            imagen =(ImageView)v.findViewById(R.id.valueRowImage);
            fecha_medida =(TextView)v.findViewById(R.id.valueRowLastTime);
            valueUnit =(TextView)v.findViewById(R.id.valueRowLastValueUnit);
            currentItem = (RelativeLayout)v.findViewById(R.id.valueRowNewValue);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            CardViewModel cardView = this.list.get(position);
            Intent intent;
            String type = "";

            boolean fragmentTransaction = false;

            if (v == showResults)
            {
                //Vamos a comprobar que imagen viene para saber a que controlador llamamos

                if (cardView.getTipo() == context.getString(R.string.weight)) {
                    type = context.getString(R.string.weight);
                    fragmentTransaction = true;
                } else if (cardView.getTipo() == context.getString(R.string.bloodPressure)) {
                    type = context.getString(R.string.bloodPressure);
                    fragmentTransaction = true;
                } else if (cardView.getTipo() == context.getString(R.string.oxygenSaturation)) {
                    type = context.getString(R.string.oxygenSaturation);
                    fragmentTransaction = true;
                }
                else if (cardView.getTipo() == context.getString(R.string.hearthRate)) {
                    type = context.getString(R.string.hearthRate);
                    fragmentTransaction = true;
                }

                if(fragmentTransaction)
                {
                    ((HomeActivity) this.context).switchToResultContent(type);
                }
            }
            else
            {
                if (cardView.getImagen() == R.drawable.bascula) {
                    //si la imagen corresponde con aparatos A&D llamamos a ANDControlActivity
                    intent = new Intent(this.context, ManualValuesActivity.class);
                }
                else if(cardView.getImagen() == R.drawable.tensiometro){
                    intent = new Intent(this.context, ManualValuesActivity.class);
                }
                else if(cardView.getImagen() == R.drawable.nonin_3230){
                    //Aparatos Nonin3230
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        intent = new Intent(this.context, ManualValuesActivity.class);
                    } else {
                        intent = new Intent(this.context, ManualValuesActivity.class);
                        DisplayFunctions
                                .showToast(
                                        context, String.valueOf(R.string.general_android_version_not_allowed));
                    }
                }
                else if(cardView.getImagen() == R.drawable.chino){
                    intent = new Intent(this.context, ManualValuesActivity.class);
                }
                else{
                    //Para probar que pasamos bien la imagen vamos al Result
                    intent = new Intent(this.context, ManualValuesActivity.class);
                }


                //Esto es para pasar datos a la vista nueva
                intent.putExtra("imagen", cardView.getImagen());
                intent.putExtra("tipo", cardView.getTipo());

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                this.context.startActivity(intent);

            }


        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public CardViewHolder onCreateViewHolder (ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_layout, viewGroup, false);
        return new CardViewHolder(v,context,items);
    }

    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int i ){

        Date fechaAhora = new Date();

        try {
            Calendar cal = Calendar.getInstance();

            DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
            Date fechaMedida= dateFormat.parse(items.get(i).getFecha_medida().replaceAll("CEST", "CST"));
            Long seconds ;
            cal.setTime(fechaMedida);

            cal.add(Calendar.HOUR, -8);

            fechaMedida = cal.getTime();

            seconds = getDateDiff(fechaMedida, fechaAhora , TimeUnit.SECONDS);

            int day = (int)TimeUnit.SECONDS.toDays(seconds);
            long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24);
            long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
            long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60);


            cardViewHolder.fecha_medida.setText("Hace " + calculateTime(seconds));


        } catch (ParseException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, "CardViewManualValuesAdapter onBindViewHolder");
            FirebaseCrash.report(e);
            e.printStackTrace();
        }

        cardViewHolder.imagen.setImageResource(items.get(i).getImagen());
        cardViewHolder.tipo.setText(items.get(i).getTipo());
        cardViewHolder.medida.setText(items.get(i).getMedida().toString());
        cardViewHolder.valueUnit.setText(items.get(i).getValueType());

    }

    public String calculateTime(long seconds) {
        String result = "";
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) -
                TimeUnit.DAYS.toHours(day);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) -
                TimeUnit.DAYS.toMinutes(day) -
                TimeUnit.HOURS.toMinutes(hours);
        long second = TimeUnit.SECONDS.toSeconds(seconds) -
                TimeUnit.DAYS.toSeconds(day) -
                TimeUnit.HOURS.toSeconds(hours) -
                TimeUnit.MINUTES.toSeconds(minute);

        if (day != 0 ){
            result = Math.abs(day) + " Dias " ;
        }else if(hours != 0){
            result +=Math.abs(hours) + " Horas";
        }else if( minute != 0){
            result +=Math.abs(minute) +" Minutos " ;
        }else if(second != 0){
            result +=Math.abs(second) + " Segundos ";
        }
        return result;
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }
}
