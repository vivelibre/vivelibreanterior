package es.vivelibre.vsalud.Activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.vivelibre.vsalud.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class Inicio_Historial {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void inicio_Historial() {
        ViewInteraction appCompatAutoCompleteTextView = onView(
                allOf(withId(R.id.loginUser), isDisplayed()));
        appCompatAutoCompleteTextView.perform(click());

        ViewInteraction appCompatAutoCompleteTextView2 = onView(
                allOf(withId(R.id.loginUser), isDisplayed()));
        appCompatAutoCompleteTextView2.perform(replaceText("eva"), closeSoftKeyboard());

        ViewInteraction appCompatAutoCompleteTextView3 = onView(
                allOf(withId(R.id.loginUser), withText("eva"), isDisplayed()));
        appCompatAutoCompleteTextView3.perform(pressImeActionButton());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.loginPassword), isDisplayed()));
        appCompatEditText.perform(replaceText("1"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.loginPassword), withText("1"), isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.loginButton), withText("INICIAR SESIÓN"),
                        withParent(allOf(withId(R.id.login),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.valueRowType), withText("Saturación"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.card_view),
                                        0),
                                1),
                        isDisplayed()));
        textView.check(matches(withText("Saturación")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.valueRowType), withText("Tensión"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.card_view),
                                        0),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("Tensión")));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.valueRowType), withText("Pulso"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.card_view),
                                        0),
                                1),
                        isDisplayed()));
        textView3.check(matches(withText("Pulso")));

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.valueRowType), withText("Peso"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.card_view),
                                        0),
                                1),
                        isDisplayed()));
        textView4.check(matches(withText("Peso")));

        ViewInteraction textView5 = onView(
                allOf(withId(R.id.valueRowType), withText("Peso"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.card_view),
                                        0),
                                1),
                        isDisplayed()));
        textView5.check(matches(withText("Peso")));

        ViewInteraction imageButton = onView(
                allOf(withContentDescription("Abrir menu"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        imageButton.perform(click());

        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Historial de medidas"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        ViewInteraction textView6 = onView(
                allOf(withText("Historial de medidas"),
                        childAtPosition(
                                allOf(withId(R.id.headResultsLayout),
                                        childAtPosition(
                                                withId(R.id.resultLayout),
                                                0)),
                                0),
                        isDisplayed()));
        textView6.check(matches(withText("Historial de medidas")));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
